package eu.hansolo.enzo;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.*;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path.Direction;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;


import java.util.Locale;


/**
 * Created by hansolo on 05.05.15.
 */
public class AvGauge extends View {
    private static final String TAG = "AvGauge";

    public static final  int   DEFAULT_WIDTH  = 300;
    public static final  int   DEFAULT_HEIGHT = 300;
    public static final  float TOP            = 0.0f;
    public static final  float LEFT           = 0.0f;
    public static final  float RIGHT          = 1.0f;
    public static final  float BOTTOM         = 1.0f;
    public static final  float CENTER         = 0.5f;
    public static final  float MIN_VALUE      = 0.0f;
    public static final  float MAX_VALUE      = 100.0f;
    private static final float ANGLE_RANGE    = 360;
    private static final float START_ANGLE    = 270f;
    private static final float STOP_ANGLE     = START_ANGLE + ANGLE_RANGE;

    private float          size;
    private float          outerValue;
    private float          oldOuterValue;
    private float          currentOuterValue;
    private float          innerValue;
    private float          oldInnerValue;
    private float          currentInnerValue;
    private float          minValue;
    private float          maxValue;
    private float          range;
    private float          angleStep;
    private float          currentOuterValueAngle;
    private float          currentInnerValueAngle;
    private String         title;
    private String         subtitle;
    private int            decimals;
    private int            barBorderColor;
    private int            backgroundColor;
    private int            outerBarColor;
    private int            innerBarColor;
    private int            titleColor;
    private int            outerValueColor;
    private int            innerValueColor;
    private boolean        dynamicBarColor;
    private GradientLookup outerGradientLookup;
    private GradientLookup innerGradientLookup;
    private RectF          barBackgroundRect;
    private Path           barBorder;
    private Path           barBackground;
    private RectF          outerBarRect;
    private RectF          innerBarRect;
    private Paint          barBackgroundPaint;
    private Paint          outerBarPaint;
    private Paint          innerBarPaint;
    private Paint          titlePaint;
    private Paint          outerValuePaint;
    private Paint          innerValuePaint;
    private Path           outerBar;
    private Path           innerBar;
    private ValueAnimator  outerAnimator;
    private ValueAnimator  innerAnimator;
    private boolean        animated;
    private long           animationDuration;
    private boolean        oneBar;


    // ******************** Constructors **************************************
    public AvGauge(final Context CONTEXT, final AttributeSet ATTRS) {
        this(CONTEXT, ATTRS, 0, null);
    }
    public AvGauge(final Context CONTEXT, final AttributeSet ATTRS, final int DEF_STYLE) {
        this(CONTEXT, ATTRS, DEF_STYLE, null);
    }
    public AvGauge(final Context CONTEXT, final AttributeSet ATTRS, final int DEF_STYLE, final GradientLookup GRADIENT_LOOKUP) {
        super(CONTEXT, ATTRS, DEF_STYLE);
        size                = 300;
        outerGradientLookup = null == GRADIENT_LOOKUP ? new GradientLookup() : GRADIENT_LOOKUP;
        innerGradientLookup = null == GRADIENT_LOOKUP ? new GradientLookup() : GRADIENT_LOOKUP;
        barBorder           = new Path();
        barBackground       = new Path();
        outerBar            = new Path();
        innerBar            = new Path();
        outerAnimator       = ValueAnimator.ofFloat(minValue, minValue);
        innerAnimator       = ValueAnimator.ofFloat(minValue, minValue);
        initAttributes(CONTEXT, ATTRS, DEF_STYLE);
        init();
    }


    // ******************** Initialization ************************************
    private void initAttributes(final Context context, final AttributeSet attrs, final int defStyle) {
        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AvGauge, defStyle, 0);

        outerValue         = a.getFloat(R.styleable.AvGauge_outerValueAvGauge, MIN_VALUE);
        innerValue         = a.getFloat(R.styleable.AvGauge_innerValueAvGauge, MIN_VALUE);
        minValue           = a.getFloat(R.styleable.AvGauge_minValueAvGauge, MIN_VALUE);
        maxValue           = a.getFloat(R.styleable.AvGauge_maxValueAvGauge, MAX_VALUE);
        title              = a.getString(R.styleable.AvGauge_titleAvGauge) == null ? "" : a.getString(R.styleable.AvGauge_titleAvGauge);
        subtitle           = a.getString(R.styleable.AvGauge_subtitleAvGauge) == null ? "" : a.getString(R.styleable.AvGauge_subtitleAvGauge);
        animated           = a.getBoolean(R.styleable.AvGauge_animatedAvGauge, true);
        animationDuration  = a.getInt(R.styleable.AvGauge_animationDurationAvGauge, 1500);
        oneBar             = a.getBoolean(R.styleable.AvGauge_oneBar, false);
        decimals           = a.getInt(R.styleable.AvGauge_decimalsAvGauge, 0);
        barBorderColor     = a.getInt(R.styleable.AvGauge_barBorderColorAvGauge, Color.argb(178, 65, 65, 65));
        backgroundColor    = a.getColor(R.styleable.AvGauge_backgroundColorAvGauge, Color.argb(178, 65, 65, 65));
        outerBarColor      = a.getColor(R.styleable.AvGauge_outerBarColorAvGauge, Color.rgb(50, 33, 201));
        innerBarColor      = a.getColor(R.styleable.AvGauge_innerBarColorAvGauge, Color.rgb(50, 20, 180));
        titleColor         = a.getColor(R.styleable.AvGauge_titleColorAvGauge, Color.rgb(255, 255, 255));
        outerValueColor    = a.getColor(R.styleable.AvGauge_outerValueColorAvGauge, Color.rgb(255, 255, 255));
        innerValueColor    = a.getColor(R.styleable.AvGauge_innerValueColorAvGauge, Color.rgb(255, 255, 255));
        dynamicBarColor    = a.getBoolean(R.styleable.AvGauge_dynamicBarColorAvGauge, false);

        a.recycle();

        range     = maxValue - minValue;
        angleStep = ANGLE_RANGE / range;
    }
    private void init() {
        initBounds();
        initGraphics();
        initAnimators();
    }
    private void initBounds() {
        barBackgroundRect = new RectF(LEFT, TOP, RIGHT, BOTTOM);
        outerBarRect      = new RectF(LEFT, TOP, RIGHT, BOTTOM);
        innerBarRect      = new RectF(LEFT, TOP, RIGHT, BOTTOM);
    }
    private void initGraphics() {
        barBackgroundPaint = getBarBackgroundPaint();
        outerBarPaint      = getOuterBarPaint();
        innerBarPaint      = getInnerBarPaint();
        titlePaint         = getTitlePaint();
        outerValuePaint    = getOuterValuePaint();
        innerValuePaint    = getInnerValuePaint();
    }
    private void initAnimators() {
        outerAnimator.setDuration(animationDuration);
        outerAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        outerAnimator.addUpdateListener(new AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {
                outerValue = (Float) animation.getAnimatedValue();
                invalidate();
            }
        });
        innerAnimator.setDuration(animationDuration);
        innerAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        innerAnimator.addUpdateListener(new AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {
                innerValue = (Float) animation.getAnimatedValue();
                invalidate();
            }
        });
    }


    // ******************** Graphics related **********************************
    private Paint getBarBackgroundPaint() {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        return paint;
    }
    private Paint getOuterBarPaint() {
        final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        return paint;
    }
    private Paint getInnerBarPaint() {
        final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        return paint;
    }
    private Paint getTitlePaint() {
        final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setColor(titleColor);
        paint.setStyle(Style.FILL);
        paint.setTextSize(getHeight() * 0.08f);
        paint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "Lato-Lig.ttf"));
        return paint;
    }
    private Paint getOuterValuePaint() {
        final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setColor(outerValueColor);
        paint.setStyle(Style.FILL);
        paint.setTextSize(getHeight() * 0.2f);
        paint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "Lato-Lig.ttf"));
        return paint;
    }
    private Paint getInnerValuePaint() {
        final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setColor(outerValueColor);
        paint.setStyle(Style.FILL);
        paint.setTextSize(getHeight() * 0.2f);
        paint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "Lato-Lig.ttf"));
        return paint;
    }


    // ******************** State related *************************************
    @Override protected void onRestoreInstanceState(final Parcelable state) {
        final Bundle bundle     = (Bundle) state;
        final Parcelable superState = bundle.getParcelable("superState");
        super.onRestoreInstanceState(superState);
        outerValue = bundle.getFloat("outerValue");
        innerValue = bundle.getFloat("innerValue");
        title      = bundle.getString("title");
    }
    @Override protected Parcelable onSaveInstanceState() {
        final Parcelable superState = super.onSaveInstanceState();
        final Bundle     state      = new Bundle();
        state.putParcelable("superState", superState);
        state.putFloat("outerValue", outerValue);
        state.putFloat("innerValue", innerValue);
        state.putString("title", title);
        return state;
    }


    // ******************** Size related **************************************
    @Override protected void onMeasure(final int WIDTH_MEASURE_SPEC, final int HEIGHT_MEASURE_SPEC) {
        // Loggable.log.debug(String.format("WIDTH_MEASURE_SPEC=%s, HEIGHT_MEASURE_SPEC=%s",
        // View.MeasureSpec.toString(WIDTH_MEASURE_SPEC),
        // View.MeasureSpec.toString(HEIGHT_MEASURE_SPEC)));

        final int WIDTH_MODE  = MeasureSpec.getMode(WIDTH_MEASURE_SPEC);
        final int HEIGHT_MODE = MeasureSpec.getMode(HEIGHT_MEASURE_SPEC);
        final int WIDTH_SIZE  = MeasureSpec.getSize(WIDTH_MEASURE_SPEC);
        final int HEIGHT_SIZE = MeasureSpec.getSize(HEIGHT_MEASURE_SPEC);

        final int CHOSEN_WIDTH  = chooseWidth(WIDTH_MODE, WIDTH_SIZE);
        final int CHOSEN_HEIGHT = chooseHeight(HEIGHT_MODE, HEIGHT_SIZE);
        setMeasuredDimension(CHOSEN_WIDTH, CHOSEN_HEIGHT);
    }

    private int chooseWidth(final int MODE, final int WIDTH) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return WIDTH;
            case View.MeasureSpec.UNSPECIFIED:
            default                          :return DEFAULT_WIDTH;
        }
    }
    private int chooseHeight(final int MODE, final int HEIGHT) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return HEIGHT;
            case View.MeasureSpec.UNSPECIFIED:
            default                          : return DEFAULT_HEIGHT;
        }
    }

    @Override protected void onSizeChanged(final int WIDTH, final int HEIGHT, final int OLD_WIDTH, final int OLD_HEIGHT) { resize(); }

    private void resize() {
        final Canvas CANVAS = new Canvas();
        final float scale = Math.min(getWidth(), getHeight());
        size  = getWidth() < getHeight() ? getWidth() : getHeight();
        
        CANVAS.scale(scale, scale);
        CANVAS.translate((scale == size) ? ((size - scale) / 2) / scale : 0, (scale == size) ? ((size - scale) / 2) / scale : 0);

        barBackgroundRect.set(0, 0, size, size);
        outerBarRect.set(0, 0, size, size);
        innerBarRect.set(0, 0, size, size);

        resizeBackground();
        resizeOuterBar();
        resizeInnerBar();

        adjustOuterValueTextSize(String.format(Locale.US, "%." + decimals + "f", outerValue));
        adjustInnerValueTextSize(String.format(Locale.US, "%." + decimals + "f", innerValue));
        adjustTitleTextSize(title);

    }
    private void resizeBackground() {
        final float SIZE   = getWidth() < getHeight() ? getWidth() : getHeight();
        final float CENTER = 0.5f * SIZE;

        barBorder.reset();
        barBorder.addCircle(CENTER, CENTER, 0.5f * SIZE, Direction.CW);
        barBorder.close();

        barBackground.reset();
        barBackground.addCircle(CENTER, CENTER, 0.28f * SIZE, Direction.CW);
        barBackground.close();
    }
    private void resizeOuterBar() {
        final float SIZE = getWidth() < getHeight() ? getWidth() : getHeight();

        if (Float.compare(minValue, 0) > 0) {
            currentOuterValueAngle = ((outerValue - Math.abs(minValue))) * angleStep;
        } else {
            if (Float.compare(outerValue, 0) < 0) {
                currentOuterValueAngle = ((outerValue - Math.abs(minValue)) * (-1)) * angleStep;
            } else {
                currentOuterValueAngle = (outerValue + Math.abs(minValue)) * angleStep;
            }
        }

        float sinAlpha = (float) Math.sin(Math.toRadians(currentOuterValueAngle + 270));
        float cosAlpha = (float) Math.cos(Math.toRadians(currentOuterValueAngle + 270));
        float center   = 0.5f * SIZE;
        if (Float.compare(currentOuterValueAngle, 0f) <= 0) {
            outerBar.reset();
            return;
        }
        if (oneBar) {
            outerBar.reset();
            outerBar.addArc(0f, 0f, SIZE, SIZE, START_ANGLE, currentOuterValueAngle);
            outerBar.lineTo(center + cosAlpha * 0.28f * SIZE, center + sinAlpha * 0.28f * SIZE);
            outerBar.addArc(0.22f * SIZE, 0.22f * SIZE, 0.78f * SIZE, 0.78f * SIZE, START_ANGLE + currentOuterValueAngle, -currentOuterValueAngle);
            outerBar.lineTo(center, 0f);
            outerBar.close();
        } else {
            outerBar.reset();
            outerBar.addArc(0f, 0f, SIZE, SIZE, START_ANGLE, currentOuterValueAngle);
            outerBar.lineTo(center + cosAlpha * 0.38f * SIZE, center + sinAlpha * 0.38f * SIZE);
            outerBar.addArc(0.12f * SIZE, 0.12f * SIZE, 0.88f * SIZE, 0.88f * SIZE, START_ANGLE + currentOuterValueAngle, -currentOuterValueAngle);
            outerBar.lineTo(center, 0f);
            outerBar.close();
        }
    }
    private void resizeInnerBar() {
        final float SIZE = getWidth() < getHeight() ? getWidth() : getHeight();

        if (Float.compare(minValue, 0) > 0) {
            currentInnerValueAngle = ((innerValue - Math.abs(minValue))) * angleStep;
        } else {
            if (Float.compare(innerValue, 0) < 0) {
                currentInnerValueAngle = ((innerValue - Math.abs(minValue)) * (-1)) * angleStep;
            } else {
                currentInnerValueAngle = (innerValue + Math.abs(minValue)) * angleStep;
            }
        }

        float sinAlpha = (float) Math.sin(Math.toRadians(currentInnerValueAngle + 270));
        float cosAlpha = (float) Math.cos(Math.toRadians(currentInnerValueAngle + 270));
        float center   = 0.5f * SIZE;
        if (Float.compare(currentInnerValueAngle, 0f) <= 0) {
            innerBar.reset();
            return;
        }
        innerBar.reset();
        innerBar.addArc(0.12f * SIZE, 0.12f * SIZE, 0.88f * SIZE, 0.88f * SIZE, START_ANGLE, currentInnerValueAngle);
        innerBar.lineTo(center + cosAlpha * 0.28f * SIZE, center + sinAlpha * 0.28f * SIZE);
        innerBar.addArc(0.22f * SIZE, 0.22f * SIZE, 0.78f * SIZE, 0.78f * SIZE, START_ANGLE + currentInnerValueAngle, -currentInnerValueAngle);
        innerBar.lineTo(center, 0.12f * SIZE);
        innerBar.close();
    }

    private void adjustOuterValueTextSize(final String TEXT) {
        final float SIZE = getWidth() < getHeight() ? getWidth() : getHeight();
        outerValuePaint.setTextSize(SIZE * 0.23677f);
        float decrement = 0f;
        while (outerValuePaint.measureText(TEXT) > 0.5f * size && outerValuePaint.getTextSize() > 0) {
            outerValuePaint.setTextSize((SIZE * (0.23677f - decrement)));
            decrement += 0.01f;
        }
    }
    private void adjustInnerValueTextSize(final String TEXT) {
        final float SIZE = getWidth() < getHeight() ? getWidth() : getHeight();
        innerValuePaint.setTextSize(SIZE * 0.08f);
        float decrement = 0f;
        while (innerValuePaint.measureText(TEXT) > 0.41333f * SIZE && innerValuePaint.getTextSize() > 0f) {
            innerValuePaint.setTextSize((SIZE * (0.08f - decrement)));
            decrement += 0.01f;
        }
    }
    private void adjustTitleTextSize(final String TEXT) {
        final float SIZE = getWidth() < getHeight() ? getWidth() : getHeight();
        titlePaint.setTextSize(SIZE * 0.08f);
        float decrement = 0f;
        while (titlePaint.measureText(TEXT) > 0.56667f * SIZE && titlePaint.getTextSize() > 0) {
            titlePaint.setTextSize((SIZE * (0.08f - decrement)));
            decrement += 0.01f;
        }
    }


    // ******************** Drawing related ***********************************
    @Override protected void onDraw(final Canvas CANVAS) {
        drawBackground(CANVAS);
        if (oneBar) {
            updateOuterBar(CANVAS);
        } else {
            updateOuterBar(CANVAS);
            updateInnerBar(CANVAS);
        }
    }

    private void drawBackground(final Canvas CANVAS) {
        // bar border
        barBackgroundPaint.setStyle(Style.STROKE);
        barBackgroundPaint.setColor(barBorderColor);
        CANVAS.drawPath(barBorder, barBackgroundPaint);

        // bar background
        barBackgroundPaint.setStyle(Style.FILL);
        barBackgroundPaint.setColor(backgroundColor);
        CANVAS.drawPath(barBackground, barBackgroundPaint);

        // title
        titlePaint.setColor(titleColor);
        titlePaint.setTextAlign(Align.CENTER);
        CANVAS.drawText(title, 0.5f * size, 0.62f * size, titlePaint);
    }
    private void updateOuterBar(final Canvas CANVAS) {
        // bar background
        if (isDynamicBarColor() && outerGradientLookup.getStops().size() > 1) {
            outerBarColor = outerGradientLookup.getColorAt(outerValue / range);
        }
        outerBarPaint.setColor(outerBarColor);
        resizeOuterBar();
        CANVAS.drawPath(outerBar, outerBarPaint);

        // Value
        outerValuePaint.setColor(outerValueColor);
        outerValuePaint.setTextAlign(Align.CENTER);
        adjustOuterValueTextSize(String.format(Locale.US, "%." + decimals + "f", outerValue));
        CANVAS.drawText(String.format(Locale.US, "%." + decimals + "f", outerValue), 0.5f * size, 0.48f * size, outerValuePaint);

        // Subtitle (only if oneBar == true)
        if (oneBar) {
            innerValuePaint.setColor(outerValueColor);
            innerValuePaint.setTextAlign(Align.CENTER);
            adjustInnerValueTextSize(subtitle);
            CANVAS.drawText(subtitle, size * 0.5f , 0.72f * size, innerValuePaint);
        }
    }
    private void updateInnerBar(final Canvas CANVAS) {
        // bar background
        if (isDynamicBarColor() && innerGradientLookup.getStops().size() > 1) {
            innerBarColor = innerGradientLookup.getColorAt(innerValue / range);
        }
        innerBarPaint.setColor(innerBarColor);
        resizeInnerBar();
        CANVAS.drawPath(innerBar, innerBarPaint);

        // Value
        innerValuePaint.setColor(innerValueColor);
        innerValuePaint.setTextAlign(Align.CENTER);
        adjustOuterValueTextSize(String.format(Locale.US, "%." + decimals + "f", innerValue));
        CANVAS.drawText(String.format(Locale.US, "%." + decimals + "f", innerValue), size * 0.5f , 0.72f * size, innerValuePaint);
    }


    // ******************** Methods *******************************************
    public float getOuterValue() { return outerValue; }
    public void setOuterValue(final float VALUE) {
        oldOuterValue = outerValue;
        if (animated) {
            currentOuterValue = Util.clamp(minValue, maxValue, VALUE);
            if (outerAnimator.isRunning()) outerAnimator.cancel();
            outerAnimator.setFloatValues(oldOuterValue, currentOuterValue);
            outerAnimator.start();
        } else {
            outerValue = Util.clamp(minValue, maxValue, VALUE);
            invalidate();
        }
    }

    public float getInnerValue() { return innerValue; }
    public void setInnerValue(final float VALUE) {
        oldInnerValue = innerValue;
        if (animated) {
            currentInnerValue = Util.clamp(minValue, maxValue, VALUE);
            if (innerAnimator.isRunning()) innerAnimator.cancel();
            innerAnimator.setFloatValues(oldInnerValue, currentInnerValue);
            innerAnimator.start();
        } else {
            innerValue = Util.clamp(minValue, maxValue, VALUE);
            invalidate();
        }
    }

    public String getTitle() {return title; }
    public void setTitle(final String TITLE) {
        title = TITLE;
        invalidate();
    }

    public String getSubtitle() { return subtitle; }
    public void setSubtitle(final String SUBTITLE) {
        subtitle = SUBTITLE;
        invalidate();
    }

    public int getOuterBarColor() { return outerBarColor; }
    public void setOuterBarColor(final int BAR_COLOR) {
        outerBarColor = BAR_COLOR;
        invalidate();
    }

    public int getInnerBarColor() { return innerBarColor; }
    public void setInnerBarColor(final int BAR_COLOR) {
        innerBarColor = BAR_COLOR;
        invalidate();
    }

    public boolean isDynamicBarColor() { return dynamicBarColor; }
    public void setDynamicBarColor(final boolean DYNAMIC_BAR_COLOR) { dynamicBarColor = DYNAMIC_BAR_COLOR; }

    public GradientLookup getOuterGradientLookup() { return outerGradientLookup; }
    public void setOuterGradientLookup(final GradientLookup GRADIENT_LOOKUP) { outerGradientLookup = GRADIENT_LOOKUP; }

    public GradientLookup getInnerGradientLookup() { return innerGradientLookup; }
    public void setInnerGradientLookup(final GradientLookup GRADIENT_LOOKUP) { innerGradientLookup = GRADIENT_LOOKUP; }

    public boolean isOneBar() { return oneBar; }
    public void setOneBar(final boolean ONE_BAR) {
        oneBar = ONE_BAR;
        invalidate();
    }


    // ******************** Utility methods ***********************************
    private String valueString(final float VALUE) { return String.format(Locale.US, "%." + Integer.toString(decimals) + "f", VALUE); }
}
