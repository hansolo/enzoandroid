package eu.hansolo.enzo;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import java.util.Locale;


/**
 * Created by hansolo on 20.10.15.
 */
public class FlatGauge extends View {
    private static final String TAG = "FlatGauge";

    public static final  int   DEFAULT_WIDTH  = 300;
    public static final  int   DEFAULT_HEIGHT = 300;
    public static final  float TOP            = 0.0f;
    public static final  float LEFT           = 0.0f;
    public static final  float RIGHT          = 1.0f;
    public static final  float BOTTOM         = 1.0f;
    public static final  float CENTER         = 0.5f;
    public static final  float MIN_VALUE      = 0.0f;
    public static final  float MAX_VALUE      = 100.0f;
    private static final float ANGLE_RANGE    = 360;
    private static final float START_ANGLE    = 270f;
    private static final float STOP_ANGLE     = START_ANGLE + ANGLE_RANGE;

    private float          size;
    private float          value;
    private float          oldValue;
    private float          currentValue;
    private float          minValue;
    private float          maxValue;
    private float          range;
    private float          angleStep;
    private float          currentValueAngle;
    private String         title;
    private String         unit;
    private int            decimals;
    private int            barBackgroundColor;
    private int            barColor;
    private int            titleColor;
    private int            valueColor;
    private int            unitColor;
    private boolean        dynamicBarColor;
    private GradientLookup gradientLookup;
    private RectF          barBackgroundRect;
    private Path           colorRing;
    private Path           separator;
    private Path           barBackground;
    private RectF          barRect;
    private Paint          barBackgroundPaint;
    private Paint          barPaint;
    private Paint          titlePaint;
    private Paint          valuePaint;
    private Paint          unitPaint;
    private Path           bar;
    private ValueAnimator  animator;
    private boolean        animated;
    private long           animationDuration;
    private int            separatorColor;
    private boolean        separatorVisible;
    private boolean        colorRingVisible;


    // ******************** Constructors **************************************
    public FlatGauge(final Context CONTEXT, final AttributeSet ATTRS) {
        this(CONTEXT, ATTRS, 0, null);
    }
    public FlatGauge(final Context CONTEXT, final AttributeSet ATTRS, final int DEF_STYLE) {
        this(CONTEXT, ATTRS, DEF_STYLE, null);
    }
    public FlatGauge(final Context CONTEXT, final AttributeSet ATTRS, final int DEF_STYLE, final GradientLookup GRADIENT_LOOKUP) {
        super(CONTEXT, ATTRS, DEF_STYLE);
        size           = 300;
        gradientLookup = null == GRADIENT_LOOKUP ? new GradientLookup() : GRADIENT_LOOKUP;
        colorRing      = new Path();
        separator      = new Path();
        barBackground  = new Path();
        bar            = new Path();
        animator       = ValueAnimator.ofFloat(minValue, minValue);

        initAttributes(CONTEXT, ATTRS, DEF_STYLE);
        init();
    }


    // ******************** Initialization ************************************
    private void initAttributes(final Context context, final AttributeSet attrs, final int defStyle) {
        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FlatGauge, defStyle, 0);

        value              = a.getFloat(R.styleable.FlatGauge_outerValueFlatGauge, MIN_VALUE);
        minValue           = a.getFloat(R.styleable.FlatGauge_minValueFlatGauge, MIN_VALUE);
        maxValue           = a.getFloat(R.styleable.FlatGauge_maxValueFlatGauge, MAX_VALUE);
        title              = a.getString(R.styleable.FlatGauge_titleFlatGauge) == null ? "" : a.getString(R.styleable.FlatGauge_titleFlatGauge);
        unit               = a.getString(R.styleable.FlatGauge_unitFlatGauge) == null ? "" : a.getString(R.styleable.FlatGauge_unitFlatGauge);
        animated           = a.getBoolean(R.styleable.FlatGauge_animatedFlatGauge, true);
        animationDuration  = a.getInt(R.styleable.FlatGauge_animationDurationFlatGauge, 1500);
        decimals           = a.getInt(R.styleable.FlatGauge_decimalsFlatGauge, 0);
        barBackgroundColor = a.getColor(R.styleable.FlatGauge_barBackgroundColorFlatGauge, Color.parseColor("#ffffff"));
        barColor           = a.getColor(R.styleable.FlatGauge_barColorFlatGauge, Color.parseColor("#00ffff"));
        titleColor         = a.getColor(R.styleable.FlatGauge_titleColorFlatGauge, Color.parseColor("#333333"));
        valueColor         = a.getColor(R.styleable.FlatGauge_valueColorFlatGauge, Color.parseColor("#333333"));
        unitColor          = a.getColor(R.styleable.FlatGauge_unitColorFlatGauge, Color.parseColor("#333333"));
        dynamicBarColor    = a.getBoolean(R.styleable.FlatGauge_dynamicBarColorFlatGauge, false);
        separatorColor     = a.getColor(R.styleable.FlatGauge_separatorColorFlatGauge, Color.parseColor("#d0d0d0"));
        separatorVisible   = a.getBoolean(R.styleable.FlatGauge_separatorVisibleFlatGauge, true);
        colorRingVisible   = a.getBoolean(R.styleable.FlatGauge_colorRingVisibleFlatGauge, true);

        a.recycle();

        range     = maxValue - minValue;
        angleStep = ANGLE_RANGE / range;
    }
    private void init() {
        initBounds();
        initGraphics();
        initAnimators();
    }
    private void initBounds() {
        barBackgroundRect = new RectF(LEFT, TOP, RIGHT, BOTTOM);
        barRect           = new RectF(LEFT, TOP, RIGHT, BOTTOM);
    }
    private void initGraphics() {
        barBackgroundPaint = getBarBackgroundPaint();
        barPaint           = getBarPaint();
        titlePaint         = getTitlePaint();
        valuePaint         = getValuePaint();
        unitPaint          = getUnitPaint();
    }
    private void initAnimators() {
        animator.setDuration(animationDuration);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.addUpdateListener(new AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {
                value = (Float) animation.getAnimatedValue();
                invalidate();
            }
        });
    }


    // ******************** Graphics related **********************************
    private Paint getBarBackgroundPaint() { return new Paint(Paint.ANTI_ALIAS_FLAG); }
    private Paint getBarPaint() {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Style.STROKE);
        return paint;
    }
    private Paint getTitlePaint() {
        final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setColor(titleColor);
        paint.setStyle(Style.FILL);
        paint.setTextSize(getHeight() * 0.08f);
        paint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "Roboto-Light.ttf"));
        return paint;
    }
    private Paint getValuePaint() {
        final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setColor(valueColor);
        paint.setStyle(Style.FILL);
        paint.setTextSize(getHeight() * 0.3f);
        paint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "Roboto-Regular.ttf"));
        return paint;
    }
    private Paint getUnitPaint() {
        final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setColor(valueColor);
        paint.setStyle(Style.FILL);
        paint.setTextSize(getHeight() * 0.2f);
        paint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "Roboto-Light.ttf"));
        return paint;
    }


    // ******************** State related *************************************
    @Override protected void onRestoreInstanceState(final Parcelable state) {
        final Bundle bundle     = (Bundle) state;
        final Parcelable superState = bundle.getParcelable("superState");
        super.onRestoreInstanceState(superState);
        value = bundle.getFloat("value");
        title = bundle.getString("title");
        unit  = bundle.getString("unit");
    }
    @Override protected Parcelable onSaveInstanceState() {
        final Parcelable superState = super.onSaveInstanceState();
        final Bundle     state      = new Bundle();
        state.putParcelable("superState", superState);
        state.putFloat("value", value);
        state.putString("title", title);
        state.putString("unit", unit);
        return state;
    }


    // ******************** Size related **************************************
    @Override protected void onMeasure(final int WIDTH_MEASURE_SPEC, final int HEIGHT_MEASURE_SPEC) {
        // Loggable.log.debug(String.format("WIDTH_MEASURE_SPEC=%s, HEIGHT_MEASURE_SPEC=%s",
        // View.MeasureSpec.toString(WIDTH_MEASURE_SPEC),
        // View.MeasureSpec.toString(HEIGHT_MEASURE_SPEC)));

        final int WIDTH_MODE  = MeasureSpec.getMode(WIDTH_MEASURE_SPEC);
        final int HEIGHT_MODE = MeasureSpec.getMode(HEIGHT_MEASURE_SPEC);
        final int WIDTH_SIZE  = MeasureSpec.getSize(WIDTH_MEASURE_SPEC);
        final int HEIGHT_SIZE = MeasureSpec.getSize(HEIGHT_MEASURE_SPEC);

        final int CHOSEN_WIDTH  = chooseWidth(WIDTH_MODE, WIDTH_SIZE);
        final int CHOSEN_HEIGHT = chooseHeight(HEIGHT_MODE, HEIGHT_SIZE);
        setMeasuredDimension(CHOSEN_WIDTH, CHOSEN_HEIGHT);
    }

    private int chooseWidth(final int MODE, final int WIDTH) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return WIDTH;
            case View.MeasureSpec.UNSPECIFIED:
            default                          :return DEFAULT_WIDTH;
        }
    }
    private int chooseHeight(final int MODE, final int HEIGHT) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return HEIGHT;
            case View.MeasureSpec.UNSPECIFIED:
            default                          : return DEFAULT_HEIGHT;
        }
    }

    @Override protected void onSizeChanged(final int WIDTH, final int HEIGHT, final int OLD_WIDTH, final int OLD_HEIGHT) { resize(); }

    private void resize() {
        final Canvas CANVAS = new Canvas();
        final float scale = Math.min(getWidth(), getHeight());
        size  = getWidth() < getHeight() ? getWidth() : getHeight();

        CANVAS.scale(scale, scale);
        CANVAS.translate((scale == size) ? ((size - scale) / 2) / scale : 0, (scale == size) ? ((size - scale) / 2) / scale : 0);

        barBackgroundRect.set(0, 0, size, size);
        barRect.set(0, 0, size, size);

        resizeBackground();
        resizeBar();

        resizeTitleText(title);
        resizeValueText(String.format(Locale.US, "%." + decimals + "f", value));
        resizeUnitText(unit);

    }
    private void resizeBackground() {
        final float CENTER = 0.5f * size;

        if (colorRingVisible) {
            colorRing.reset();
            colorRing.addCircle(CENTER, CENTER, 0.496f * size, Direction.CW);
            colorRing.close();
        }

        if (separatorVisible) {
            separator.reset();
            separator.moveTo(CENTER, 0.02f * size);
            separator.lineTo(CENTER, 0.138f * size);
            separator.close();
        }

        barBackground.reset();
        barBackground.addCircle(CENTER, CENTER, 0.363f * size, Direction.CW);
        barBackground.close();
    }
    private void resizeBar() {
        if (Float.compare(minValue, 0) > 0) {
            currentValueAngle = ((value - Math.abs(minValue))) * angleStep;
        } else {
            if (Float.compare(value, 0) < 0) {
                currentValueAngle = ((value - Math.abs(minValue)) * (-1)) * angleStep;
            } else {
                currentValueAngle = (value + Math.abs(minValue)) * angleStep;
            }
        }

        if (Float.compare(currentValueAngle, 0f) <= 0) {
            bar.reset();
            return;
        }

        bar.reset();
        bar.addArc(0, 0, 0.842f * size, 0.842f * size, START_ANGLE, currentValueAngle);

        Matrix translate = new Matrix();
        translate.setTranslate(0.079f * size, 0.079f * size);
        bar.transform(translate);
    }

    private void resizeTitleText(final String TEXT) {
        final float SIZE = getWidth() < getHeight() ? getWidth() : getHeight();
        titlePaint.setTextSize(SIZE * 0.08f);
        float decrement = 0f;
        while (titlePaint.measureText(TEXT) > 0.56667f * SIZE && titlePaint.getTextSize() > 0) {
            titlePaint.setTextSize((SIZE * (0.08f - decrement)));
            decrement += 0.01f;
        }
    }
    private void resizeValueText(final String TEXT) {
        final float SIZE = getWidth() < getHeight() ? getWidth() : getHeight();
        valuePaint.setTextSize(SIZE * 0.3f);
        float decrement = 0f;
        while (valuePaint.measureText(TEXT) > 0.5f * size && valuePaint.getTextSize() > 0) {
            valuePaint.setTextSize((SIZE * (0.3f - decrement)));
            decrement += 0.01f;
        }
    }
    private void resizeUnitText(final String TEXT) {
        final float SIZE = getWidth() < getHeight() ? getWidth() : getHeight();
        unitPaint.setTextSize(SIZE * 0.08f);
        float decrement = 0f;
        while (unitPaint.measureText(TEXT) > 0.41333f * SIZE && unitPaint.getTextSize() > 0f) {
            unitPaint.setTextSize((SIZE * (0.08f - decrement)));
            decrement += 0.01f;
        }
    }


    // ******************** Drawing related ***********************************
    @Override protected void onDraw(final Canvas CANVAS) {
        drawBackground(CANVAS);
        updateBar(CANVAS);
    }

    private void drawBackground(final Canvas CANVAS) {
        // bar background
        barBackgroundPaint.setStyle(Style.FILL);
        barBackgroundPaint.setColor(barBackgroundColor);
        CANVAS.drawPath(barBackground, barBackgroundPaint);

        // title
        titlePaint.setColor(titleColor);
        titlePaint.setTextAlign(Align.CENTER);
        CANVAS.drawText(title, 0.5f * size, 0.315f * size, titlePaint);

        // unit
        unitPaint.setColor(unitColor);
        unitPaint.setTextAlign(Align.CENTER);
        CANVAS.drawText(unit, 0.5f * size, 0.75f * size, unitPaint);

        // color ring
        if (colorRingVisible) {
            barBackgroundPaint.setStyle(Style.STROKE);
            barBackgroundPaint.setStrokeWidth(size * 0.0075f);
            barBackgroundPaint.setColor(barColor);
            CANVAS.drawPath(colorRing, barBackgroundPaint);
        }

        // separator
        if (separatorVisible) {
            barBackgroundPaint.setStyle(Style.STROKE);
            barBackgroundPaint.setStrokeWidth(1f);
            barBackgroundPaint.setColor(separatorColor);
            CANVAS.drawPath(separator, barBackgroundPaint);
        }
    }
    private void updateBar(final Canvas CANVAS) {
        // bar background
        if (isDynamicBarColor() && gradientLookup.getStops().size() > 1) {
            barColor = gradientLookup.getColorAt(value / range);
        }
        barPaint.setStrokeWidth(size * 0.12f);
        barPaint.setStrokeCap(Cap.BUTT);
        barPaint.setColor(barColor);
        resizeBar();
        CANVAS.drawPath(bar, barPaint);

        // Value
        valuePaint.setColor(valueColor);
        valuePaint.setTextAlign(Align.CENTER);
        resizeValueText(String.format(Locale.US, "%." + decimals + "f", value));
        CANVAS.drawText(String.format(Locale.US, "%." + decimals + "f", value), 0.5f * this.size, 0.6f * this.size, valuePaint);
    }


    // ******************** Methods *******************************************
    public float getValue() { return value; }
    public void setValue(final float VALUE) {
        oldValue = value;
        if (animated) {
            currentValue = Util.clamp(minValue, maxValue, VALUE);
            if (animator.isRunning()) animator.cancel();
            animator.setFloatValues(oldValue, currentValue);
            animator.start();
        } else {
            value = Util.clamp(minValue, maxValue, VALUE);
            invalidate();
        }
    }

    public String getTitle() {return title; }
    public void setTitle(final String TITLE) {
        title = TITLE;
        invalidate();
    }

    public String getUnit() { return unit; }
    public void setUnit(final String UNIT) {
        unit = UNIT;
        invalidate();
    }

    public int getBarBackgroundColor() { return barBackgroundColor; }
    public void setBarBackgroundColor(final int BACKGROUND_COLOR) {
        barBackgroundColor = BACKGROUND_COLOR;
        invalidate();
    }

    public int getBarColor() { return barColor; }
    public void setBarColor(final int BAR_COLOR) {
        barColor = BAR_COLOR;
        invalidate();
    }

    public int getTitleColor() { return titleColor; }
    public void setTitleColor(final int TITLE_COLOR) {
        titleColor = TITLE_COLOR;
        invalidate();
    }

    public int getValueColor() { return valueColor; }
    public void setValueColor(final int VALUE_COLOR) {
        valueColor = VALUE_COLOR;
        invalidate();
    }

    public int getUnitColor() { return unitColor; }
    public void setUnitColor(final int UNIT_COLOR) {
        unitColor = UNIT_COLOR;
        invalidate();
    }

    public boolean isDynamicBarColor() { return dynamicBarColor; }
    public void setDynamicBarColor(final boolean DYNAMIC_BAR_COLOR) { dynamicBarColor = DYNAMIC_BAR_COLOR; }

    public GradientLookup getGradientLookup() { return gradientLookup; }
    public void setGradientLookup(final GradientLookup GRADIENT_LOOKUP) { gradientLookup = GRADIENT_LOOKUP; }

    public int getSeparatorColor() { return separatorColor; }
    public void setSeparatorColor(final int SEPARATOR_COLOR) {
        separatorColor = SEPARATOR_COLOR;
        invalidate();
    }

    public boolean isSeparatorVisible() { return separatorVisible; }
    public void setSeparatorVisible(final boolean SEPARATOR_VISIBLE) {
        separatorVisible = SEPARATOR_VISIBLE;
        invalidate();
    }

    public boolean isColorRingVisible() { return colorRingVisible; }
    public void setColorRingVisible(final boolean COLOR_RING_VISIBLE) {
        colorRingVisible = COLOR_RING_VISIBLE;
        invalidate();
    }
}
