package eu.hansolo.enzo;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.*;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path.Direction;
import android.graphics.Shader.TileMode;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import eu.hansolo.enzo.SectionEvent.SectionEventType;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Created by hansolo on 05.05.15.
 */
public class Gauge extends View {
    private static final String TAG = "Gauge";

    public static final  int      DEFAULT_WIDTH  = 300;
    public static final  int      DEFAULT_HEIGHT = 300;
    public static final  float    TOP            = 0.0f;
    public static final  float    LEFT           = 0.0f;
    public static final  float    RIGHT          = 1.0f;
    public static final  float    BOTTOM         = 1.0f;
    public static final  float    MIN_VALUE      = 0.0f;
    public static final  float    MAX_VALUE      = 100.0f;
    private static final float    ANGLE_RANGE    = 280;
    private static final float    START_ANGLE    = 140f;
    private final        Typeface ROBOTO_THIN    = Typeface.createFromAsset(getResources().getAssets(), "Roboto-Thin.ttf");
    private final        Typeface ROBOTO_REGULAR = Typeface.createFromAsset(getResources().getAssets(), "Roboto-Regular.ttf");
    private final        Typeface ROBOTO_MEDIUM  = Typeface.createFromAsset(getResources().getAssets(), "Roboto-Medium.ttf");

    public static enum Limit {
        EXCEEDED,
        IN_RANGE,
        UNDERRUN
    }

    public static enum NeedleType {
        STANDARD;
    }

    public static enum TickLabelOrientation {
        ORTHOGONAL,
        HORIZONTAL,
        TANGENT
    }

    public static enum NumberFormat {
        AUTO("0"),
        STANDARD("0"),
        FRACTIONAL("0.0#"),
        SCIENTIFIC("0.##E0"),
        PERCENTAGE("##0.0%");

        private final DecimalFormat DF;

        private NumberFormat(final String FORMAT_STRING) {
            Locale.setDefault(new Locale("en", "US"));

            DF = new DecimalFormat(FORMAT_STRING);
        }

        public String format(final Number NUMBER) {
            return DF.format(NUMBER);
        }
    }

    private float size;
    private float center;

    private boolean interactive;

    private float                value;
    private float                oldValue;
    private float                currentValue;
    private float                minValue;
    private float                maxValue;
    private float                range;
    private float                threshold;
    private boolean              thresholdVisible;
    private float                minMeasuredValue;
    private boolean              minMeasuredValueVisible;
    private float                maxMeasuredValue;
    private boolean              maxMeasuredValueVisible;
    private int                  decimals;
    private String               title;
    private String               unit;
    private boolean              animated;
    private long                 animationDuration;
    private float                startAngle;
    private float                angleRange;
    private float                angleStep;
    private boolean              autoScale;
    private int                  textColor;
    private int                  needleColor;
    private int                  tickMarkColor;
    private int                  tickLabelColor;
    private TickLabelOrientation tickLabelOrientation;
    private NumberFormat         numberFormat;
    private List<Section>        sections;
    private boolean              sectionsVisible;
    private List<Section>        areas;
    private boolean              areasVisible;
    private List<Marker>         markers;
    private boolean              markersVisible;
    private float                majorTickSpace;
    private float                minorTickSpace;
    private Limit                limit;

    private RectF         sectionBounds;
    private RectF         areaBounds;
    private Paint         backgroundPaint;
    private Paint         tickMarksPaint;
    private Paint         needlePaint;
    private Paint         valuePaint;
    private Paint         foregroundPaint;
    private Path          outerBorder;
    private Path          hightlightBorder;
    private Path          innerBorder;
    private Path          body;
    private Path          background;
    private Path          majorTickMarks;
    private Path          middleTickMarks;
    private Path          minorTickMarks;
    private Path          needleShadow;
    private Path          needle;
    private Path          knobShadow;
    private Path          knobFrame;
    private Path          knob;
    private ValueAnimator animator;


    // ******************** Constructors **************************************
    public Gauge(final Context CONTEXT, final AttributeSet ATTRS) {
        this(CONTEXT, ATTRS, 0);
    }
    public Gauge(final Context CONTEXT, final AttributeSet ATTRS, final int DEF_STYLE) {
        super(CONTEXT, ATTRS, DEF_STYLE);
        sectionBounds        = new RectF(LEFT, TOP, RIGHT, BOTTOM);
        areaBounds           = new RectF(LEFT, TOP, RIGHT, BOTTOM);
        outerBorder          = new Path();
        hightlightBorder     = new Path();
        innerBorder          = new Path();
        body                 = new Path();
        background           = new Path();
        majorTickMarks       = new Path();
        middleTickMarks      = new Path();
        minorTickMarks       = new Path();
        needleShadow         = new Path();
        needle               = new Path();
        knobShadow           = new Path();
        knobFrame            = new Path();
        knob                 = new Path();
        title                = "";
        unit                 = "";
        sections             = new ArrayList<Section>(10);
        areas                = new ArrayList<Section>(10);
        textColor            = Util.darker(Color.WHITE, 0.2f);
        startAngle           = START_ANGLE;
        angleRange           = ANGLE_RANGE;
        numberFormat         = NumberFormat.STANDARD;
        tickLabelOrientation = TickLabelOrientation.HORIZONTAL;
        majorTickSpace       = 10;
        minorTickSpace       = 1;
        autoScale            = false;
        animator             = ValueAnimator.ofFloat(minValue, minValue);
        initAttributes(CONTEXT, ATTRS, DEF_STYLE);
        init();
    }


    // ******************** Initialization ************************************
    private void initAttributes(final Context context, final AttributeSet attrs, final int defStyle) {
        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Gauge, defStyle, 0);

        minValue          = a.getFloat(R.styleable.Gauge_minValueGauge, MIN_VALUE);
        maxValue          = a.getFloat(R.styleable.Gauge_maxValueGauge, MAX_VALUE);
        value             = a.getFloat(R.styleable.Gauge_valueGauge, minValue);
        title             = a.getString(R.styleable.Gauge_titleGauge) == null ? "" : a.getString(R.styleable.Gauge_titleGauge);
        unit              = a.getString(R.styleable.Gauge_unitGauge) == null ? "" : a.getString(R.styleable.Gauge_unitGauge);
        animated          = a.getBoolean(R.styleable.Gauge_animatedGauge, true);
        animationDuration = a.getInt(R.styleable.Gauge_animationDurationGauge, 1500);
        decimals          = a.getInt(R.styleable.Gauge_decimalsGauge, 0);
        needleColor       = a.getInt(R.styleable.Gauge_needleColorGauge, Color.parseColor("#F8907D"));
        tickMarkColor     = a.getColor(R.styleable.Gauge_tickMarkColorGauge, Color.rgb(0, 0, 0));
        tickLabelColor    = a.getColor(R.styleable.Gauge_tickLabelColorGauge, Color.rgb(0, 0, 0));

        a.recycle();

        range     = maxValue - minValue;
        angleStep = ANGLE_RANGE / range;
    }
    private void init() {
        initGraphics();
        initAnimators();
    }
    private void initGraphics() {
        backgroundPaint    = getBackgroundPaint();
        tickMarksPaint     = getTickMarksPaint();
        needlePaint        = getNeedlePaint();
        valuePaint         = getValuePaint();
        foregroundPaint    = getForegroundPaint();
    }
    private void initAnimators() {
        animator.setDuration(animationDuration);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.addUpdateListener(new AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {
                value = (Float) animation.getAnimatedValue();
                limit = value < minValue ? Limit.UNDERRUN : value > getMaxValue() ? Limit.EXCEEDED : Limit.IN_RANGE;
                invalidate();
            }
        });
    }


    // ******************** Graphics related **********************************
    private Paint getBackgroundPaint() {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        return paint;
    }
    private Paint getTickMarksPaint() {
        Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Style.FILL_AND_STROKE);
        paint.setTextAlign(Align.CENTER);
        return paint;    
    }
    private Paint getNeedlePaint() {
        final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        return paint;
    }
    private Paint getValuePaint() {
        final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setColor(textColor);
        paint.setStyle(Style.FILL);
        return paint;
    }
    private Paint getForegroundPaint() {
        final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Style.FILL);
        return paint;
    }


    // ******************** State related *************************************
    @Override protected void onRestoreInstanceState(final Parcelable state) {
        final Bundle bundle     = (Bundle) state;
        final Parcelable superState = bundle.getParcelable("superState");
        super.onRestoreInstanceState(superState);
        value            = bundle.getFloat("value");
        minMeasuredValue = bundle.getFloat("minMeasuredValue");
        maxMeasuredValue = bundle.getFloat("maxMeasuredValue");
        threshold        = bundle.getFloat("threshold");
        title            = bundle.getString("title");
    }
    @Override protected Parcelable onSaveInstanceState() {
        final Parcelable superState = super.onSaveInstanceState();
        final Bundle     state      = new Bundle();
        state.putParcelable("superState", superState);
        state.putFloat("value", value);
        state.putFloat("minMeasuredValue", minMeasuredValue);
        state.putFloat("maxMeasuredValue", maxMeasuredValue);
        state.putFloat("threshold", threshold);
        state.putString("title", title);
        return state;
    }


    // ******************** Size related **************************************
    @Override protected void onMeasure(final int WIDTH_MEASURE_SPEC, final int HEIGHT_MEASURE_SPEC) {
        // Loggable.log.debug(String.format("WIDTH_MEASURE_SPEC=%s, HEIGHT_MEASURE_SPEC=%s",
        // View.MeasureSpec.toString(WIDTH_MEASURE_SPEC),
        // View.MeasureSpec.toString(HEIGHT_MEASURE_SPEC)));

        final int WIDTH_MODE  = MeasureSpec.getMode(WIDTH_MEASURE_SPEC);
        final int HEIGHT_MODE = MeasureSpec.getMode(HEIGHT_MEASURE_SPEC);
        final int WIDTH_SIZE  = MeasureSpec.getSize(WIDTH_MEASURE_SPEC);
        final int HEIGHT_SIZE = MeasureSpec.getSize(HEIGHT_MEASURE_SPEC);

        final int CHOSEN_WIDTH  = chooseWidth(WIDTH_MODE, WIDTH_SIZE);
        final int CHOSEN_HEIGHT = chooseHeight(HEIGHT_MODE, HEIGHT_SIZE);
        setMeasuredDimension(CHOSEN_WIDTH, CHOSEN_HEIGHT);
    }

    private int chooseWidth(final int MODE, final int WIDTH) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return WIDTH;
            case View.MeasureSpec.UNSPECIFIED:
            default                          :return DEFAULT_WIDTH;
        }
    }
    private int chooseHeight(final int MODE, final int HEIGHT) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return HEIGHT;
            case View.MeasureSpec.UNSPECIFIED:
            default                          : return DEFAULT_HEIGHT;
        }
    }

    @Override protected void onSizeChanged(final int WIDTH, final int HEIGHT, final int OLD_WIDTH, final int OLD_HEIGHT) { resize(); }

    private void resize() {
        final Canvas CANVAS = new Canvas();
        final float scale = Math.min(getWidth(), getHeight());
        size   = getWidth() < getHeight() ? getWidth() : getHeight();
        center = 0.5f * size;

        CANVAS.scale(scale, scale);
        CANVAS.translate((scale == size) ? ((size - scale) / 2) / scale : 0, (scale == size) ? ((size - scale) / 2) / scale : 0);

        sectionBounds.set(0.085f * size, 0.085f * size, 0.915f * size, 0.915f * size);
        areaBounds.set(0.104f * size, 0.104f * size, 0.896f * size, 0.896f * size);

        resizeBackground();
        resizeTicksAndSections();
        resizeNeedle();
        resizeForeground();
    }
    private void resizeBackground() {
        outerBorder.reset();
        outerBorder.addCircle(center, center, 0.5f * size, Direction.CW);
        outerBorder.close();

        hightlightBorder.reset();
        hightlightBorder.addCircle(center, center, 0.485f * size, Direction.CW);
        hightlightBorder.close();

        innerBorder.reset();
        innerBorder.addCircle(center, center, 0.4825f * size, Direction.CW);
        innerBorder.close();

        body.reset();
        body.addCircle(center, center, 0.4675f * size, Direction.CW);
        body.close();

        background.reset();
        background.addCircle(center, center, 0.28f * size, Direction.CW);
        background.close();

        backgroundPaint.setTextSize(0.06f * size);
        backgroundPaint.setTextAlign(Align.CENTER);
    }
    private void resizeTicksAndSections() {
        tickMarksPaint.setTextSize(0.045f * size);

        majorTickMarks.reset();
        middleTickMarks.reset();
        minorTickMarks.reset();
        float sinValue;
        float cosValue;
        for (float angle = 0, counter = minValue ; counter <= maxValue ; angle -= angleStep, counter++) {
            sinValue = (float) Math.sin((angle - startAngle + 100) * Math.PI / 180);
            cosValue = (float) Math.cos((angle - startAngle + 100) * Math.PI / 180);
            Point innerMainPoint   = new Point( (int) (center + size * 0.368 * sinValue), (int) (center + size * 0.368 * cosValue));
            Point innerMediumPoint = new Point( (int) (center + size * 0.388 * sinValue), (int) (center + size * 0.388 * cosValue));
            Point innerMinorPoint  = new Point( (int) (center + size * 0.3975 * sinValue), (int) (center + size * 0.3975 * cosValue));
            Point outerPoint       = new Point( (int) (center + size * 0.432 * sinValue), (int) (center + size * 0.432 * cosValue));

            if (counter % majorTickSpace == 0) {
                majorTickMarks.moveTo(innerMainPoint.x, innerMainPoint.y);
                majorTickMarks.lineTo(outerPoint.x, outerPoint.y);
            } else if (minorTickSpace % 2 != 0 && counter % 5 == 0) {
                middleTickMarks.moveTo(innerMediumPoint.x, innerMediumPoint.y);
                middleTickMarks.lineTo(outerPoint.x, outerPoint.y);
            } else if (counter % minorTickSpace == 0) {
                minorTickMarks.moveTo(innerMinorPoint.x, innerMinorPoint.y);
                minorTickMarks.lineTo(outerPoint.x, outerPoint.y);
            }
        }
    }
    private void resizeNeedle() {
        needleShadow.reset();
        needleShadow.moveTo(0.49f * size, 0.075f * size);
        needleShadow.lineTo(0.475f * size, 0.5f * size);
        needleShadow.lineTo(0.525f * size, 0.5f * size);
        needleShadow.lineTo(0.51f * size, 0.075f * size);
        needleShadow.lineTo(0.49f * size, 0.075f * size);
        needleShadow.close();

        needle.reset();
        needle.moveTo(0.49524f * size, 0.085f * size);
        needle.lineTo(0.48008f * size, 0.5f * size);
        needle.lineTo(0.51992f * size, 0.5f * size);
        needle.lineTo(0.50476f * size, 0.085f * size);
        needle.lineTo(0.49524f * size, 0.085f * size);
        needle.close();
    }
    private void resizeForeground() {
        knobShadow.reset();
        knobShadow.addCircle(center, center, 0.18f * size, Direction.CW);
        knobShadow.close();

        knobFrame.reset();
        knobFrame.addCircle(center, center, 0.175f * size, Direction.CW);
        knobFrame.close();

        knob.reset();
        knob.addCircle(center, center, 0.165f * size, Direction.CW);
        knob.close();

        foregroundPaint.setTextAlign(Align.CENTER);
    }


    // ******************** Drawing related ***********************************
    @Override protected void onDraw(final Canvas CANVAS) {
        drawBackground(CANVAS);
        drawTicksAndSections(CANVAS);
        drawNeedle(CANVAS);
        drawForeground(CANVAS);
        drawValue(CANVAS);
    }

    private void drawBackground(final Canvas CANVAS) {
        CANVAS.save();

        // Frame
        int[]          colors;
        float[]        fractions;
        LinearGradient gradient;

        colors    = new int[] { Color.rgb(224, 224, 224),  Color.rgb(133, 133, 133), Color.rgb(84, 84, 84) };
        fractions = new float[] { 0, 0.26f, 1f };
        gradient  = new LinearGradient(0, 0, 0, size, colors, fractions, TileMode.CLAMP);
        backgroundPaint.setShader(gradient);
        CANVAS.drawPath(outerBorder, backgroundPaint);

        colors    = new int[] { Color.rgb(255, 255, 255),  Color.rgb(146, 146, 147), Color.rgb(135, 136, 138) };
        fractions = new float[] { 0, 0.5f, 1f };
        gradient  = new LinearGradient(0, 0.015f * size, 0, 0.9985f * size, colors, fractions, TileMode.CLAMP);
        backgroundPaint.setShader(gradient);
        CANVAS.drawPath(hightlightBorder, backgroundPaint);

        colors    = new int[] { Color.rgb(71, 72, 72), Color.rgb(110, 106, 107), Color.rgb(186, 185, 187) };
        fractions = new float[] { 0, 0.5f, 1f };
        gradient  = new LinearGradient(0, 0.0175f * size, 0, 0.99825f * size, colors, fractions, TileMode.CLAMP);
        backgroundPaint.setShader(gradient);
        CANVAS.drawPath(innerBorder, backgroundPaint);

        // Body
        colors    = new int[] { Color.rgb(245, 245, 245),  Color.rgb(235, 235, 235) };
        fractions = new float[] { 0, 1f };
        gradient  = new LinearGradient(0, 0.0375f * size, 0, 0.9625f * size, colors, fractions, TileMode.CLAMP);
        backgroundPaint.setShader(gradient);
        CANVAS.drawPath(body, backgroundPaint);

        // Inner Shadow
        colors    = new int[] { Color.TRANSPARENT, Color.TRANSPARENT, Color.argb(25, 0, 0, 0), Color.argb(65, 0, 0, 0) };
        fractions = new float[] { 0, 0.9f, 0.95f, 1f };
        backgroundPaint.setShader(new RadialGradient(0.5f * size, 0.525f * size, 0.4625f * size, colors, fractions, TileMode.CLAMP));
        CANVAS.drawPath(body, backgroundPaint);

        CANVAS.restore();
    }
    private void drawTicksAndSections(final Canvas CANVAS) {
        CANVAS.save();

        // Sections
        tickMarksPaint.setStyle(Style.STROKE);
        float sectionStartAngle;
        float sectionStopAngle;
        float offset = 10;
        for (Section section : sections) {
            sectionStartAngle = (startAngle + (angleStep * section.getStart())) - offset;
            sectionStopAngle  = ((section.getStop() - section.getStart()) * angleStep);
            tickMarksPaint.setStrokeWidth(0.037f * size);
            tickMarksPaint.setColor(section.getColor());
            CANVAS.drawArc(sectionBounds, sectionStartAngle, sectionStopAngle, false, tickMarksPaint);
        }

        // Areas
        tickMarksPaint.setStyle(Style.FILL);
        float areaStartAngle;
        float areaStopAngle;
        for (Section area : areas) {
            areaStartAngle = (startAngle + (angleStep * area.getStart())) - offset;
            areaStopAngle  = ((area.getStop() - area.getStart()) * angleStep);
            tickMarksPaint.setColor(area.getColor());
            CANVAS.drawArc(areaBounds, areaStartAngle, areaStopAngle, true, tickMarksPaint);
        }

        // Tickmarks
        tickMarksPaint.setStyle(Style.FILL_AND_STROKE);
        tickMarksPaint.setColor(tickMarkColor);
        tickMarksPaint.setStrokeWidth(size * 0.0055f);
        CANVAS.drawPath(majorTickMarks, tickMarksPaint);
        tickMarksPaint.setStrokeWidth(size * 0.0035f);
        CANVAS.drawPath(middleTickMarks, tickMarksPaint);
        tickMarksPaint.setStrokeWidth(size * 0.00225f);
        CANVAS.drawPath(minorTickMarks, tickMarksPaint);

        // Tick Label
        tickMarksPaint.setTypeface(ROBOTO_THIN);
        tickMarksPaint.setColor(tickLabelColor);

        float sinValue;
        float cosValue;
        float orthText = TickLabelOrientation.ORTHOGONAL == tickLabelOrientation ? 0.33f : 0.31f;
        for (float angle = 0, counter = minValue ; counter <= maxValue ; angle -= angleStep, counter++) {
            sinValue        = (float) Math.sin((angle - startAngle + 90 + offset) * Math.PI / 180);
            cosValue        = (float) Math.cos((angle - startAngle + 90 + offset) * Math.PI / 180);
            Point textPoint = new Point( (int) (center + size * orthText * sinValue), (int) (center + size * orthText * cosValue));

            if (counter % majorTickSpace == 0) {
                // Draw text
                CANVAS.save();
                CANVAS.translate(textPoint.x, textPoint.y);
                switch(tickLabelOrientation) {
                    case ORTHOGONAL:
                        if ((360 - startAngle - angle) % 360 > 90 && (360 - startAngle - angle) % 360 < angleRange) {
                            CANVAS.rotate((180 - startAngle - angle) % 360);
                        } else {
                            CANVAS.rotate((360 - startAngle - angle) % 360);
                        }
                        break;
                    case TANGENT:
                        if ((360 - startAngle - angle - 90) % 360 > 90 && (360 - startAngle - angle - 90) % 360 < angleRange) {
                            CANVAS.rotate((90 - startAngle - angle) % 360);
                        } else {
                            CANVAS.rotate((angleRange - startAngle - angle) % 360);
                        }
                        break;
                    case HORIZONTAL:
                    default:
                        break;
                }
                CANVAS.drawText(Integer.toString((int) counter), 0f, 0f, tickMarksPaint);
                CANVAS.restore();
            }
        }

        CANVAS.restore();
    }
    private void drawNeedle(final Canvas CANVAS) {
        CANVAS.save();
        CANVAS.rotate(angleStep * (value - minValue) - startAngle, size * 0.5f, size * 0.5f);

        needlePaint.setStyle(Style.FILL);
        int[]          colors;
        float[]        fractions;
        LinearGradient gradient;

        // Needle Shadow
        colors    = new int[] { Color.TRANSPARENT, Color.argb(65, 0, 0, 0), Color.TRANSPARENT };
        fractions = new float[] { 0, 0.5f, 1f };
        gradient  = new LinearGradient(0.475f * size, 0, 0.525f * size, 0, colors, fractions, TileMode.CLAMP);
        needlePaint.setShader(gradient);
        CANVAS.drawPath(needleShadow, needlePaint);

        // Needle
        colors    = new int[] { Util.brighter(needleColor, 0.15f),  Util.darker(needleColor, 0.15f) };
        fractions = new float[] { 0, 1f };
        gradient  = new LinearGradient(0, 0.13f * size, 0, 0.5f * size, colors, fractions, TileMode.CLAMP);
        needlePaint.setShader(gradient);
        CANVAS.drawPath(needle, needlePaint);

        // Needle Highlight
        colors    = new int[] { Color.TRANSPARENT, Color.TRANSPARENT, Color.argb(51, 255, 255, 255),  Color.argb(51, 255, 255, 255) };
        fractions = new float[] { 0, 0.5f, 0.5000001f, 1f };
        gradient  = new LinearGradient(0.48008f * size, 0, 0.51992f * size, 0, colors, fractions, TileMode.CLAMP);
        needlePaint.setShader(gradient);
        CANVAS.drawPath(needle, needlePaint);

        CANVAS.restore();
    }
    private void drawForeground(final Canvas CANVAS) {
        CANVAS.save();

        int[]          colors;
        float[]        fractions;
        LinearGradient gradient;

        // DropShadow
        colors    = new int[] { Color.argb(65, 0, 0, 0), Color.argb(65, 0, 0, 0), Color.argb(25, 0, 0, 0), Color.TRANSPARENT };
        fractions = new float[] { 0, 0.9f, 0.95f, 1f };
        foregroundPaint.setShader(new RadialGradient(0.5f * size, 0.55f * size, 0.18f * size, colors, fractions, TileMode.CLAMP));
        CANVAS.drawPath(knobShadow, foregroundPaint);

        // Knob Frame
        colors    = new int[] { Color.rgb(255, 255, 255), Color.rgb(230, 230, 230), Color.rgb(240, 240, 240)};
        fractions = new float[] { 0, 0.52f, 1f };
        gradient  = new LinearGradient(0, 0.325f * size, 0, 0.675f * size, colors, fractions, TileMode.CLAMP);
        foregroundPaint.setShader(gradient);
        CANVAS.drawPath(knobFrame, foregroundPaint);

        // Knob
        colors    = new int[] { Color.rgb(250, 250, 250),  Color.rgb(230, 230, 230)};
        fractions = new float[] { 0, 1f};
        gradient  = new LinearGradient(0, 0.335f * size, 0, 0.665f * size, colors, fractions, TileMode.CLAMP);
        foregroundPaint.setShader(gradient);
        CANVAS.drawPath(knob, foregroundPaint);

        CANVAS.restore();
    }
    private void drawValue(final Canvas CANVAS) {
        // Value
        valuePaint.setTypeface(ROBOTO_MEDIUM);
        valuePaint.setColor(textColor);
        valuePaint.setTextSize(0.1f * size);
        valuePaint.setTextAlign(Align.CENTER);
        int xPos = (int) (0.5f * size);
        int yPos = (int) ((0.5f * size) - ((valuePaint.descent() + valuePaint.ascent()) * 0.5f)) ;
        CANVAS.drawText(String.format(Locale.US, "%." + decimals + "f", value), xPos, yPos, valuePaint);

        // Unit
        valuePaint.setTypeface(ROBOTO_REGULAR);
        valuePaint.setTextSize(0.045f * size);
        yPos = (int) ((0.4f * size) - ((valuePaint.descent() + valuePaint.ascent()) * 0.5f)) ;
        CANVAS.drawText(unit, xPos, yPos, valuePaint);

        // Title
        valuePaint.setTypeface(ROBOTO_REGULAR);
        valuePaint.setTextSize(0.006f * size);
        yPos = (int) ((0.84f * size) - ((valuePaint.descent() + valuePaint.ascent()) * 0.5f)) ;
        CANVAS.drawText(title, xPos, yPos, valuePaint);
    }


    // ******************** Methods *******************************************
    public final float getValue() { return value; }
    public final void setValue(final float VALUE) {
        if (isInteractive()) return;
        oldValue = value;
        if (animated) {
            currentValue = Util.clamp(minValue, maxValue, VALUE);
            if (animator.isRunning()) animator.cancel();
            animator.setFloatValues(oldValue, currentValue);
            animator.start();
        } else {
            value = Util.clamp(minValue, maxValue, VALUE);
            limit = value < minValue ? Limit.UNDERRUN : value > getMaxValue() ? Limit.EXCEEDED : Limit.IN_RANGE;
            for (Section section : sections) {
                if (section.contains(value)) {
                    section.fireSectionEvent(new SectionEvent(Gauge.this, SectionEventType.ENTERING_SECTION));
                    break;
                }
            }
            invalidate();
        }
    }

    public final float getMinValue() { return minValue; }
    public final void setMinValue(final float MIN_VALUE) {
        minValue = MIN_VALUE;
        calcAutoScale();
    }

    public final float getMaxValue() { return maxValue; }
    public final void setMaxValue(final float MAX_VALUE) {
        maxValue = MAX_VALUE;
        calcAutoScale();
    }

    public final float getThreshold() { return threshold; }
    public final void setThreshold(final float THRESHOLD) { threshold = Util.clamp(minValue, maxValue, THRESHOLD); }

    public final float getMinMeasuredValue() { return minMeasuredValue; }
    public final void setMinMeasuredValue(final float MIN_MEASURED_VALUE) {
        minMeasuredValue = MIN_MEASURED_VALUE;
        invalidate();
    }

    public final float getMaxMeasuredValue() { return maxMeasuredValue; }
    public final void setMaxMeasuredValue(final float MAX_MEASURED_VALUE) {
        maxMeasuredValue = MAX_MEASURED_VALUE;
        invalidate();
    }

    public void resetMinMeasuredValue() { setMinMeasuredValue(getValue()); }
    public void resetMaxMeasuredValue() { setMaxMeasuredValue(getValue()); }
    public void resetMinAndMaxMeasuredValue() {
        setMinMeasuredValue(getValue());
        setMaxMeasuredValue(getValue());
    }

    public final int getDecimals() { return decimals; }
    public final void setDecimals(final int DECIMALS) {
        decimals = Util.clamp(0, 3, DECIMALS);
        invalidate();
    }

    public final String getTitle() { return title; }
    public final void setTitle(final String TITLE) {
        title = TITLE;
        invalidate();
    }

    public final String getUnit() { return unit; }
    public final void setUnit(final String UNIT) {
        unit = UNIT;
        invalidate();
    }

    public final boolean isAnimated() { return animated; }
    public final void setAnimated(final boolean ANIMATED) { animated = ANIMATED; }

    public float getStartAngle() { return startAngle; }
    public final void setStartAngle(final float START_ANGLE) {
        startAngle = Util.clamp(0f, 360f, START_ANGLE);
        invalidate();
    }

    public final float getAnimationDuration() { return animationDuration; }
    public final void setAnimationDuration(final long ANIMATION_DURATION) { animationDuration = Util.clamp(20, 5000, ANIMATION_DURATION); }

    public final float getAngleRange() { return angleRange; }
    public final void setAngleRange(final float ANGLE_RANGE) {
        angleRange = Util.clamp(0f, 360f, ANGLE_RANGE);
        invalidate();
    }

    public final boolean isAutoScale() { return autoScale; }
    public final void setAutoScale(final boolean AUTO_SCALE) {
        autoScale = AUTO_SCALE;
        calcAutoScale();
    }

    public final boolean isInteractive() { return interactive; }
    public final void setInteractive(final boolean INTERACTIVE) {
        interactive = INTERACTIVE;
        invalidate();
    }

    public final List<Section> getSections() { return sections; }
    public final void setSections(final Section... SECTIONS) {
        sections.clear();
        for (Section section : SECTIONS) {
            sections.add(section);
        }
    }
    public final void addSection(final Section SECTION) {
        if (sections.contains(SECTION)) return;
        sections.add(SECTION);
    }
    public final void removeSection(final Section SECTION) {
        if (sections.contains(SECTION)) sections.remove(SECTION);
    }

    public final List<Section> getAreas() { return areas; }
    public final void setAreas(final Section... AREAS) {
        areas.clear();
        for (Section area : AREAS) {
            areas.add(area);
        }
    }
    public final void addArea(final Section AREA) {
        if (areas.contains(AREA)) return;
        areas.add(AREA);
    }
    public final void removeArea(final Section AREA) {
        if (areas.contains(AREA)) areas.remove(AREA);
    }



    // ******************** Utility methods ***********************************
    private String valueString(final float VALUE) { return String.format(Locale.US, "%." + Integer.toString(decimals) + "f", VALUE); }

    private void calcAutoScale() {
        if (autoScale) {
            int   maxNoOfMajorTicks = 10;
            int   maxNoOfMinorTicks = 10;
            double niceMinValue;
            double niceMaxValue;
            double niceRange        = calcNiceNumber((maxValue - minValue), false);
            majorTickSpace          = (float) calcNiceNumber(niceRange / (maxNoOfMajorTicks - 1), true);
            niceMinValue            = (float) Math.floor(minValue / majorTickSpace) * majorTickSpace;
            niceMaxValue            = (float) Math.ceil(maxValue / majorTickSpace) * majorTickSpace;
            minorTickSpace          = (float) calcNiceNumber(majorTickSpace / (maxNoOfMinorTicks - 1), true);
            minValue                = (float) niceMinValue;
            maxValue                = (float) niceMaxValue;
            invalidate();
        }
    };
    private double calcNiceNumber (final double RANGE, final boolean ROUND) {
        double exponent = Math.floor(Math.log10(RANGE));   // exponent of RANGE
        double fraction = RANGE / Math.pow(10, exponent);  // fractional part of RANGE
        double niceFraction;

        if (ROUND) {
            if (fraction < 1.5) {
                niceFraction = 1;
            } else if (fraction < 3) {
                niceFraction = 2;
            } else if (fraction < 7) {
                niceFraction = 5;
            } else {
                niceFraction = 10;
            }
        } else {
            if (fraction <= 1) {
                niceFraction = 1;
            } else if (fraction <= 2) {
                niceFraction = 2;
            } else if (fraction <= 5) {
                niceFraction = 5;
            } else {
                niceFraction = 10;
            }
        }
        return niceFraction * Math.pow(10, exponent);
    }
}
