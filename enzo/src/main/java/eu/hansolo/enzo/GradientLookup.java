package eu.hansolo.enzo;

import android.graphics.Color;
import android.util.Log;

import java.util.*;


/**
 * Created by hansolo on 17.04.15.
 */
public class GradientLookup {
    private Map<Float, Stop> stops;


    // ******************** Constructors **************************************
    public GradientLookup(final Stop... STOPS) {
        stops = new TreeMap<Float, Stop>();
        for (Stop stop : STOPS) { stops.put(stop.getOffset(), stop); }
        init();
    }

    // ******************** Initialization ************************************
    private void init() {
        if (stops.isEmpty()) return;
        float minFraction = Collections.min(stops.keySet());
        float maxFraction = Collections.max(stops.keySet());

        if (minFraction > 0) { stops.put(0.0f, new Stop(0.0f, stops.get(minFraction).getColor())); }
        if (maxFraction < 1) { stops.put(1.0f, new Stop(1.0f, stops.get(maxFraction).getColor())); }
    }


    // ******************** Methods *******************************************
    public int getColorAt(final float POSITION_OF_COLOR) {
        if (stops.isEmpty()) return Color.rgb(0, 0, 0);
        final float POSITION = POSITION_OF_COLOR < 0 ? 0 : (POSITION_OF_COLOR > 1 ? 1 : POSITION_OF_COLOR);
        final int COLOR;
        if (stops.size() == 1) {
            final Map<Float, Color> ONE_ENTRY = (Map<Float, Color>) stops.entrySet().iterator().next();
            COLOR = stops.get(ONE_ENTRY.keySet().iterator().next()).getColor();
        } else {
            Stop lowerBound = stops.get(0f);
            Stop upperBound = stops.get(1f);
            for (Float fraction : stops.keySet()) {
                if (Float.compare(fraction,POSITION) < 0) {
                    lowerBound = stops.get(fraction);
                }
                if (Float.compare(fraction, POSITION) > 0) {
                    upperBound = stops.get(fraction);
                    break;
                }
            }
            COLOR = interpolateColor(lowerBound, upperBound, POSITION);
        }
        return COLOR;
    }

    public List<Stop> getStops() { return new ArrayList<Stop>(stops.values()); }
    public void setStops(final Stop... STOPS) {
        stops.clear();
        for (Stop stop : STOPS) {
            stops.put(stop.getOffset(), stop);
        }
        init();
    }

    private int interpolateColor(final Stop LOWER_BOUND, final Stop UPPER_BOUND, final float POSITION) {
        final float POS  = (POSITION - LOWER_BOUND.getOffset()) / (UPPER_BOUND.getOffset() - LOWER_BOUND.getOffset());
        return Util.interpolateColor(LOWER_BOUND.getColor(), UPPER_BOUND.getColor(), POS);
    }
}
