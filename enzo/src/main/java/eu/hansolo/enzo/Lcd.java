package eu.hansolo.enzo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.*;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Shader.TileMode;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import eu.hansolo.enzo.SectionEvent.SectionEventType;

import java.util.Locale;


public class Lcd extends View {
    private static final String TAG = "LCD";
    public enum Trend { UP, RISING, STEADY, FALLING, DOWN }
    public enum Design {
        BEIGE("beige",1),
        BLUE("blue",2),
        ORANGE("orange",3),
        RED("red",4),
        YELLOW("yellow",5),
        WHITE("white",6),
        GRAY("gray",7),
        BLACK("black",8),
        GREEN("green",9),
        GREEN_DARKGREEN("green_darkgreen",10),
        BLUE2("blue2",11),
        BLUE_BLACK("blue_black",12),
        BLUE_DARKBLUE("blue_darkblue",13),
        BLUE_LIGHTBLUE("blue_lightblue",14),
        BLUE_GRAY("blue_gray",15),
        STANDARD("standard",16),
        LIGHTGREEN("lightgreen",17),
        STANDARD_GREEN("standard_green",18),
        BLUE_BLUE("blue_blue",19),
        RED_DARKRED("red_darkred",20),
        DARKBLUE("darkblue",21),
        PURPLE("purple",22),
        BLACK_RED("black_red",23),
        DARKGREEN("darkgreen",24),
        AMBER("amber",25),
        LIGHTBLUE("lightblue",26),
        GREEN_BLACK("green_black",27),
        YELLOW_BLACK("yellow_black",28),
        BLACK_YELLOW("black_yellow",29),
        LIGHTGREEN_BLACK("lightgreen_black",30),
        DARKPURPLE("darkpurple",31),
        DARKAMBER("darkamber",32),
        BLUE_LIGHTBLUE2("blue_lightblue2",33),
        GRAY_PURPLE("gray_purple",34),
        FLAT_TURQOISE("flat_turqoise",35),
        FLAT_GREEN_SEA("flat_green_sea",36),
        FLAT_EMERLAND("flat_emerland",37),
        FLAT_NEPHRITIS("flat_nephritis",38),
        FLAT_PETER_RIVER("flat_peter_river",39),
        FLAT_BELIZE_HOLE("flat_belize_hole",40),
        FLAT_AMETHYST("flat_amethyst",41),
        FLAT_WISTERIA("flat_wisteria",42),
        FLAT_SUNFLOWER("flat_sunflower",43),
        FLAT_ORANGE("flat_orange",44),
        FLAT_CARROT("flat_carrot",45),
        FLAT_PUMPKIN("flat_pumpkin",46),
        FLAT_ALIZARIN("flat_alizarin",47),
        FLAT_POMEGRANATE("flat_pomegranate",48),
        FLAT_CLOUDS("flat_clouds",49),
        FLAT_SILVER("flat_silver",50),
        FLAT_CONCRETE("flat_concrete",51),
        FLAT_ASBESTOS("flat_asbestos",52),
        FLAT_WET_ASPHALT("flat_wet_asphalt",53),
        FLAT_MIDNIGHT_BLUE("flat_midnight_blue",54);
        
        public final String NAME;
        public final int    NUMBER;
        
        Design(final String NAME, final int NUMBER) {
            this.NAME   = NAME;
            this.NUMBER = NUMBER;
        }
    }
    
    public static final int   DEFAULT_WIDTH  = 264;
    public static final int   DEFAULT_HEIGHT = 96;
    public static final float TOP            = 0.0f;
    public static final float LEFT           = 0.0f;
    public static final float RIGHT          = 1.0f;
    public static final float BOTTOM         = 1.0f;
    public static final float CENTER         = 0.5f;
    public static final float MIN_VALUE      = 0.0f;
    public static final float MAX_VALUE      = 100.0f;

    private float         value;
    private float         oldValue;
    private float         formerValue;
    private float         currentValue;
    private float         minValue;
    private float         maxValue;
    private String        upperCenterText;
    private boolean       upperCenterTextVisible;
    private String        unit;
    private boolean       unitVisible;
    private String        lowerRightText;
    private boolean       lowerRightTextVisible;
    private int           decimals;
    private float         threshold;
    private boolean       thresholdVisible;
    private boolean       alarmVisible;
    private boolean       signalVisible;
    private float         signalStrength;
    private boolean       thresholdExceeded;
    private float         upperLeftText;
    private boolean       upperLeftTextVisible;
    private float         upperRightText;
    private boolean       upperRightTextVisible;
    private String        lowerCenterText;
    private boolean       lowerCenterTextVisible;
    private boolean       formerValueVisible;
    private Trend         trend;
    private boolean       trendVisible;
    private int           design;
    private int           backgroundColor;
    private int           foregroundColor;
    private RectF         frameRect;
    private RectF         backgroundRect;
    private Paint         framePaint;
    private Paint         backgroundPaint;
    private Paint         backgroundTextPaint;
    private Paint         valuePaint;
    private Paint         unitPaint;
    private Paint         smallValuePaint;
    private Paint         iconPaint;
    private Path          thresholdIcon;
    private Path          trendDownIcon;
    private Path          trendFallingIcon;
    private Path          trendSteadyIcon;
    private Path          trendRisingIcon;
    private Path          trendUpIcon;
    private Path          alarmIcon;
    private Path          signalIcon;
    private float[]       frameFractions      = {0.0f, 0.01f, 0.83f, 1.0f};
    private int[]         frameColors         = {Color.rgb(26, 26, 26), Color.rgb(77, 77, 77), Color.rgb(77, 77, 77), Color.rgb(221, 221, 221)};
    private float[]       backgroundFractions = {0f, 0.005f, 0.5f, 0.501f, 1f};
    private ValueAnimator animator;
    private boolean       animated;
    private long          animationDuration;


    // ******************** Constructors **************************************
    public Lcd(final Context CONTEXT, final AttributeSet ATTRS) {
        this(CONTEXT, ATTRS, 0);
    }
    public Lcd(final Context CONTEXT, final AttributeSet ATTRS, final int DEF_STYLE) {
        super(CONTEXT, ATTRS, DEF_STYLE);
        thresholdExceeded = false;
        formerValue       = 0f;
        animator          = ValueAnimator.ofFloat(minValue, minValue);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        initAttributes(CONTEXT, ATTRS, DEF_STYLE);
        init();
    }


    // ******************** Initialization ************************************
    private void initAttributes(final Context context, final AttributeSet attrs, final int defStyle) {
        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Lcd, defStyle, 0);
        animated               = a.getBoolean(R.styleable.Lcd_animated, true);
        animationDuration      = a.getInt(R.styleable.Lcd_animationDuration, 800);
        minValue               = a.getFloat(R.styleable.Lcd_minValue, MIN_VALUE);
        maxValue               = a.getFloat(R.styleable.Lcd_maxValue, MAX_VALUE);
        upperCenterText        = a.getString(R.styleable.Lcd_upperCenterText) == null ? "" : a.getString(R.styleable.Lcd_upperCenterText);
        upperCenterTextVisible = a.getBoolean(R.styleable.Lcd_upperCenterTextVisible, false);
        unit                   = a.getString(R.styleable.Lcd_unit) == null ? "" : a.getString(R.styleable.Lcd_unit);
        unitVisible            = a.getBoolean(R.styleable.Lcd_unitVisible, false);
        lowerRightText         = a.getString(R.styleable.Lcd_lowerRightText) == null ? "" : a.getString(R.styleable.Lcd_lowerRightText);
        lowerRightTextVisible  = a.getBoolean(R.styleable.Lcd_lowerRightTextVisible, false);
        value                  = a.getFloat(R.styleable.Lcd_value, 0f);
        decimals               = a.getInt(R.styleable.Lcd_decimals, 0);
        threshold              = a.getFloat(R.styleable.Lcd_threshold, maxValue);
        thresholdVisible       = a.getBoolean(R.styleable.Lcd_thresholdVisible, false);
        alarmVisible           = a.getBoolean(R.styleable.Lcd_alarmVisible, false);
        signalVisible          = a.getBoolean(R.styleable.Lcd_signalVisible, false);
        signalStrength         = a.getFloat(R.styleable.Lcd_signalStrength, 0f);
        upperLeftText          = a.getFloat(R.styleable.Lcd_upperLeftText, maxValue);
        upperLeftTextVisible   = a.getBoolean(R.styleable.Lcd_upperLeftTextVisible, false);
        upperRightText         = a.getFloat(R.styleable.Lcd_upperRightText, minValue);
        upperRightTextVisible  = a.getBoolean(R.styleable.Lcd_upperRightTextVisible, false);
        lowerCenterText        = a.getString(R.styleable.Lcd_lowerCenterText) == null ? "" : a.getString(R.styleable.Lcd_lowerCenterText);
        lowerCenterTextVisible = a.getBoolean(R.styleable.Lcd_lowerCenterTextVisible, false);
        formerValueVisible     = a.getBoolean(R.styleable.Lcd_formerValueVisible, false);
        trend                  = Trend.STEADY;
        trendVisible           = a.getBoolean(R.styleable.Lcd_trendVisible, false);
        design                 = a.getInt(R.styleable.Lcd_design, 0);
        a.recycle();

        animator.setDuration(animationDuration);
    }
    private void init() {
        initBounds();
        initGraphics();
        initAnimator();
    }
    private void initBounds() {
        frameRect      = new RectF(LEFT, TOP, RIGHT, BOTTOM);
        backgroundRect = new RectF(frameRect.left + 1, frameRect.top + 1, frameRect.right - 1, frameRect.bottom - 1);
    }
    private void initGraphics() {
        framePaint          = new Paint(Paint.ANTI_ALIAS_FLAG);
        backgroundPaint     = new Paint(Paint.ANTI_ALIAS_FLAG);
        backgroundTextPaint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        valuePaint          = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        unitPaint           = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        smallValuePaint     = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        iconPaint           = getIconPaint();
        thresholdIcon       = createThresholdIcon();
        trendDownIcon       = createTrendDownIcon();
        trendFallingIcon    = createTrendFallingIcon();
        trendSteadyIcon     = createTrendSteadyIcon();
        trendRisingIcon     = createTrendRisingIcon();
        trendUpIcon         = createTrendUpIcon();
        alarmIcon           = createAlarmIcon();
    }
    private void initAnimator() {
        animator.addUpdateListener(new AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {
                oldValue          = value;
                value             = (Float) animation.getAnimatedValue();
                thresholdExceeded = Float.compare(value, threshold) > 0;
                upperLeftText     = Math.min(value, upperLeftText);
                upperRightText    = Math.max(value, upperRightText);
                float change = (value - oldValue) / (maxValue - minValue);
                if (change < -0.05) {
                    trend = Trend.DOWN;
                } else if (change >= -0.05 && change < -0.01) {
                    trend = Trend.FALLING;
                } else if (change > -0.01 && change < 0.01) {
                    trend = Trend.STEADY;
                } else if (change >= 0.01 && change < 0.05) {
                    trend = Trend.RISING;
                } else if (change > 0.05) {
                    trend = Trend.UP;
                }
                invalidate();
            }
        });

        animator.addListener(new AnimatorListenerAdapter() {
            @Override public void onAnimationStart(Animator animation) {

            }

            @Override public void onAnimationEnd(Animator animation) {

            }
        });
    }


    // ******************** Graphics related **********************************
    private Paint getIconPaint() {
        final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(foregroundColor);
        paint.setStyle(Style.FILL);
        return paint;
    }
    private Path createThresholdIcon() {
        Path path = new Path();
        path.moveTo(10f, 18f);
        path.lineTo(14f, 18f);
        path.lineTo(14f, 22f);
        path.lineTo(10f, 22f);
        path.lineTo(10f, 18f);
        path.close();
        path.moveTo(10f, 8f);
        path.lineTo(14f, 8f);
        path.lineTo(14f, 16f);
        path.lineTo(10f, 16f);
        path.lineTo(10f, 8f);
        path.close();path.moveTo(24f, 24f);
        path.lineTo(12f, 0f);
        path.lineTo(0f, 24f);
        path.lineTo(24f, 24f);
        path.close();
        //Matrix matrix = new Matrix();
        //matrix.setScale(4f, 4f);
        //path.transform(matrix);
        return path;
    }
    private Path createTrendDownIcon() {
        Path path = new Path();
        path.moveTo(0f, 0f);
        path.lineTo(8f, 12f);
        path.lineTo(16f, 0f);
        path.lineTo(0f, 0f);
        path.close();
        //Matrix matrix = new Matrix();
        //matrix.setScale(4f, 4f);
        //path.transform(matrix);
        return path;
    }
    private Path createTrendFallingIcon() {
        Path path = new Path();
        path.moveTo(0.25f, 8f);
        path.lineTo(15.875f, 11.75f);
        path.lineTo(5.125f, 0.125f);
        path.lineTo(0.25f, 8f);
        path.close();
        //Matrix matrix = new Matrix();
        //matrix.setScale(4f, 4f);
        //path.transform(matrix);
        return path;
    }
    private Path createTrendSteadyIcon() {
        Path path = new Path();
        path.moveTo(0f, 0f);
        path.lineTo(16f, 6f);
        path.lineTo(0f, 12f);
        path.lineTo(0f, 0f);
        path.close();
        //Matrix matrix = new Matrix();
        //matrix.setScale(4f, 4f);
        //path.transform(matrix);
        return path;
    }
    private Path createTrendRisingIcon() {
        Path path = new Path();
        path.moveTo(0.25f, 3.875f);
        path.lineTo(15.875f, 0.125f);
        path.lineTo(5.125f, 11.75f);
        path.lineTo(0.25f, 3.875f);
        path.close();
        //Matrix matrix = new Matrix();
        //matrix.setScale(4f, 4f);
        //path.transform(matrix);
        return path;
    }
    private Path createTrendUpIcon() {
        Path path = new Path();
        path.moveTo(0f, 12f);
        path.lineTo(8f, 0f);
        path.lineTo(16f, 12f);
        path.lineTo(0f, 12f);
        path.close();
        //Matrix matrix = new Matrix();
        //matrix.setScale(4f, 4f);
        //path.transform(matrix);
        return path;
    }
    private Path createAlarmIcon() {
        Path path = new Path();
        path.moveTo(11.8125f, 16f);
        path.cubicTo(11.4027f, 17.3107f, 10.2986f, 18.5f, 9f, 18.5f);
        path.cubicTo(7.7013f, 18.5f, 6.597f, 17.3107f, 6.1875f, 16f);
        path.lineTo(11.8125f, 16f);
        path.close();
        path.moveTo(17.8097f, 13.6406f);
        path.cubicTo(11.8125f, 9.5625f, 16.3125f, 0f, 9f, 0f);
        path.lineTo(9f, 0f);
        path.lineTo(9f, 0f);
        path.cubicTo(1.6875f, 0f, 6.1875f, 9.5625f, 0.1903f, 13.6406f);
        path.cubicTo(0.0541f, 13.7332f, 0f, 13.9043f, 0f, 14.0625f);
        path.cubicTo(0f, 14.1292f, 0.0118f, 13.5716f, 0.0363f, 13.6364f);
        path.cubicTo(0.1191f, 13.8553f, 0.3285f, 14f, 0.5625f, 14f);
        path.lineTo(9f, 14f);
        path.lineTo(17.4375f, 14f);
        path.cubicTo(17.6715f, 14f, 17.8808f, 13.8553f, 17.9637f, 13.6364f);
        path.cubicTo(17.9882f, 13.5716f, 18f, 14.1292f, 18f, 14.0625f);
        path.cubicTo(18f, 13.9043f, 17.9459f, 13.7332f, 17.8097f, 13.6406f);
        path.close();
        //Matrix matrix = new Matrix();
        //matrix.setScale(4f, 4f);
        //path.transform(matrix);
        return path;
    }


    // ******************** State related *************************************
    @Override protected void onRestoreInstanceState(final Parcelable state) {
        final Bundle     bundle     = (Bundle) state;
        final Parcelable superState = bundle.getParcelable("superState");
        super.onRestoreInstanceState(superState);
        value           = bundle.getFloat("value");
        upperLeftText   = bundle.getFloat("upperLeftText");
        upperRightText  = bundle.getFloat("upperRightText");
        lowerCenterText = bundle.getString("lowerCenterText");
        lowerRightText  = bundle.getString("lowerRightText");
        upperCenterText = bundle.getString("upperCenterText");
    }
    @Override protected Parcelable onSaveInstanceState() {
        final Parcelable superState = super.onSaveInstanceState();
        final Bundle     state      = new Bundle();
        state.putParcelable("superState", superState);
        state.putFloat("value", value);
        state.putFloat("upperLeftText", upperLeftText);
        state.putFloat("upperRightText", upperRightText);
        state.putString("lowerCenterText", lowerCenterText);
        state.putString("lowerRightText", lowerRightText);
        state.putString("upperCenterText", upperCenterText);
        return state;
    }


    // ******************** Size related **************************************
    @Override protected void onMeasure(final int WIDTH_MEASURE_SPEC, final int HEIGHT_MEASURE_SPEC) {
        // Loggable.log.debug(String.format("WIDTH_MEASURE_SPEC=%s, HEIGHT_MEASURE_SPEC=%s",
        // View.MeasureSpec.toString(WIDTH_MEASURE_SPEC),
        // View.MeasureSpec.toString(HEIGHT_MEASURE_SPEC)));

        final int WIDTH_MODE  = MeasureSpec.getMode(WIDTH_MEASURE_SPEC);
        final int HEIGHT_MODE = MeasureSpec.getMode(HEIGHT_MEASURE_SPEC);
        final int WIDTH_SIZE  = MeasureSpec.getSize(WIDTH_MEASURE_SPEC);
        final int HEIGHT_SIZE = MeasureSpec.getSize(HEIGHT_MEASURE_SPEC);

        final int CHOSEN_WIDTH  = chooseWidth(WIDTH_MODE, WIDTH_SIZE);
        final int CHOSEN_HEIGHT = chooseHeight(HEIGHT_MODE, HEIGHT_SIZE);
        setMeasuredDimension(CHOSEN_WIDTH, CHOSEN_HEIGHT);
    }

    private int chooseWidth(final int MODE, final int WIDTH) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return WIDTH;
            case View.MeasureSpec.UNSPECIFIED:
            default                          :return DEFAULT_WIDTH;
        }
    }
    private int chooseHeight(final int MODE, final int HEIGHT) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return HEIGHT;
            case View.MeasureSpec.UNSPECIFIED:
            default                          : return DEFAULT_HEIGHT;
        }
    }

    @Override protected void onSizeChanged(final int WIDTH, final int HEIGHT, final int OLD_WIDTH, final int OLD_HEIGHT) { resize(); }

    private void resize() {
        final Canvas CANVAS = new Canvas();
        final float  scale  = Math.min(getWidth(), getHeight());
        final int    width  = getWidth();
        final int    height = getHeight();
        CANVAS.scale(scale, scale);
        CANVAS.translate((scale == height) ? ((width - scale) / 2) / scale : 0, (scale == width) ? ((height - scale) / 2) / scale : 0);

        frameRect.set(0, 0, width, height);
        backgroundRect.set(2, 2, width - 2, height - 2);

        resizeFramePaint();
        resizeBackgroundPaint(design);
        resizeBackgroundTextPaint();
        resizeValuePaint();
        resizeUnitPaint();
        resizeSmallValuePaint();
        resizeIconPaint();

        backgroundTextPaint.setTextSize(height * 0.583333333f);
        valuePaint.setTextSize(height * 0.5833333333f);
        unitPaint.setTextSize(height * 0.26f);
        smallValuePaint.setTextSize(getHeight() * 0.16f);

        // Adjust icons
        Matrix scaleMatrix     = new Matrix();
        Matrix translateMatrix = new Matrix();

        scaleMatrix.setScale((0.208333f * height) / 20f, (0.208333f * height) / 20f);
        thresholdIcon.transform(scaleMatrix);
        translateMatrix.setTranslate(0.0303f * width, 0.71f * height);
        thresholdIcon.transform(translateMatrix);

        scaleMatrix.setScale((0.16667f * height) / 16f, (0.125f * height)  / 12f);
        trendUpIcon.transform(scaleMatrix);
        translateMatrix.setTranslate(0.18182f * width, 0.8125f * height);
        trendUpIcon.transform(translateMatrix);
        trendRisingIcon.transform(scaleMatrix);
        trendRisingIcon.transform(translateMatrix);
        trendSteadyIcon.transform(scaleMatrix);
        trendSteadyIcon.transform(translateMatrix);
        trendFallingIcon.transform(scaleMatrix);
        trendFallingIcon.transform(translateMatrix);
        trendDownIcon.transform(scaleMatrix);
        trendDownIcon.transform(translateMatrix);

        scaleMatrix.setScale((0.1875f * height) / 18f, (0.19792f * height) / 19f);
        alarmIcon.transform(scaleMatrix);
        translateMatrix.setTranslate(0.28788f * width, 0.75f * height);
        alarmIcon.transform(translateMatrix);
    }
    private void resizeFramePaint() {
        if (design < 35) {
            LinearGradient gradient = new LinearGradient(0, 0, 0, getHeight(), frameColors, frameFractions, TileMode.CLAMP);
            framePaint.setShader(gradient);
        } else {
            framePaint.setColor(Color.WHITE);
        }
    }
    private void resizeBackgroundPaint(final int DESIGN) {
        final int[] COLORS;
        switch(DESIGN) {
            case 1:
                COLORS = new int[]{ Color.rgb(200, 200, 177),
                                    Color.rgb(241, 237, 207),
                                    Color.rgb(234, 230, 194),
                                    Color.rgb(225, 220, 183),
                                    Color.rgb(237, 232, 191)};
                foregroundColor = Color.rgb(   0,   0,   0);
                backgroundColor = Color.argb( 20,   0,   0,   0);
                break;
            case 2:
                COLORS = new int[] { Color.rgb(255, 255, 255),
                                     Color.rgb(231, 246, 255),
                                     Color.rgb(170, 224, 255),
                                     Color.rgb(136, 212, 255),
                                     Color.rgb(192, 232, 255)};
                foregroundColor = Color.rgb( 18,  69, 100);
                backgroundColor = Color.argb(20, 18, 69, 100);
                break;
            case 3:
                COLORS = new int[] { Color.rgb(255, 255, 255),
                                     Color.rgb(255, 245, 225),
                                     Color.rgb(255, 217, 147),
                                     Color.rgb(255, 201, 104),
                                     Color.rgb(255, 227, 173)};
                foregroundColor = Color.rgb( 80,  55,   0);
                backgroundColor = Color.argb(20, 80,  55, 0);
                break;
            case 4:
                COLORS = new int[] { Color.rgb(255, 255, 255),
                                     Color.rgb(255, 225, 225),
                                     Color.rgb(252, 114, 115),
                                     Color.rgb(252, 114, 115),
                                     Color.rgb(254, 178, 178)};
                foregroundColor = Color.rgb( 79,  12,  14);
                backgroundColor = Color.argb(20, 79, 12, 14);
                break;
            case 5:
                COLORS = new int[] { Color.rgb(255, 255, 255),
                                     Color.rgb(245, 255, 186),
                                     Color.rgb(158, 205,   0),
                                     Color.rgb(158, 205,   0),
                                     Color.rgb(210, 255,   0)};
                foregroundColor = Color.rgb( 64,  83,   0);
                backgroundColor = Color.argb(20, 64,  83, 0);
                break;
            case 6:
                COLORS = new int[] { Color.rgb(255, 255, 255),
                                     Color.rgb(255, 255, 255),
                                     Color.rgb(241, 246, 242),
                                     Color.rgb(229, 239, 244),
                                     Color.rgb(255, 255, 255)};
                foregroundColor = Color.rgb(  0,   0,   0);
                backgroundColor = Color.argb(20, 0, 0, 0);
                break;
            case 7:
                COLORS = new int[] { Color.rgb( 65,  65,  65),
                                     Color.rgb(117, 117, 117),
                                     Color.rgb( 87,  87,  87),
                                     Color.rgb( 65,  65,  65),
                                     Color.rgb( 81,  81,  81)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            case 8:
                COLORS = new int[] { Color.rgb( 65,  65,  65),
                                     Color.rgb(102, 102, 102),
                                     Color.rgb( 51,  51,  51),
                                     Color.rgb(  0,   0,   0),
                                     Color.rgb( 51,  51,  51)};
                foregroundColor = Color.rgb(204, 204, 204);
                backgroundColor = Color.argb(20, 204, 204, 204);
                break;
            case 9:
                COLORS = new int[] { Color.rgb( 33,  67,  67),
                                     Color.rgb( 33,  67,  67),
                                     Color.rgb( 29,  58,  58),
                                     Color.rgb( 28,  57,  57),
                                     Color.rgb( 23,  46,  46)};
                foregroundColor = Color.rgb(  0, 185, 165);
                backgroundColor = Color.argb(20,   0, 185, 165);
                break;
            case 10:
                COLORS = new int[] { Color.rgb( 27,  41,  17),
                                     Color.rgb( 70,  84,  58),
                                     Color.rgb( 36,  60,  14),
                                     Color.rgb( 24,  50,   1),
                                     Color.rgb(  8,  10,   7)};
                foregroundColor = Color.rgb(152, 255, 74);
                backgroundColor = Color.argb(20, 152, 255, 74);
                break;
            case 11:
                COLORS = new int[] { Color.rgb(  0,  68, 103),
                                     Color.rgb(  8, 109, 165),
                                     Color.rgb(  0,  72, 117),
                                     Color.rgb(  0,  72, 117),
                                     Color.rgb(  0,  68, 103)};
                foregroundColor = Color.rgb(111, 182, 228);
                backgroundColor = Color.argb(20, 111, 182, 228);
                break;
            case 12:
                COLORS = new int[] { Color.rgb( 22, 125, 212),
                                     Color.rgb(  3, 162, 254),
                                     Color.rgb(  3, 162, 254),
                                     Color.rgb(  3, 162, 254),
                                     Color.rgb( 11, 172, 244)};
                foregroundColor = Color.rgb(  0,   0,   0);
                backgroundColor = Color.argb(20,   0,   0,   0);
                break;
            case 13:
                COLORS = new int[] { Color.rgb( 18,  33,  88),
                                     Color.rgb( 18,  33,  88),
                                     Color.rgb( 19,  30,  90),
                                     Color.rgb( 17,  31,  94),
                                     Color.rgb( 21,  25,  90)};
                foregroundColor = Color.rgb( 23,  99, 221);
                backgroundColor = Color.argb(20, 23,  99, 221);
                break;
            case 14:
                COLORS = new int[] { Color.rgb( 88, 107, 132),
                                     Color.rgb( 53,  74, 104),
                                     Color.rgb( 27,  37,  65),
                                     Color.rgb(  5,  12,  40),
                                     Color.rgb( 32,  47,  79)};
                foregroundColor = Color.rgb( 71, 178, 254);
                backgroundColor = Color.argb(20, 71, 178, 254);
                break;
            case 15:
                COLORS = new int[] { Color.rgb(135, 174, 255),
                                     Color.rgb(101, 159, 255),
                                     Color.rgb( 44,  93, 255),
                                     Color.rgb( 27,  65, 254),
                                     Color.rgb( 12,  50, 255)};
                foregroundColor = Color.rgb(178, 180, 237);
                backgroundColor = Color.argb(20, 178, 180, 237);
                break;
            case 16:
                COLORS = new int[] { Color.rgb(131, 133, 119),
                                     Color.rgb(176, 183, 167),
                                     Color.rgb(165, 174, 153),
                                     Color.rgb(166, 175, 156),
                                     Color.rgb(175, 184, 165)};
                foregroundColor = Color.rgb( 35,  42,  52);
                backgroundColor = Color.argb(20, 35, 42, 52);
                break;
            case 17:
                COLORS = new int[] { Color.rgb(194, 212, 188),
                                     Color.rgb(212, 234, 206),
                                     Color.rgb(205, 224, 194),
                                     Color.rgb(206, 225, 194),
                                     Color.rgb(214, 233, 206)};
                foregroundColor = Color.rgb(  0,  12,   6);
                backgroundColor = Color.argb(20, 0, 12, 6);
                break;
            case 18:
                COLORS = new int[] { Color.rgb(255, 255, 255),
                                     Color.rgb(219, 230, 220),
                                     Color.rgb(179, 194, 178),
                                     Color.rgb(153, 176, 151),
                                     Color.rgb(114, 138, 109)};
                foregroundColor = Color.rgb(  0,  12,   6);
                backgroundColor = Color.argb(20, 0, 12, 6);
                break;
            case 19:
                COLORS = new int[] { Color.rgb(100, 168, 253),
                                     Color.rgb(100, 168, 253),
                                     Color.rgb( 95, 160, 250),
                                     Color.rgb( 80, 144, 252),
                                     Color.rgb( 74, 134, 255)};
                foregroundColor = Color.rgb(  0,  44, 187);
                backgroundColor = Color.argb(20, 0, 44, 187);
                break;
            case 20:
                COLORS = new int[] { Color.rgb( 72,  36,  50),
                                     Color.rgb(185, 111, 110),
                                     Color.rgb(148,  66,  72),
                                     Color.rgb( 83,  19,  20),
                                     Color.rgb(  7,   6,  14)};
                foregroundColor = Color.rgb(254, 139, 146);
                backgroundColor = Color.argb(20, 254, 139, 146);
                break;
            case 21:
                COLORS = new int[] { Color.rgb( 14,  24,  31),
                                     Color.rgb( 46, 105, 144),
                                     Color.rgb( 19,  64,  96),
                                     Color.rgb(  6,  20,  29),
                                     Color.rgb(  8,   9,  10)};
                foregroundColor = Color.rgb( 61, 179, 255);
                backgroundColor = Color.argb(20, 61, 179, 255);
                break;
            case 22:
                COLORS = new int[] { Color.rgb(175, 164, 255),
                                     Color.rgb(188, 168, 253),
                                     Color.rgb(176, 159, 255),
                                     Color.rgb(174, 147, 252),
                                     Color.rgb(168, 136, 233)};
                foregroundColor = Color.rgb(  7,  97,  72);
                backgroundColor = Color.argb(20, 7, 97, 72);
                break;
            case 23:
                COLORS = new int[] { Color.rgb(  8,  12,  11),
                                     Color.rgb( 10,  11,  13),
                                     Color.rgb( 11,  10,  15),
                                     Color.rgb(  7,  13,   9),
                                     Color.rgb(  9,  13,  14)};
                foregroundColor = Color.rgb(181,   0,  38);
                backgroundColor = Color.argb(20, 181, 0, 38);
                break;
            case 24:
                COLORS = new int[] { Color.rgb( 25,  85,   0),
                                     Color.rgb( 47, 154,   0),
                                     Color.rgb( 30, 101,   0),
                                     Color.rgb( 30, 101,   0),
                                     Color.rgb( 25,  85,   0)};
                foregroundColor = Color.rgb( 35,  49,  35);
                backgroundColor = Color.argb(20,  35,  49,  35);
                break;
            case 25:
                COLORS = new int[] { Color.rgb(182,  71,   0),
                                     Color.rgb(236, 155,  25),
                                     Color.rgb(212,  93,   5),
                                     Color.rgb(212,  93,   5),
                                     Color.rgb(182,  71,   0)};
                foregroundColor = Color.rgb( 89,  58,  10);
                backgroundColor = Color.argb(20,  89,  58,  10);
                break;
            case 26:
                COLORS = new int[] { Color.rgb(125, 146, 184),
                                     Color.rgb(197, 212, 231),
                                     Color.rgb(138, 155, 194),
                                     Color.rgb(138, 155, 194),
                                     Color.rgb(125, 146, 184)};
                foregroundColor = Color.rgb(  9,   0,  81);
                backgroundColor = Color.argb(20,   9,   0,  81);
                break;
            case 27:
                COLORS = new int[] { Color.rgb(  1,  47,   0),
                                     Color.rgb( 20, 106,  61),
                                     Color.rgb( 33, 125,  84),
                                     Color.rgb( 33, 125,  84),
                                     Color.rgb( 33, 109,  63)};
                foregroundColor = Color.rgb(  3,  15,  11);
                backgroundColor = Color.argb(20,   3,  15,  11);
                break;
            case 28:
                COLORS = new int[] { Color.rgb(223, 248,  86),
                                     Color.rgb(222, 255,  28),
                                     Color.rgb(213, 245,  24),
                                     Color.rgb(213, 245,  24),
                                     Color.rgb(224, 248,  88)};
                foregroundColor = Color.rgb(  9,  19,   0);
                backgroundColor = Color.argb(20,   9,  19,   0);
                break;
            case 29:
                COLORS = new int[] { Color.rgb( 43,   3,   3),
                                     Color.rgb( 29,   0,   0),
                                     Color.rgb( 26,   2,   2),
                                     Color.rgb( 31,   5,   8),
                                     Color.rgb( 30,   1,   3)};
                foregroundColor = Color.rgb(255, 254,  24);
                backgroundColor = Color.argb(20, 255, 254,  24);
                break;
            case 30:
                COLORS = new int[] { Color.rgb( 79, 121,  19),
                                     Color.rgb( 96, 169,   0),
                                     Color.rgb(120, 201,   2),
                                     Color.rgb(118, 201,   0),
                                     Color.rgb(105, 179,   4)};
                foregroundColor = Color.rgb(  0,  35,   0);
                backgroundColor = Color.argb(20,   0,  35,   0);
                break;
            case 31:
                COLORS = new int[] { Color.rgb( 35,  24,  75),
                                     Color.rgb( 42,  20, 111),
                                     Color.rgb( 40,  22, 103),
                                     Color.rgb( 40,  22, 103),
                                     Color.rgb( 41,  21, 111)};
                foregroundColor = Color.rgb(158, 167, 210);
                backgroundColor = Color.argb(20, 158, 167, 210);
                break;
            case 32:
                COLORS = new int[] { Color.rgb(134,  39,  17),
                                     Color.rgb(120,  24,   0),
                                     Color.rgb( 83,  15,  12),
                                     Color.rgb( 83,  15,  12),
                                     Color.rgb(120,  24,   0)};
                foregroundColor = Color.rgb(233, 140,  44);
                backgroundColor = Color.argb(20, 233, 140,  44);
                break;
            case 33:
                COLORS = new int[] { Color.rgb( 15,  84, 151),
                                     Color.rgb( 60, 103, 198),
                                     Color.rgb( 67, 109, 209),
                                     Color.rgb( 67, 109, 209),
                                     Color.rgb( 64, 101, 190)};
                foregroundColor = Color.rgb(193, 253, 254);
                backgroundColor = Color.argb(20, 193, 253, 254);
                break;
            case 34:
                COLORS = new int[] { Color.rgb(153, 164, 161),
                                     Color.rgb(203, 215, 213),
                                     Color.rgb(202, 212, 211),
                                     Color.rgb(202, 212, 211),
                                     Color.rgb(198, 209, 213)};
                foregroundColor = Color.rgb( 99, 124, 204);
                backgroundColor = Color.argb(20, 90, 124, 204);
                break;
            case 35:
                COLORS = new int[]{ Color.rgb( 31, 188, 156),
                                    Color.rgb( 31, 188, 156),
                                    Color.rgb( 31, 188, 156),
                                    Color.rgb( 31, 188, 156),
                                    Color.rgb( 31, 188, 156)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            case 36:
                COLORS = new int[]{ Color.rgb( 26, 188, 156),
                                    Color.rgb( 26, 188, 156),
                                    Color.rgb( 26, 188, 156),
                                    Color.rgb( 26, 188, 156),
                                    Color.rgb( 26, 188, 156)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            case 37:
                COLORS = new int[]{ Color.rgb( 46, 204, 113),
                                    Color.rgb( 46, 204, 113),
                                    Color.rgb( 46, 204, 113),
                                    Color.rgb( 46, 204, 113),
                                    Color.rgb( 46, 204, 113)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            case 38:
                COLORS = new int[]{ Color.rgb( 39, 174,  96),
                                    Color.rgb( 39, 174,  96),
                                    Color.rgb( 39, 174,  96),
                                    Color.rgb( 39, 174,  96),
                                    Color.rgb( 39, 174,  96)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            case 39:
                COLORS = new int[]{ Color.rgb( 52, 152, 219),
                                    Color.rgb( 52, 152, 219),
                                    Color.rgb( 52, 152, 219),
                                    Color.rgb( 52, 152, 219),
                                    Color.rgb( 52, 152, 219)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            case 40:
                COLORS = new int[]{ Color.rgb( 41, 128, 185),
                                    Color.rgb( 41, 128, 185),
                                    Color.rgb( 41, 128, 185),
                                    Color.rgb( 41, 128, 185),
                                    Color.rgb( 41, 128, 185)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            case 41:
                COLORS = new int[]{ Color.rgb(155,  89, 182),
                                    Color.rgb(155,  89, 182),
                                    Color.rgb(155,  89, 182),
                                    Color.rgb(155,  89, 182),
                                    Color.rgb(155,  89, 182)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            case 42:
                COLORS = new int[]{ Color.rgb(142,  68, 173),
                                    Color.rgb(142,  68, 173),
                                    Color.rgb(142,  68, 173),
                                    Color.rgb(142,  68, 173),
                                    Color.rgb(142,  68, 173)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            case 43:
                COLORS = new int[]{ Color.rgb(241, 196,  15),
                                    Color.rgb(241, 196,  15),
                                    Color.rgb(241, 196,  15),
                                    Color.rgb(241, 196,  15),
                                    Color.rgb(241, 196,  15)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            case 44:
                COLORS = new int[]{ Color.rgb(243, 156,  18),
                                    Color.rgb(243, 156,  18),
                                    Color.rgb(243, 156,  18),
                                    Color.rgb(243, 156,  18),
                                    Color.rgb(243, 156,  18)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            case 45:
                COLORS = new int[]{ Color.rgb(230, 126,  34),
                                    Color.rgb(230, 126,  34),
                                    Color.rgb(230, 126,  34),
                                    Color.rgb(230, 126,  34),
                                    Color.rgb(230, 126,  34)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            case 46:
                COLORS = new int[]{ Color.rgb(211,  84,   0),
                                    Color.rgb(211,  84,   0),
                                    Color.rgb(211,  84,   0),
                                    Color.rgb(211,  84,   0),
                                    Color.rgb(211,  84,   0)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            case 47:
                COLORS = new int[]{ Color.rgb(231,  76,  60),
                                    Color.rgb(231,  76,  60),
                                    Color.rgb(231,  76,  60),
                                    Color.rgb(231,  76,  60),
                                    Color.rgb(231,  76,  60)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            case 48:
                COLORS = new int[]{ Color.rgb(192,  57,  43),
                                    Color.rgb(192,  57,  43),
                                    Color.rgb(192,  57,  43),
                                    Color.rgb(192,  57,  43),
                                    Color.rgb(192,  57,  43)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            case 49:
                COLORS = new int[]{ Color.rgb(236, 240, 241),
                                    Color.rgb(236, 240, 241),
                                    Color.rgb(236, 240, 241),
                                    Color.rgb(236, 240, 241),
                                    Color.rgb(236, 240, 241)};
                foregroundColor = Color.rgb(  0,   0,   0);
                backgroundColor = Color.argb(20,   0,   0, 0);
                break;
            case 50:
                COLORS = new int[]{ Color.rgb(189, 195, 199),
                                    Color.rgb(189, 195, 199),
                                    Color.rgb(189, 195, 199),
                                    Color.rgb(189, 195, 199),
                                    Color.rgb(189, 195, 199)};
                foregroundColor = Color.rgb(  0,   0,   0);
                backgroundColor = Color.argb( 20,  0,   0, 0);
                break;
            case 51:
                COLORS = new int[]{ Color.rgb(149, 165, 166),
                                    Color.rgb(149, 165, 166),
                                    Color.rgb(149, 165, 166),
                                    Color.rgb(149, 165, 166),
                                    Color.rgb(149, 165, 166)};
                foregroundColor = Color.rgb(  0,   0,   0);
                backgroundColor = Color.argb(  20,   0,   0, 0);
                break;
            case 52:
                COLORS = new int[]{ Color.rgb(127, 140, 141),
                                    Color.rgb(127, 140, 141),
                                    Color.rgb(127, 140, 141),
                                    Color.rgb(127, 140, 141),
                                    Color.rgb(127, 140, 141)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            case 53:
                COLORS = new int[]{ Color.rgb( 52,  73,  94),
                                    Color.rgb( 52,  73,  94),
                                    Color.rgb( 52,  73,  94),
                                    Color.rgb( 52,  73,  94),
                                    Color.rgb( 52,  73,  94)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            case 54:
                COLORS = new int[]{ Color.rgb( 44,  62,  80),
                                    Color.rgb( 44,  62,  80),
                                    Color.rgb( 44,  62,  80),
                                    Color.rgb( 44,  62,  80),
                                    Color.rgb( 44,  62,  80)};
                foregroundColor = Color.rgb(255, 255, 255);
                backgroundColor = Color.argb(20, 255, 255, 255);
                break;
            default:
                COLORS = new int[] { Color.rgb(131, 133, 119),
                                     Color.rgb(176, 183, 167),
                                     Color.rgb(165, 174, 153),
                                     Color.rgb(166, 175, 156),
                                     Color.rgb(175, 184, 165)};
                foregroundColor = Color.rgb( 35,  42,  52);
                backgroundColor = Color.argb(25, 35, 42, 52);
                break;
        }
        LinearGradient gradient = new LinearGradient(0, 1, 0, getHeight() - 1, COLORS, backgroundFractions, TileMode.CLAMP);

        backgroundPaint.setShader(gradient);
    }
    private void resizeBackgroundTextPaint() {
        backgroundTextPaint.setColor(foregroundColor);
        backgroundTextPaint.setAlpha(25);
        backgroundTextPaint.setStyle(Style.FILL);
        backgroundTextPaint.setTextSize(getHeight() * 0.583333333f);
        backgroundTextPaint.setTextAlign(Align.LEFT);
        backgroundTextPaint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "digital.ttf"));
        //paint.setShadowLayer(0.01f, 0.002f, 0.002f, Color.argb(165, 0, 0, 0));
    }
    private void resizeValuePaint() {
        valuePaint.setColor(foregroundColor);
        valuePaint.setStyle(Style.FILL);
        valuePaint.setTextSize(getHeight() * 0.583333333f);
        valuePaint.setTextAlign(Align.LEFT);
        valuePaint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "digital.ttf"));
        //paint.setShadowLayer(0.01f, 0.002f, 0.002f, Color.argb(165, 0, 0, 0));
    }
    private void resizeUnitPaint() {
        unitPaint.setColor(foregroundColor);
        unitPaint.setStyle(Style.FILL);
        unitPaint.setTextSize(getHeight() * 0.26f);
        unitPaint.setTextAlign(Align.LEFT);
        unitPaint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "OpenSans-Regular.ttf"));
        //paint.setShadowLayer(0.01f, 0.002f, 0.002f, Color.argb(165, 0, 0, 0));
    }
    private void resizeSmallValuePaint() {
        smallValuePaint.setColor(foregroundColor);
        smallValuePaint.setStyle(Style.FILL);
        smallValuePaint.setTextSize(getHeight() * 0.16f);
        smallValuePaint.setTextAlign(Align.LEFT);
        smallValuePaint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "OpenSans-Regular.ttf"));
        //paint.setShadowLayer(0.01f, 0.002f, 0.002f, Color.argb(165, 0, 0, 0));
    }
    private void resizeIconPaint() {
        iconPaint.setColor(foregroundColor);
        iconPaint.setStyle(Style.FILL);
    }


    // ******************** Drawing related ***********************************
    @Override protected void onDraw(final Canvas CANVAS) {
        drawFrame(CANVAS);
        drawBackground(CANVAS);
        drawForeground(CANVAS);

        /*
        final float scale = Math.min(getWidth(), getHeight());
        canvas.scale(scale, scale);
        canvas.translate((scale == getHeight()) ? ((getWidth() - scale) / 2) / scale : 0
                        ,(scale == getWidth()) ? ((getHeight() - scale) / 2) / scale : 0);
        */
    }

    private void drawFrame(final Canvas CANVAS) {
        CANVAS.drawRoundRect(frameRect, 10, 10, framePaint);
    }
    private void drawBackground(final Canvas CANVAS) {
        CANVAS.drawRoundRect(backgroundRect, 9, 9, backgroundPaint);
    }
    private void drawForeground(final Canvas CANVAS) {
        final float WIDTH  = getWidth();
        final float HEIGHT = getHeight();
        
        final float UNIT_WIDTH  = TextUtils.isEmpty(unit) ? 0 : unitPaint.measureText(unit);
        final float VALUE_WIDTH = valuePaint.measureText(valueString(value));

        // calculate background text
        final float ONE_SEGMENT_WIDTH = valuePaint.measureText("8");

        // Width of decimals
        final float WIDTH_OF_DECIMALS = decimals == 0 ? 0 : decimals * ONE_SEGMENT_WIDTH + ONE_SEGMENT_WIDTH;

        // Available width
        final float AVAILABLE_WIDTH = WIDTH - 2f - (UNIT_WIDTH + HEIGHT * 0.0833333333f) - WIDTH_OF_DECIMALS;

        // Number of segments
        final int NO_OF_SEGMENTS = (int) Math.floor(AVAILABLE_WIDTH / ONE_SEGMENT_WIDTH);

        String backgroundText = "";
        for (int i = 0 ; i < NO_OF_SEGMENTS ; i++) {
            backgroundText += '8';
        }
        if (decimals != 0) {
            backgroundText += ".";
            for (int i = 0 ; i < decimals ; i++) {
                backgroundText += '8';
            }
        }
        final float BACKGROUND_WIDTH = backgroundTextPaint.measureText(backgroundText);

        //valueBackground
        if (unitVisible) {
            CANVAS.drawText(backgroundText, WIDTH - 2 - BACKGROUND_WIDTH - (UNIT_WIDTH + HEIGHT * 0.0833333333f), 0.71f * HEIGHT, backgroundTextPaint);
        } else {
            CANVAS.drawText(backgroundText, WIDTH - 2 - BACKGROUND_WIDTH - HEIGHT * 0.0833333333f, 0.71f * HEIGHT, backgroundTextPaint);
        }

        //valueText
        if (unitVisible) {
            CANVAS.drawText(valueString(value), WIDTH - 2 - VALUE_WIDTH - (UNIT_WIDTH + HEIGHT * 0.0833333333f), 0.71f * HEIGHT, valuePaint);
        } else {
            CANVAS.drawText(valueString(value), WIDTH - 2 - VALUE_WIDTH - HEIGHT * 0.0833333333f, 0.71f * HEIGHT, valuePaint);
        }

        //unitText
        if (unitVisible) {
            CANVAS.drawText(unit, WIDTH - UNIT_WIDTH - 0.04f * HEIGHT, 0.71f * HEIGHT, unitPaint);
        }

        //lowerCenterText
        if (lowerCenterTextVisible) {
            if (formerValueVisible) {
                CANVAS.drawText(valueString(formerValue), (WIDTH - smallValuePaint.measureText(valueString(formerValue))) * 0.5f, 0.96f * HEIGHT, smallValuePaint);
            } else {
                CANVAS.drawText(lowerCenterText, (WIDTH - smallValuePaint.measureText(lowerCenterText)) * 0.5f, 0.96f * HEIGHT, smallValuePaint);
            }
        }

        //upperLeftText
        if (upperLeftTextVisible) {
            CANVAS.drawText(valueString(upperLeftText), 0.0416666667f * HEIGHT, 0.19f * HEIGHT, smallValuePaint);
        }

        //upperRightText
        if (upperRightTextVisible) {
            CANVAS.drawText(valueString(upperRightText), WIDTH - 0.0416666667f * HEIGHT - smallValuePaint.measureText(valueString(upperRightText)), 0.19f * HEIGHT, smallValuePaint);
        }

        //upperCenterText
        if (upperCenterTextVisible) {
            CANVAS.drawText(upperCenterText, (WIDTH - smallValuePaint.measureText(upperCenterText)) * 0.5f, 0.19f * HEIGHT, smallValuePaint);
        }

        //lowerRightText
        if (lowerRightTextVisible) {
            CANVAS.drawText(lowerRightText, WIDTH - 0.0416666667f * HEIGHT - smallValuePaint.measureText(lowerRightText), 0.96f * HEIGHT, smallValuePaint);
        }

        //threshold
        if (thresholdVisible && thresholdExceeded) {
            CANVAS.drawPath(thresholdIcon, iconPaint);
        }

        //trend
        if (trendVisible) {
            switch (trend) {
                case UP     : CANVAS.drawPath(trendUpIcon, iconPaint);      break;
                case RISING : CANVAS.drawPath(trendRisingIcon, iconPaint);  break;
                case STEADY : CANVAS.drawPath(trendSteadyIcon, iconPaint);  break;
                case FALLING: CANVAS.drawPath(trendFallingIcon, iconPaint); break;
                case DOWN   : CANVAS.drawPath(trendDownIcon, iconPaint);    break;
            }
        }

        //alarm
        if (alarmVisible) {
            CANVAS.drawPath(alarmIcon, iconPaint);
        }
    }


    // ******************** Methods *******************************************
    public boolean isAnimated() { return animated; }
    public void setAnimated(final boolean ANIMATED) {
        animated = ANIMATED;
        invalidate();
    }

    public long getAnimationDuration() { return animationDuration; }
    public void setAnimationDuration(final long ANIMATION_DURATION) {
        animationDuration = Util.clamp(0l, 10000l, ANIMATION_DURATION);
        invalidate();
    }

    public float getValue() { return value; }
    public void setValue(final float VALUE) {
        oldValue    = value;
        formerValue = value;
        if (animated) {
            if (formerValueVisible) lowerCenterText = String.format(Locale.US, "%." + decimals + "f", formerValue);
            currentValue = Util.clamp(minValue, maxValue, VALUE);
            if (animator.isRunning()) animator.cancel();
            animator.setFloatValues(oldValue, currentValue);
            animator.start();
        } else {
            if (formerValueVisible) lowerCenterText = String.format(Locale.US, "%." + decimals + "f", formerValue);
            value             = Util.clamp(minValue, maxValue, VALUE);
            thresholdExceeded = Float.compare(value, threshold) > 0;
            upperLeftText     = Math.min(value, upperLeftText);
            upperRightText    = Math.max(value, upperRightText);
            float change      = (value - oldValue) / (maxValue - minValue);
            if (change < -0.05) {
                trend = Trend.DOWN;
            } else if (change >= -0.05 && change < -0.01) {
                trend = Trend.FALLING;
            } else if (change > -0.01 && change < 0.01) {
                trend = Trend.STEADY;
            } else if (change >= 0.01 && change < 0.05) {
                trend = Trend.RISING;
            } else if (change > 0.05) {
                trend = Trend.UP;
            }
            invalidate();
        }
    }

    public float getMinValue() { return minValue; }
    public void setMinValue(final float MIN_VALUE) {
        minValue = Util.clamp(-Float.MAX_VALUE, maxValue, MIN_VALUE);
        invalidate();
    }

    public float getMaxValue() { return maxValue; }
    public void setMaxValue(final float MAX_VALUE) {
        maxValue = Util.clamp(minValue, Float.MAX_VALUE, MAX_VALUE);
        invalidate();
    }

    public boolean isUnitVisible() { return unitVisible; }
    public void setUnitVisible(final boolean UNIT_VISIBLE) {
        unitVisible = UNIT_VISIBLE;
        invalidate();
    }

    public String getUnit() { return unit; }
    public void setUnit(final String UNIT) {
        unit = UNIT;
        invalidate();
    }

    public String getUpperCenterText() { return upperCenterText; }
    public void setUpperCenterText(final String TEXT) {
        upperCenterText = TEXT;
        invalidate();
    }

    public String getLowerRightText() { return lowerRightText; }
    public void setLowerRightText(final String TEXT) {
        lowerRightText = TEXT;
        invalidate();
    }

    public String getLowerCenterText() { return lowerCenterText; }
    public void setLowerCenterText(final String LOWER_CENTER_TEXT) {
        if (formerValueVisible) return;
        lowerCenterText = LOWER_CENTER_TEXT;
        invalidate();
    }

    public boolean isFormerValueVisible() { return formerValueVisible; }
    public void setFormerValueVisible(final boolean FORMER_VALUE_VISIBLE) {
        formerValueVisible = FORMER_VALUE_VISIBLE;
        invalidate();
    }

    public float getMinMeasuredValue() { return upperLeftText; }
    public float getMaxMeasuredValue() { return upperRightText; }
    public void resetMinMaxMeasuredValues() {
        upperLeftText  = maxValue;
        upperRightText = minValue;
        invalidate();
    }

    public int getDecimals() { return decimals; }
    public void setDecimals(final int DECIMALS) {
        decimals = DECIMALS;
        invalidate();
    }

    public float getThreshold() { return threshold; }
    public void setThreshold(final float THRESHOLD) {
        threshold = Util.clamp(minValue, maxValue, THRESHOLD);
        invalidate();
    }

    public Trend getTrend() { return trend; }

    public boolean isTrendVisible() { return trendVisible; }
    public void setTrendVisible(final boolean TREND_VISIBLE) {
        trendVisible = TREND_VISIBLE;
        invalidate();
    }

    public boolean isThresholdVisible() { return thresholdVisible; }
    public void setThresholdVisible(final boolean THRESHOLD_VISIBLE) {
        thresholdVisible = THRESHOLD_VISIBLE;
        invalidate();
    }

    public boolean isAlarmVisible() { return alarmVisible; }
    public void setAlarmVisible(final boolean ALARM_VISIBLE) {
        alarmVisible = ALARM_VISIBLE;
        invalidate();
    }

    public boolean isSignalVisible() { return signalVisible; }
    public void setSignalVisible(final boolean SIGNAL_VISIBLE) {
        signalVisible = SIGNAL_VISIBLE;
        invalidate();
    }

    public float getSignalStrength() { return signalStrength; }
    public void setSignalStrength(final float SIGNAL_STRENGTH) {
        signalStrength = Util.clamp(0f, 1f, SIGNAL_STRENGTH);
        invalidate();
    }

    public int getDesign() { return design; }
    public void setDesign(final Design DESIGN) {
        setDesign(DESIGN.NUMBER);
    }
    public void setDesign(final int DESIGN) {
        design = DESIGN;
        invalidate();
    }


    // ******************** Utility methods ***********************************
    private String valueString(final float VALUE) { return String.format(Locale.US, "%." + Integer.toString(decimals) + "f", VALUE); }
}
