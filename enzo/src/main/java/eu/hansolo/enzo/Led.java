package eu.hansolo.enzo;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.*;
import android.graphics.Shader.TileMode;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;


/**
 * Created by hansolo on 17.02.15.
 */
public class Led extends View {
    private static final String TAG = "LED";

    public static final int   DEFAULT_WIDTH  = 100;
    public static final int   DEFAULT_HEIGHT = 100;
    public static final float TOP            = 0.0f;
    public static final float LEFT           = 0.0f;
    public static final float RIGHT          = 1.0f;
    public static final float BOTTOM         = 1.0f;
    public static final float CENTER         = 0.5f;

    private Handler  handler;
    private Runnable runnable;

    private boolean on;
    private boolean frameVisible;
    private int     ledColor;
    private boolean blinking;
    private RectF   frameRect;
    private RectF   ledRect;
    private RectF   highlightRect;
    private Paint   framePaint;
    private Paint   offPaint;
    private Paint   onPaint;
    private Paint   innerShadowPaint;
    private Paint   highlightPaint;
    private float[] frameFractions       = { 0.0f, 0.15f, 0.26f, 0.2601f, 0.85f, 1.0f };
    private int[]   frameColors          = { Color.argb(165, 20, 20, 20),
                                             Color.argb(165, 20, 20, 20),
                                             Color.argb(165, 41, 41, 41),
                                             Color.argb(164, 41, 41, 41),
                                             Color.argb(103, 200, 200, 200),
                                             Color.argb(88, 200, 200, 200) };
    private float[] onOffFractions       = { 0.0f, 0.49f, 1.0f };
    private int[]   innerShadowColors    = new int[] { Color.argb(0, 0, 0, 0), Color.argb(0, 117, 117, 117), Color.argb(102, 0, 0, 0) };
    private float[] innerShadowFractions = new float[] { 0f, 0.85f, 1.0f };
    private float[] highlightFractions   = { 0.0f, 1.0f };
    private int[]   highlightColors      = { Color.argb(204, 200, 200, 200), Color.argb(0, 200, 200, 200) };


    // ******************** Constructors **************************************
    public Led(final Context CONTEXT, final AttributeSet ATTRS) {
        this(CONTEXT, ATTRS, 0);
    }
    public Led(final Context CONTEXT, final AttributeSet ATTRS, final int DEF_STYLE) {
        super(CONTEXT, ATTRS, DEF_STYLE);
        initAttributes(CONTEXT, ATTRS, DEF_STYLE);
        init();
        handler  = new Handler();
        runnable = new Runnable() {
            @Override public void run() {
                setOn(!isOn());
                handler.postDelayed(runnable, 500);
            }
        };
    }


    // ******************** Initialization ************************************
    private void initAttributes(final Context context, final AttributeSet attrs, final int defStyle) {
        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Led, defStyle, 0);
        on                 = a.getBoolean(R.styleable.Led_on, false);
        frameVisible       = a.getBoolean(R.styleable.Led_frameVisible, true);
        ledColor           = a.getInt(R.styleable.Led_ledColor, 0);

        a.recycle();
    }
    private void init() {
        initBounds();
        initGraphics();
    }
    private void initBounds() {
        frameRect     = new RectF(LEFT, TOP, RIGHT, BOTTOM);
        ledRect       = new RectF(0.14f * getWidth(), 0.14f * getHeight(), 0.72f * getWidth(), 0.72f * getHeight());
        highlightRect = new RectF(0.21f * getWidth(), 0.21f * getHeight(), 0.58f * getWidth(), 0.58f * getHeight());
    }
    private void initGraphics() {
        framePaint       = new Paint(Paint.ANTI_ALIAS_FLAG);
        offPaint         = new Paint(Paint.ANTI_ALIAS_FLAG);
        onPaint          = new Paint(Paint.ANTI_ALIAS_FLAG);
        innerShadowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        highlightPaint   = new Paint(Paint.ANTI_ALIAS_FLAG);
    }


    // ******************** State related *************************************
    @Override protected void onRestoreInstanceState(final Parcelable state) {
        final Bundle bundle     = (Bundle) state;
        final Parcelable superState = bundle.getParcelable("superState");
        super.onRestoreInstanceState(superState);
        on           = bundle.getBoolean("on");
        frameVisible = bundle.getBoolean("frameVisible");
        ledColor = bundle.getInt("color");
    }
    @Override protected Parcelable onSaveInstanceState() {
        final Parcelable superState = super.onSaveInstanceState();
        final Bundle     state      = new Bundle();
        state.putParcelable("superState", superState);
        state.putBoolean("on", on);
        state.putBoolean("frameVisible", frameVisible);
        state.putInt("color", ledColor);
        return state;
    }


    // ******************** Size related **************************************
    @Override protected void onMeasure(final int WIDTH_MEASURE_SPEC, final int HEIGHT_MEASURE_SPEC) {
        // Loggable.log.debug(String.format("WIDTH_MEASURE_SPEC=%s, HEIGHT_MEASURE_SPEC=%s",
        // View.MeasureSpec.toString(WIDTH_MEASURE_SPEC),
        // View.MeasureSpec.toString(HEIGHT_MEASURE_SPEC)));

        final int WIDTH_MODE  = MeasureSpec.getMode(WIDTH_MEASURE_SPEC);
        final int HEIGHT_MODE = MeasureSpec.getMode(HEIGHT_MEASURE_SPEC);
        final int WIDTH_SIZE  = MeasureSpec.getSize(WIDTH_MEASURE_SPEC);
        final int HEIGHT_SIZE = MeasureSpec.getSize(HEIGHT_MEASURE_SPEC);

        final int CHOSEN_WIDTH  = chooseWidth(WIDTH_MODE, WIDTH_SIZE);
        final int CHOSEN_HEIGHT = chooseHeight(HEIGHT_MODE, HEIGHT_SIZE);
        setMeasuredDimension(CHOSEN_WIDTH, CHOSEN_HEIGHT);
    }

    private int chooseWidth(final int MODE, final int WIDTH) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return WIDTH;
            case View.MeasureSpec.UNSPECIFIED:
            default                          :return DEFAULT_WIDTH;
        }
    }
    private int chooseHeight(final int MODE, final int HEIGHT) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return HEIGHT;
            case View.MeasureSpec.UNSPECIFIED:
            default                          : return DEFAULT_HEIGHT;
        }
    }

    @Override protected void onSizeChanged(final int WIDTH, final int HEIGHT, final int OLD_WIDTH, final int OLD_HEIGHT) { resize(); }

    private void resize() {
        final Canvas canvas = new Canvas();
        final float  scale  = Math.min(getWidth(), getHeight());
        final int    width  = getWidth();
        final int    height = getHeight();
        final int    size   = width < height ? width : height;
        canvas.scale(scale, scale);
        canvas.translate((scale == height) ? ((width - scale) / 2) / scale : 0, (scale == width) ? ((height - scale) / 2) / scale : 0);

        frameRect.set(0, 0, size, size);
        ledRect.set(0.14f * size, 0.14f * size, 0.72f * size, 0.72f * size);
        highlightRect.set(0.21f * size, 0.21f * size, 0.58f * size, 0.58f * size);

        resizeFramePaint();
        resizeOffPaint(ledColor);
        resizeOnPaint(ledColor);
        resizeInnerShadowPaint();
        resizeHighlightPaint();
    }
    private void resizeFramePaint() {
        LinearGradient gradient = new LinearGradient(0.14f * getWidth(), 0.14f * getHeight(),
                                                     0.84f * getWidth(), 0.84f * getHeight(),
                                                     frameColors, frameFractions, TileMode.CLAMP);
        framePaint.setShader(gradient);
    }
    private void resizeOffPaint(final int COLOR) {
        final int   C;
        final int[] COLORS;
        switch(COLOR) {
            case 1:  /* red */     C = Color.parseColor("#CA0814"); break;
            case 2:  /* green */   C = Color.parseColor("#2BCA23"); break;
            case 3:  /* blue */    C = Color.parseColor("#033BFF"); break;
            case 4:  /* yellow */  C = Color.parseColor("#FAFA00"); break;
            case 5:  /* orange */  C = Color.parseColor("#FC5100"); break;
            case 6:  /* cyan */    C = Color.parseColor("#00FFFF"); break;
            case 7:  /* magenta */ C = Color.parseColor("#FF00FF"); break;
            case 8:  /* purple */  C = Color.parseColor("#7900FF"); break;
            case 9:  /* gray */    C = Color.parseColor("#CCCCCC"); break;
            default: /* red */     C = Color.parseColor("#CA0814"); break;
        }
        COLORS = new int[]{ derive(C, -80.0f), derive(C, -87.0f), derive(C, -80.0f) };
        LinearGradient gradient = new LinearGradient(0.25714f * getWidth(), 0.25714f * getHeight(),
                                                     0.74286f * getWidth(), 0.74286f * getHeight(),
                                                     COLORS, onOffFractions, TileMode.CLAMP);


        offPaint.setShader(gradient);
    }
    private void resizeOnPaint(final int COLOR) {
        final int   C;
        final int[] COLORS;
        switch(COLOR) {
            case 1:  /* red */     C = Color.parseColor("#CA0814"); break;
            case 2:  /* green */   C = Color.parseColor("#2BCA23"); break;
            case 3:  /* blue */    C = Color.parseColor("#033BFF"); break;
            case 4:  /* yellow */  C = Color.parseColor("#FAFA00"); break;
            case 5:  /* orange */  C = Color.parseColor("#FC5100"); break;
            case 6:  /* cyan */    C = Color.parseColor("#00FFFF"); break;
            case 7:  /* magenta */ C = Color.parseColor("#FF00FF"); break;
            case 8:  /* purple */  C = Color.parseColor("#7900FF"); break;
            case 9:  /* gray */    C = Color.parseColor("#CCCCCC"); break;
            default: /* red */     C = Color.parseColor("#CA0814"); break;
        }
        COLORS = new int[]{ derive(C, -23f), derive(C, -50f), C};
        LinearGradient gradient = new LinearGradient(0.25714f * getWidth(), 0.25714f * getHeight(),
                                                     0.74286f * getWidth(), 0.74286f * getHeight(),
                                                     COLORS, onOffFractions, TileMode.CLAMP);
        onPaint.setShader(gradient);
    }
    private void resizeInnerShadowPaint() {
        RadialGradient gradient = new RadialGradient(0.5f * getWidth(), 0.5f * getHeight(), getWidth() == 0 ? 0.35714f : 0.35714f * getWidth(),
                                                     innerShadowColors, innerShadowFractions, TileMode.CLAMP);
        innerShadowPaint.setShader(gradient);
    }
    private void resizeHighlightPaint() {
        RadialGradient gradient = new RadialGradient(0.3f * getWidth(), 0.3f * getHeight(), getWidth() == 0 ? 0.28571f : 0.28571f * getWidth(),
                                                     highlightColors, highlightFractions, TileMode.CLAMP);
        highlightPaint.setShader(gradient);
    }


    // ******************** Drawing related ***********************************
    @Override protected void onDraw(final Canvas CANVAS) {
        drawFrame(CANVAS);
        drawLed(CANVAS);
        drawHighlight(CANVAS);
    }

    private void drawFrame(final Canvas CANVAS) {
        if (frameVisible) CANVAS.drawCircle(getWidth() * 0.5f, getHeight() * 0.5f, getWidth() * 0.5f, framePaint);
    }
    private void drawLed(final Canvas CANVAS) {
        if (on) {
            CANVAS.drawCircle(getWidth() * 0.5f, getHeight() * 0.5f, getWidth() * 0.35714f, onPaint);
            CANVAS.drawCircle(getWidth() * 0.5f, getHeight() * 0.5f, getWidth() * 0.35714f, innerShadowPaint);
        } else {
            CANVAS.drawCircle(getWidth() * 0.5f, getHeight() * 0.5f, getWidth() * 0.35714f, offPaint);
        }
    }
    private void drawHighlight(final Canvas CANVAS) {
        CANVAS.drawCircle(getWidth() * 0.5f, getHeight() * 0.5f, getWidth() * 0.28571f, highlightPaint);
    }


    // ******************** Methods *******************************************
    public boolean isOn() { return on; }
    public void setOn(final boolean ON) {
        on = ON;
        invalidate();
    }

    public boolean isFrameVisible() { return frameVisible; }
    public void setFrameVisible(final boolean FRAME_VISIBLE) {
        frameVisible = FRAME_VISIBLE;
        invalidate();
    }

    public int getLedColor() { return ledColor; }
    public void setLedColor(final int LED_COLOR) {
        ledColor = LED_COLOR;
        invalidate();
    }

    public boolean isBlinking() { return blinking; }
    public void setBlinking(final boolean BLINKING) {
        if (BLINKING) {
            if (blinking) return;
            handler.postDelayed(runnable, 500);
        } else {
            if (!blinking) return;
            setOn(false);
            handler.removeCallbacks(runnable);
        }
        blinking = BLINKING;
    }

    public int derive(final String COLOR, final float PERCENTAGE) {
        return derive(Color.parseColor(COLOR), PERCENTAGE);
    }
    public int derive(final int COLOR, final float PERCENTAGE) {
        float   p   = clamp(-100f, 100f, PERCENTAGE) / 100.0f;
        float[] hsv = new float[3];
        Color.colorToHSV(COLOR, hsv);
        hsv[2] = clamp(0f, 1f, hsv[2] + (p * hsv[2]));
        return Color.HSVToColor(hsv);
    }

    private float clamp(final float MIN, final float MAX, final float VALUE) {
        if (VALUE < MIN) return MIN;
        if (VALUE > MAX) return MAX;
        return VALUE;
    }
}
