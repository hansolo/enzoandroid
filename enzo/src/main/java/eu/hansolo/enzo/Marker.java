package eu.hansolo.enzo;

import eu.hansolo.enzo.MarkerEvent.MarkerEventType;

import java.util.List;


/**
 * Created by hansolo on 05.05.15.
 */
public class Marker {
    private float                    value;
    private String                   text;
    private boolean                  exceeded;
    private List<MarkerEventHandler> markerEventHandlerList;


    // ******************** Constructors **************************************
    public Marker() {
        this(0, "Marker");
    }
    public Marker(final float VALUE) {
        this(VALUE, "Marker");
    }
    public Marker(final float VALUE, final String TEXT) {
        value    = VALUE;
        text     = TEXT;
        exceeded = false;
    }


    // ******************** Methods *******************************************
    public final float getValue() { return value; }
    public final void setValue(final float VALUE) { value = VALUE; }

    public final String getText() { return text; }
    public final void setText(final String TEXT) { text = TEXT; }

    public final boolean isExceeded() { return exceeded; }
    public final void setExceeded(final boolean EXCEEDED) {
        exceeded = EXCEEDED;
        if (EXCEEDED) {
            fireMarkerEvent(new MarkerEvent(this, MarkerEventType.MARKER_EXCEEDED));
        } else {
            fireMarkerEvent(new MarkerEvent(this, MarkerEventType.MARKER_UNDERRUN));
        }
    }


    // ******************** Event handling ************************************
    public void addOnSectionEvent(final MarkerEventHandler HANDLER) {
        if (markerEventHandlerList.contains(HANDLER)) return;
        markerEventHandlerList.add(HANDLER);
    }
    public void removeOnMarkerEvent(final MarkerEventHandler HANDLER) {
        if (markerEventHandlerList.contains(HANDLER)) markerEventHandlerList.remove(HANDLER);
    }
    public void fireMarkerEvent(MarkerEvent EVENT) {
        for (MarkerEventHandler handler : markerEventHandlerList) {
            handler.handle(EVENT);
        }
    }


    public boolean equals(final Marker MARKER) {
        return (Float.compare(MARKER.getValue(), getValue()) == 0 &&
                MARKER.getText().equals(getText()));
    }

    @Override public String toString() {
        final StringBuilder NAME = new StringBuilder();
        NAME.append("Marker: ").append("\n");
        NAME.append("text  : ").append(getText()).append("\n");
        NAME.append("Value : ").append(getValue()).append("\n");
        return NAME.toString();
    }
}
