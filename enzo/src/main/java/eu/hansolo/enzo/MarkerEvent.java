package eu.hansolo.enzo;

/**
 * Created by hansolo on 05.05.15.
 */
public class MarkerEvent extends java.util.EventObject {
    public enum MarkerEventType { MARKER_EXCEEDED, MARKER_UNDERRUN };
    public final MarkerEventType TYPE;

    public MarkerEvent(Object source, final MarkerEventType TYPE) {
        super(source);
        this.TYPE = TYPE;
    }
}
