package eu.hansolo.enzo;

import java.util.EventListener;


/**
 * Created by hansolo on 05.05.15.
 */
public interface MarkerEventHandler  extends EventListener {
    void handle(final MarkerEvent EVENT);
}
