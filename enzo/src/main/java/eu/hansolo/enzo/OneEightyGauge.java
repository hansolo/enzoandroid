package eu.hansolo.enzo;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.*;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path.Direction;
import android.graphics.Path.FillType;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import eu.hansolo.enzo.SectionEvent.SectionEventType;

import java.util.Locale;


/**
 * Created by hansolo on 13.04.15.
 */
public class OneEightyGauge extends View {
    private static final String TAG = "OneEightyGauge";

    public static final  int   DEFAULT_WIDTH  = 270;
    public static final  int   DEFAULT_HEIGHT = 200;
    public static final  float TOP            = 0.0f;
    public static final  float LEFT           = 0.0f;
    public static final  float RIGHT          = 1.0f;
    public static final  float BOTTOM         = 1.0f;
    public static final  float CENTER         = 0.5f;
    public static final  float MIN_VALUE      = 0.0f;
    public static final  float MAX_VALUE      = 100.0f;
    private static final float START_ANGLE    = 180f;
    private static final float STOP_ANGLE     = 180f;
    private static final float ANGLE_RANGE    = 180;
    private static final float ASPECT_RATIO   = 0.74074f;

    private float          value;
    private float          oldValue;
    private float          currentValue;
    private float          minValue;
    private float          maxValue;
    private float          range;
    private float          angleStep;
    private float          currentValueAngle;
    private String         title;
    private String         unit;
    private int            decimals;
    private int            barBackgroundColor;
    private int            barColor;
    private int            unitColor;
    private int            titleColor;
    private int            valueColor;
    private int            minValueColor;
    private int            maxValueColor;
    private boolean        dynamicBarColor;
    private GradientLookup gradientLookup;
    private RectF          barBackgroundRect;
    private Path           barBackground;
    private RectF          dataBarRect;
    private Paint          barBackgroundPaint;
    private Paint          barPaint;
    private Paint          unitPaint;
    private Paint          titlePaint;
    private Paint          valuePaint;
    private Paint          minValuePaint;
    private Paint          maxValuePaint;
    private Path           dataBar;
    private ValueAnimator  animator;
    private boolean        animated;
    private long           animationDuration;


    // ******************** Constructors **************************************
    public OneEightyGauge(final Context CONTEXT, final AttributeSet ATTRS) {
        this(CONTEXT, ATTRS, 0, null);
    }
    public OneEightyGauge(final Context CONTEXT, final AttributeSet ATTRS, final int DEF_STYLE) {
        this(CONTEXT, ATTRS, DEF_STYLE, null);
    }
    public OneEightyGauge(final Context CONTEXT, final AttributeSet ATTRS, final int DEF_STYLE, final GradientLookup GRADIENT_LOOKUP) {
        super(CONTEXT, ATTRS, DEF_STYLE);
        gradientLookup = null == GRADIENT_LOOKUP ? new GradientLookup() : GRADIENT_LOOKUP;
        barBackground  = new Path();
        dataBar        = new Path();
        animator       = ValueAnimator.ofFloat(minValue, minValue);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        initAttributes(CONTEXT, ATTRS, DEF_STYLE);
        init();
    }


    // ******************** Initialization ************************************
    private void initAttributes(final Context context, final AttributeSet attrs, final int defStyle) {
        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.OneEightyGauge, defStyle, 0);

        value              = a.getFloat(R.styleable.OneEightyGauge_valueOneEightyGauge, MIN_VALUE);
        minValue           = a.getFloat(R.styleable.OneEightyGauge_minValueOneEightyGauge, MIN_VALUE);
        maxValue           = a.getFloat(R.styleable.OneEightyGauge_maxValueOneEightyGauge, MAX_VALUE);
        title              = a.getString(R.styleable.OneEightyGauge_titleOneEightyGauge) == null ? "" : a.getString(R.styleable.OneEightyGauge_titleOneEightyGauge);
        unit               = a.getString(R.styleable.OneEightyGauge_unitOneEightyGauge) == null ? "" : a.getString(R.styleable.OneEightyGauge_unitOneEightyGauge);
        animated           = a.getBoolean(R.styleable.OneEightyGauge_animatedOneEightyGauge, true);
        animationDuration  = a.getInt(R.styleable.OneEightyGauge_animationDurationOneEightyGauge, 800);
        decimals           = a.getInt(R.styleable.OneEightyGauge_decimalsOneEightyGauge, 0);
        barBackgroundColor = a.getColor(R.styleable.OneEightyGauge_barBackgroundColorOneEightyGauge, Color.rgb(239, 240, 240));
        barColor           = a.getColor(R.styleable.OneEightyGauge_barColorOneEightyGauge, Color.rgb(50, 33, 201));
        unitColor          = a.getColor(R.styleable.OneEightyGauge_unitColorOneEightyGauge, Color.rgb(0, 0, 0));
        titleColor         = a.getColor(R.styleable.OneEightyGauge_titleColorOneEightyGauge, Color.rgb(0, 0, 0));
        valueColor         = a.getColor(R.styleable.OneEightyGauge_valueColorOneEightyGauge, Color.rgb(0, 0, 0));
        minValueColor      = a.getColor(R.styleable.OneEightyGauge_minValueColorOneEightyGauge, Color.rgb(0, 0, 0));
        maxValueColor      = a.getColor(R.styleable.OneEightyGauge_maxValueColorOneEightyGauge, Color.rgb(0, 0, 0));
        dynamicBarColor    = a.getBoolean(R.styleable.OneEightyGauge_dynamicBarColorOneEightyGauge, false);

        a.recycle();

        animator.setDuration(animationDuration);
        range     = maxValue - minValue;
        angleStep = ANGLE_RANGE / range;
    }
    private void init() {
        initBounds();
        initGraphics();
        initAnimator();
    }
    private void initBounds() {
        barBackgroundRect = new RectF(LEFT, TOP, RIGHT, BOTTOM);
        dataBarRect       = new RectF(LEFT, TOP, RIGHT, BOTTOM);
    }
    private void initGraphics() {
        barBackgroundPaint = getBarBackgroundPaint();
        barPaint           = getBarPaint();
        unitPaint          = getUnitPaint();
        titlePaint         = getTitlePaint();
        valuePaint         = getValuePaint();
        minValuePaint      = getMinValuePaint();
        maxValuePaint      = getMaxValuePaint();
    }
    private void initAnimator() {
        animator.addUpdateListener(new AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {
                value = (Float) animation.getAnimatedValue();
                invalidate();
            }
        });
    }


    // ******************** Graphics related **********************************
    private Paint getBarBackgroundPaint() {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        return paint;
    }
    private Paint getBarPaint() {
        final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        return paint;
    }
    private Paint getUnitPaint() {
        final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setColor(unitColor);
        paint.setStyle(Style.FILL);
        paint.setTextSize(getHeight() * 0.12f);
        paint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "Roboto-Thin.ttf"));
        return paint;
    }
    private Paint getTitlePaint() {
        final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setColor(titleColor);
        paint.setStyle(Style.FILL);
        paint.setTextSize(getHeight() * 0.12f);
        paint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "Roboto-Thin.ttf"));
        return paint;
    }
    private Paint getValuePaint() {
        final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setColor(valueColor);
        paint.setStyle(Style.FILL);
        paint.setTextSize(getHeight() * 0.24f);
        paint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "Roboto-Regular.ttf"));
        return paint;
    }
    private Paint getMinValuePaint() {
        final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setColor(minValueColor);
        paint.setStyle(Style.FILL);
        paint.setTextSize(getHeight() * 0.12f);
        paint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "Roboto-Thin.ttf"));
        return paint;
    }
    private Paint getMaxValuePaint() {
        final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setColor(maxValueColor);
        paint.setStyle(Style.FILL);
        paint.setTextSize(getHeight() * 0.12f);
        paint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "Roboto-Thin.ttf"));
        return paint;
    }


    // ******************** State related *************************************
    @Override protected void onRestoreInstanceState(final Parcelable state) {
        final Bundle bundle     = (Bundle) state;
        final Parcelable superState = bundle.getParcelable("superState");
        super.onRestoreInstanceState(superState);
        value = bundle.getFloat("value");
        unit  = bundle.getString("unit");
        title = bundle.getString("title");
    }
    @Override protected Parcelable onSaveInstanceState() {
        final Parcelable superState = super.onSaveInstanceState();
        final Bundle     state      = new Bundle();
        state.putParcelable("superState", superState);
        state.putFloat("value", value);
        state.putString("unit", unit);
        state.putString("title", title);
        return state;
    }


    // ******************** Size related **************************************
    @Override protected void onMeasure(final int WIDTH_MEASURE_SPEC, final int HEIGHT_MEASURE_SPEC) {
        // Loggable.log.debug(String.format("WIDTH_MEASURE_SPEC=%s, HEIGHT_MEASURE_SPEC=%s",
        // View.MeasureSpec.toString(WIDTH_MEASURE_SPEC),
        // View.MeasureSpec.toString(HEIGHT_MEASURE_SPEC)));

        final int WIDTH_MODE  = MeasureSpec.getMode(WIDTH_MEASURE_SPEC);
        final int HEIGHT_MODE = MeasureSpec.getMode(HEIGHT_MEASURE_SPEC);
        final int WIDTH_SIZE  = MeasureSpec.getSize(WIDTH_MEASURE_SPEC);
        final int HEIGHT_SIZE = MeasureSpec.getSize(HEIGHT_MEASURE_SPEC);

        final int CHOSEN_WIDTH  = chooseWidth(WIDTH_MODE, WIDTH_SIZE);
        final int CHOSEN_HEIGHT = chooseHeight(HEIGHT_MODE, HEIGHT_SIZE);
        setMeasuredDimension(CHOSEN_WIDTH, CHOSEN_HEIGHT);
    }

    private int chooseWidth(final int MODE, final int WIDTH) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return WIDTH;
            case View.MeasureSpec.UNSPECIFIED:
            default                          :return DEFAULT_WIDTH;
        }
    }
    private int chooseHeight(final int MODE, final int HEIGHT) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return HEIGHT;
            case View.MeasureSpec.UNSPECIFIED:
            default                          : return DEFAULT_HEIGHT;
        }
    }

    @Override protected void onSizeChanged(final int WIDTH, final int HEIGHT, final int OLD_WIDTH, final int OLD_HEIGHT) { resize(); }

    private void resize() {
        final Canvas CANVAS = new Canvas();
        final float scale  = Math.min(getWidth(), getHeight());
        int          width  = getWidth();
        int          height = getHeight();

        /*
        if (ASPECT_RATIO * width > height) {
            width  = (int) (1 / (ASPECT_RATIO / height));
        } else if (1 / (ASPECT_RATIO / height) > width) {
            height = (int) (ASPECT_RATIO * width);
        }
        */

        CANVAS.scale(scale, scale);
        CANVAS.translate((scale == height) ? ((width - scale) / 2) / scale : 0, (scale == width) ? ((height - scale) / 2) / scale : 0);

        barBackgroundRect.set(0, 0, width, height);
        dataBarRect.set(0, 0, width, height);

        resizeBackground();
        resizeDataBar();

        float smallText = height * 0.12f;
        float bigText   = height * 0.24f;

        titlePaint.setTextSize(smallText);
        valuePaint.setTextSize(bigText);
        unitPaint.setTextSize(smallText);
        minValuePaint.setTextSize(smallText);
        maxValuePaint.setTextSize(smallText);
    }
    private void resizeBackground() {
        final float WIDTH  = getWidth();
        final float HEIGHT = getHeight();

        barBackground.reset();
        barBackground.setFillType(FillType.EVEN_ODD);

        barBackground.addArc(new RectF(0f, 0f, WIDTH, WIDTH), START_ANGLE, STOP_ANGLE);
        barBackground.addArc(new RectF(0.23148f * WIDTH, 0.23148f * WIDTH, 0.7685f * WIDTH, 0.7685f * WIDTH), 0, -STOP_ANGLE);
        barBackground.close();
    }
    private void resizeDataBar() {
        final float WIDTH  = getWidth();
        final float HEIGHT = getHeight();

        currentValueAngle = (value + Math.abs(minValue)) * angleStep;

        float sinAlpha = (float) Math.sin(Math.toRadians(currentValueAngle + 180));
        float cosAlpha = (float) Math.cos(Math.toRadians(currentValueAngle + 180));
        float center   = 0.5f * WIDTH;
        if (Float.compare(currentValueAngle, 0f) <= 0) {
            dataBar.reset();
            return;
        }
        dataBar.reset();
        dataBar.addArc(0f, 0f, WIDTH, WIDTH, START_ANGLE, currentValueAngle);
        dataBar.lineTo(center + cosAlpha * 0.26852f * WIDTH, center + sinAlpha * 0.26852f * WIDTH);
        dataBar.addArc(0.23148f * WIDTH, 0.23148f * WIDTH, 0.7685f * WIDTH, 0.7685f * WIDTH, START_ANGLE + currentValueAngle, -currentValueAngle);
        dataBar.lineTo(0f, center);
        dataBar.close();
    }


    // ******************** Drawing related ***********************************
    @Override protected void onDraw(final Canvas CANVAS) {
        drawBackground(CANVAS);
        updateBar(CANVAS);
    }

    private void drawBackground(final Canvas CANVAS) {
        final float WIDTH           = getWidth();
        final float HEIGHT          = getHeight();
        final float TITLE_WIDTH     = titlePaint.measureText(title);
        final float UNIT_WIDTH      = unitPaint.measureText(unit);
        final float MIN_VALUE_WIDTH = minValuePaint.measureText(String.format(Locale.US, "%." + decimals + "f", minValue));
        final float MAX_VALUE_WIDTH = maxValuePaint.measureText(String.format(Locale.US, "%." + decimals + "f", maxValue));

        // bar background
        barBackgroundPaint.setColor(barBackgroundColor);
        CANVAS.drawPath(barBackground, barBackgroundPaint);

        // unit
        unitPaint.setColor(unitColor);
        CANVAS.drawText(unit, 0.5f * (WIDTH - UNIT_WIDTH), 0.55f * HEIGHT, unitPaint);

        // title
        titlePaint.setColor(titleColor);
        CANVAS.drawText(title, 0.5f * (WIDTH - TITLE_WIDTH), 0.95f * HEIGHT, titlePaint);

        // minValue
        minValuePaint.setColor(minValueColor);
        CANVAS.drawText(String.format(Locale.US, "%." + decimals + "f", minValue), 0.5f * ((0.25f * WIDTH) - MIN_VALUE_WIDTH), 0.8f * HEIGHT, minValuePaint);

        // maxValue
        maxValuePaint.setColor(maxValueColor);
        CANVAS.drawText(String.format(Locale.US, "%." + decimals + "f", maxValue), 0.5f * ((0.25f * WIDTH) - MAX_VALUE_WIDTH) + 0.75f * WIDTH, 0.8f * HEIGHT, maxValuePaint);
    }
    private void updateBar(final Canvas CANVAS) {
        final float WIDTH       = getWidth();
        final float HEIGHT      = getHeight();
        final float VALUE_WIDTH = valuePaint.measureText(String.format(Locale.US, "%." + decimals + "f", value));

        // bar background
        if (isDynamicBarColor() && gradientLookup.getStops().size() > 1) {
            barColor = gradientLookup.getColorAt(value / range);
        }
        barPaint.setColor(barColor);
        resizeDataBar();
        CANVAS.drawPath(dataBar, barPaint);

        // Value
        valuePaint.setColor(valueColor);
        CANVAS.drawText(String.format(Locale.US, "%." + decimals + "f", value), (WIDTH - VALUE_WIDTH) * 0.5f , 0.8f * HEIGHT, valuePaint);
    }


    // ******************** Methods *******************************************
    public float getValue() { return value; }
    public void setValue(final float VALUE) {
        oldValue = value;
        if (animated) {
            currentValue = Util.clamp(minValue, maxValue, VALUE);
            if (animator.isRunning()) animator.cancel();
            animator.setFloatValues(oldValue, currentValue);
            animator.start();
        } else {
            value = Util.clamp(minValue, maxValue, VALUE);
            invalidate();
        }
    }

    public String getTitle() {return title; }
    public void setTitle(final String TITLE) {
        title = TITLE;
        invalidate();
    }

    public String getUnit() { return unit; }
    public void setUnit(final String UNIT) {
        unit = UNIT;
        invalidate();
    }

    public int getBarColor() { return barColor; }
    public void setBarColor(final int BAR_COLOR) {
        barColor = BAR_COLOR;
        invalidate();
    }

    public boolean isDynamicBarColor() { return dynamicBarColor; }
    public void setDynamicBarColor(final boolean DYNAMIC_BAR_COLOR) { dynamicBarColor = DYNAMIC_BAR_COLOR; }

    public GradientLookup getGradientLookup() { return gradientLookup; }
    public void setGradientLookup(final GradientLookup GRADIENT_LOOKUP) { gradientLookup = GRADIENT_LOOKUP; }


    // ******************** Utility methods ***********************************
    private String valueString(final float VALUE) { return String.format(Locale.US, "%." + Integer.toString(decimals) + "f", VALUE); }
}
