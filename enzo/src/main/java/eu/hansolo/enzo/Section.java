package eu.hansolo.enzo;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by hansolo on 18.02.15.
 */
public class Section {
    private float                     start;
    private float                     stop;
    private String                    text;
    private int                       color;
    private Drawable                  image;
    private Paint                     paint;
    private List<SectionEventHandler> sectionEventHandlerList;


    // ******************** Constructors **************************************
    public Section() {
        this(-1, -1, Color.argb(0, 0, 0, 0), "", null);
    }
    public Section(final float START, final float STOP) {
        this(START, STOP, Color.argb(0, 0, 0, 0), "", null);
    }
    public Section(final float START, final float STOP, final int COLOR) {
        this(START, STOP, COLOR, "", null);
    }
    public Section(final float START, final float STOP, final int COLOR, final String TEXT) {
        this(START, STOP, COLOR, TEXT, null);
    }
    public Section(final float START, final float STOP, final int COLOR, final String TEXT, final Drawable IMAGE) {
        start                   = START;
        stop                    = STOP;
        text                    = TEXT;
        color                   = COLOR;
        image                   = IMAGE;
        sectionEventHandlerList = new ArrayList<SectionEventHandler>(8);
        paint                   = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);
        paint.setStyle(Style.FILL);
    }


    // ******************** Methods *******************************************
    public final float getStart() { return start; }
    public final void setStart(final float START) { start = START; }

    public final float getStop() { return stop; }
    public final void setStop(final float STOP) { stop = STOP; }

    public final String getText() { return text; }
    public final void setText(final String TEXT) { text = TEXT; }

    public final int getColor() { return color; }
    public final void setColor(final int COLOR) {
        color = COLOR;
        paint.setColor(color);
    }

    public final Drawable getImage() { return image; }
    public final void setImage(final Drawable IMAGE) { image = IMAGE; }

    public final Paint getPaint() { return paint; }

    public boolean contains(final float VALUE) { return ((Float.compare(VALUE, getStart()) >= 0 && Float.compare(VALUE, getStop()) <= 0)); }


    // ******************** Event handling ************************************
    public void addOnSectionEvent(final SectionEventHandler HANDLER) {
        if (sectionEventHandlerList.contains(HANDLER)) return;
        sectionEventHandlerList.add(HANDLER);
    }
    public void removeOnSectionEvent(final SectionEventHandler HANDLER) {
        if (sectionEventHandlerList.contains(HANDLER)) sectionEventHandlerList.remove(HANDLER);
    }
    public void fireSectionEvent(SectionEvent EVENT) {
        for (SectionEventHandler handler : sectionEventHandlerList) {
            handler.handle(EVENT);
        }
    }


    public boolean equals(final Section SECTION) {
        return (Double.compare(SECTION.getStart(), getStart()) == 0 &&
                Double.compare(SECTION.getStop(), getStop()) == 0 &&
                SECTION.getText().equals(getText()));
    }

    @Override public String toString() {
        final StringBuilder NAME = new StringBuilder();
        NAME.append("Section: ").append("\n");
        NAME.append("text      : ").append(getText()).append("\n");
        NAME.append("startValue: ").append(getStart()).append("\n");
        NAME.append("stopValue : ").append(getStop()).append("\n");
        return NAME.toString();
    }
}
