package eu.hansolo.enzo;

/**
 * Created by hansolo on 18.02.15.
 */
public class SectionEvent extends java.util.EventObject {
    public enum SectionEventType { ENTERING_SECTION, LEAVING_LEAVING };
    public final SectionEventType TYPE;

    public SectionEvent(Object source, final SectionEventType TYPE) {
        super(source);
        this.TYPE = TYPE;
    }
}
