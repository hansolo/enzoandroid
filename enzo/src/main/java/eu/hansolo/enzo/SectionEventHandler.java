package eu.hansolo.enzo;

import java.util.EventListener;


/**
 * Created by hansolo on 18.02.15.
 */
public interface SectionEventHandler extends EventListener {
    void handle(final SectionEvent EVENT);
}
