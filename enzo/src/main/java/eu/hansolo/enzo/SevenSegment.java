package eu.hansolo.enzo;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by hansolo on 17.10.15.
 */
public class SevenSegment extends View {
    private static final String                 TAG = "SevenSegment";

    public static final int                     DEFAULT_WIDTH  = 268;
    public static final int                     DEFAULT_HEIGHT = 357;
    public static final float                   TOP            = 0.0f;
    public static final float                   LEFT           = 0.0f;
    public static final float                   RIGHT          = 1.0f;
    public static final float                   BOTTOM         = 1.0f;
    public static final float                   CENTER         = 0.5f;
    private             int                     a,b,c,d,e,f,g;
    private             Map<Character, Integer> mapping;
    private             char                    character;
    private             boolean                 dot;
    private             int                     segmentColor;
    private             boolean                 glow;
    private             RectF                   inactiveRect;
    private             RectF                   activeRect;
    private             Paint                   inactivePaint;
    private             Paint                   activePaint;
    private             float                   aspectRatio;
    private             Path                    segA;
    private             Path                    segB;
    private             Path                    segC;
    private             Path                    segD;
    private             Path                    segE;
    private             Path                    segF;
    private             Path                    segG;
    private             Path                    segDot;
    

    // ******************** Constructors **************************************
    public SevenSegment(final Context CONTEXT, final AttributeSet ATTRS) {
        this(CONTEXT, ATTRS, 0);
    }
    public SevenSegment(final Context CONTEXT, final AttributeSet ATTRS, final int DEF_STYLE) {
        super(CONTEXT, ATTRS, DEF_STYLE);

        a = 1 << 0;
        b = 1 << 1;
        c = 1 << 2;
        d = 1 << 3;
        e = 1 << 4;
        f = 1 << 5;
        g = 1 << 6;

        mapping = new HashMap<Character, Integer>(24);
        mapping.put(' ', 0);
        mapping.put('0', a | b | c | d | e | f);
        mapping.put('1', b | c);
        mapping.put('2', a | b | d | e | g);
        mapping.put('3', a | b | c | d | g);
        mapping.put('4', b | c | f | g);
        mapping.put('5', a | c | d | f | g);
        mapping.put('6', a | c | d | e | f | g);
        mapping.put('7', a | b | c);
        mapping.put('8', a | b | c | d | e | f | g);
        mapping.put('9', a | b | c | f | g);
        mapping.put('-', g);
        mapping.put('E', a | d | e | f | g);
        mapping.put('a', c | d | e | g);
        mapping.put('b', c | d | e | f | g);
        mapping.put('c', d | e | g);
        mapping.put('d', b | c | d | e | g);
        mapping.put('f', a | e | f | g);

        initAttributes(CONTEXT, ATTRS, DEF_STYLE);
        init();
    }


    // ******************** Initialization ************************************
    private void initAttributes(final Context context, final AttributeSet attrs, final int defStyle) {
        final TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.Led, defStyle, 0);
        character    = ta.getString(R.styleable.SevenSegment_characterSevenSegment) == null ? '0' : ta.getString(R.styleable.SevenSegment_characterSevenSegment).charAt(0);
        dot          = ta.getBoolean(R.styleable.SevenSegment_dotSevenSegment, true);
        segmentColor = ta.getInt(R.styleable.SevenSegment_segmentColorSevenSegment, Color.rgb(255, 0, 0));
        glow         = ta.getBoolean(R.styleable.SevenSegment_glowSevenSegment, false);
        ta.recycle();
        aspectRatio  = DEFAULT_HEIGHT / DEFAULT_WIDTH;
    }
    private void init() {
        initBounds();
        initGraphics();
    }
    private void initBounds() {
        inactiveRect = new RectF(LEFT, TOP, RIGHT, BOTTOM);
        activeRect   = new RectF(LEFT, TOP, RIGHT, BOTTOM);
    }
    private void initGraphics() {
        segA          = new Path();
        segB          = new Path();
        segC          = new Path();
        segD          = new Path();
        segE          = new Path();
        segF          = new Path();
        segG          = new Path();
        segDot        = new Path();
        inactivePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        activePaint   = new Paint(Paint.ANTI_ALIAS_FLAG);
    }


    // ******************** State related *************************************
    @Override protected void onRestoreInstanceState(final Parcelable state) {
        final Bundle bundle     = (Bundle) state;
        final Parcelable superState = bundle.getParcelable("superState");
        super.onRestoreInstanceState(superState);
        character    = bundle.getChar("character");
        dot          = bundle.getBoolean("dot");
        segmentColor = bundle.getInt("color");
        glow         = bundle.getBoolean("glow");
    }
    @Override protected Parcelable onSaveInstanceState() {
        final Parcelable superState = super.onSaveInstanceState();
        final Bundle     state      = new Bundle();
        state.putParcelable("superState", superState);
        state.putChar("character", character);
        state.putBoolean("dot", dot);
        state.putInt("color", segmentColor);
        state.putBoolean("glow", glow);
        return state;
    }


    // ******************** Size related **************************************
    @Override protected void onMeasure(final int WIDTH_MEASURE_SPEC, final int HEIGHT_MEASURE_SPEC) {
        // Loggable.log.debug(String.format("WIDTH_MEASURE_SPEC=%s, HEIGHT_MEASURE_SPEC=%s",
        // View.MeasureSpec.toString(WIDTH_MEASURE_SPEC),
        // View.MeasureSpec.toString(HEIGHT_MEASURE_SPEC)));

        final int WIDTH_MODE  = MeasureSpec.getMode(WIDTH_MEASURE_SPEC);
        final int HEIGHT_MODE = MeasureSpec.getMode(HEIGHT_MEASURE_SPEC);
        final int WIDTH_SIZE  = MeasureSpec.getSize(WIDTH_MEASURE_SPEC);
        final int HEIGHT_SIZE = MeasureSpec.getSize(HEIGHT_MEASURE_SPEC);

        final int CHOSEN_WIDTH  = chooseWidth(WIDTH_MODE, WIDTH_SIZE);
        final int CHOSEN_HEIGHT = chooseHeight(HEIGHT_MODE, HEIGHT_SIZE);
        setMeasuredDimension(CHOSEN_WIDTH, CHOSEN_HEIGHT);
    }

    private int chooseWidth(final int MODE, final int WIDTH) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return WIDTH;
            case View.MeasureSpec.UNSPECIFIED:
            default                          :return DEFAULT_WIDTH;
        }
    }
    private int chooseHeight(final int MODE, final int HEIGHT) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return HEIGHT;
            case View.MeasureSpec.UNSPECIFIED:
            default                          : return DEFAULT_HEIGHT;
        }
    }

    @Override protected void onSizeChanged(final int WIDTH, final int HEIGHT, final int OLD_WIDTH, final int OLD_HEIGHT) { resize(); }

    private void resize() {
        final Canvas CANVAS = new Canvas();
        final int    width  = getWidth();
        final int    height = getHeight();
        final float  scale  = Math.min(width, height);
        CANVAS.scale(scale, scale);
        CANVAS.translate((scale == height) ? ((width - scale) / 2) / scale : 0, (scale == width) ? ((height - scale) / 2) / scale : 0);

        inactiveRect.set(0, 0, width, height);
        activeRect.set(0, 0, width, height);

        resizeSegments();
    }

    private void resizeSegments() {
        final float WIDTH  = getWidth();
        final float HEIGHT = getHeight();

        segA.reset();
        segA.moveTo(0.11790393013100436f * WIDTH, 0.014925373134328358f * HEIGHT);
        segA.lineTo(0.11790393013100436f * WIDTH, 0.01791044776119403f * HEIGHT);
        segA.lineTo(0.1965065502183406f * WIDTH, 0.07164179104477612f * HEIGHT);
        segA.lineTo(0.8122270742358079f * WIDTH, 0.07164179104477612f * HEIGHT);
        segA.lineTo(0.8864628820960698f * WIDTH, 0.020895522388059702f * HEIGHT);
        segA.lineTo(0.8864628820960698f * WIDTH, 0.01791044776119403f * HEIGHT);
        segA.lineTo(0.8602620087336245f * WIDTH, 0.0f * HEIGHT);
        segA.lineTo(0.13973799126637554f * WIDTH, 0.0f * HEIGHT);
        segA.lineTo(0.11790393013100436f * WIDTH, 0.014925373134328358f * HEIGHT);
        segA.close();

        segB.reset();
        segB.moveTo(0.8951965065502183f * WIDTH, 0.023880597014925373f * HEIGHT);
        segB.lineTo(0.9213973799126638f * WIDTH, 0.04477611940298507f * HEIGHT);
        segB.lineTo(0.9213973799126638f * WIDTH, 0.08358208955223881f * HEIGHT);
        segB.lineTo(0.8820960698689956f * WIDTH, 0.4626865671641791f * HEIGHT);
        segB.lineTo(0.8296943231441049f * WIDTH, 0.49850746268656715f * HEIGHT);
        segB.lineTo(0.777292576419214f * WIDTH, 0.4626865671641791f * HEIGHT);
        segB.lineTo(0.8209606986899564f * WIDTH, 0.07462686567164178f * HEIGHT);
        segB.lineTo(0.8951965065502183f * WIDTH, 0.023880597014925373f * HEIGHT);
        segB.close();

        segC.reset();
        segC.moveTo(0.8296943231441049f * WIDTH, 0.5014925373134328f * HEIGHT);
        segC.lineTo(0.8777292576419214f * WIDTH, 0.5343283582089552f * HEIGHT);
        segC.lineTo(0.8296943231441049f * WIDTH, 0.9671641791044776f * HEIGHT);
        segC.lineTo(0.8078602620087336f * WIDTH, 0.982089552238806f * HEIGHT);
        segC.lineTo(0.7292576419213974f * WIDTH, 0.9253731343283582f * HEIGHT);
        segC.lineTo(0.7685589519650655f * WIDTH, 0.5432835820895522f * HEIGHT);
        segC.lineTo(0.8296943231441049f * WIDTH, 0.5014925373134328f * HEIGHT);
        segC.close();

        segD.reset();
        segD.moveTo(0.7205240174672489f * WIDTH, 0.9283582089552239f * HEIGHT);
        segD.lineTo(0.1091703056768559f * WIDTH, 0.9283582089552239f * HEIGHT);
        segD.lineTo(0.039301310043668124f * WIDTH, 0.9761194029850746f * HEIGHT);
        segD.lineTo(0.039301310043668124f * WIDTH, 0.982089552238806f * HEIGHT);
        segD.lineTo(0.06550218340611354f * WIDTH, 1.0f * HEIGHT);
        segD.lineTo(0.7816593886462883f * WIDTH, 1.0f * HEIGHT);
        segD.lineTo(0.7991266375545851f * WIDTH, 0.9880597014925373f * HEIGHT);
        segD.lineTo(0.7991266375545851f * WIDTH, 0.982089552238806f * HEIGHT);
        segD.lineTo(0.7205240174672489f * WIDTH, 0.9283582089552239f * HEIGHT);
        segD.close();

        segE.reset();
        segE.moveTo(0.03056768558951965f * WIDTH, 0.9761194029850746f * HEIGHT);
        segE.lineTo(0.0f * WIDTH, 0.9552238805970149f * HEIGHT);
        segE.lineTo(0.0f * WIDTH, 0.9164179104477612f * HEIGHT);
        segE.lineTo(0.043668122270742356f * WIDTH, 0.5373134328358209f * HEIGHT);
        segE.lineTo(0.09606986899563319f * WIDTH, 0.5014925373134328f * HEIGHT);
        segE.lineTo(0.14410480349344978f * WIDTH, 0.5373134328358209f * HEIGHT);
        segE.lineTo(0.10043668122270742f * WIDTH, 0.9253731343283582f * HEIGHT);
        segE.lineTo(0.03056768558951965f * WIDTH, 0.9761194029850746f * HEIGHT);
        segE.close();

        segF.reset();
        segF.moveTo(0.1091703056768559f * WIDTH, 0.01791044776119403f * HEIGHT);
        segF.lineTo(0.18777292576419213f * WIDTH, 0.07462686567164178f * HEIGHT);
        segF.lineTo(0.15283842794759825f * WIDTH, 0.45671641791044776f * HEIGHT);
        segF.lineTo(0.09170305676855896f * WIDTH, 0.49850746268656715f * HEIGHT);
        segF.lineTo(0.043668122270742356f * WIDTH, 0.4626865671641791f * HEIGHT);
        segF.lineTo(0.08733624454148471f * WIDTH, 0.03283582089552239f * HEIGHT);
        segF.lineTo(0.1091703056768559f * WIDTH, 0.01791044776119403f * HEIGHT);
        segF.close();

        segG.reset();
        segG.moveTo(0.7729257641921398f * WIDTH, 0.5373134328358209f * HEIGHT);
        segG.lineTo(0.8253275109170306f * WIDTH, 0.49850746268656715f * HEIGHT);
        segG.lineTo(0.7685589519650655f * WIDTH, 0.4626865671641791f * HEIGHT);
        segG.lineTo(0.1572052401746725f * WIDTH, 0.4626865671641791f * HEIGHT);
        segG.lineTo(0.10043668122270742f * WIDTH, 0.49850746268656715f * HEIGHT);
        segG.lineTo(0.1572052401746725f * WIDTH, 0.5373134328358209f * HEIGHT);
        segG.lineTo(0.7729257641921398f * WIDTH, 0.5373134328358209f * HEIGHT);
        segG.close();

        segDot.reset();
        segDot.addCircle(0.9301310043668122f * WIDTH, 0.9522388059701492f * HEIGHT, 0.07423580786026202f * WIDTH, Direction.CW);
        segDot.close();
    }


    // ******************** Drawing related ***********************************
    @Override protected void onDraw(final Canvas CANVAS) {
        drawSegments(CANVAS, mapping.get(character) == null ? 0 : mapping.get(character));
    }

    private void drawSegments(final Canvas CANVAS, final int SEGMENTS) {
        inactivePaint.setStyle(Style.FILL);
        inactivePaint.setColor(derive(segmentColor, -80));

        activePaint.setStyle(Style.FILL);
        activePaint.setColor(segmentColor);

        for (int segment = 0 ; segment < 7 ; segment++) {
            // Check for each bit in bitmask
            Paint paint = ((1 << segment & SEGMENTS) != 0) ? activePaint : inactivePaint;
            switch(segment) {
                case 0: CANVAS.drawPath(segA, paint); break;
                case 1: CANVAS.drawPath(segB, paint); break;
                case 2: CANVAS.drawPath(segC, paint); break;
                case 3: CANVAS.drawPath(segD, paint); break;
                case 4: CANVAS.drawPath(segE, paint); break;
                case 5: CANVAS.drawPath(segF, paint); break;
                case 6: CANVAS.drawPath(segG, paint); break;
            }
        }
        CANVAS.drawPath(segDot, dot ? activePaint : inactivePaint);
    };


    // ******************** Methods *******************************************
    public char getCharacter() { return character; }
    public void setCharacter(final char CHARACTER) {
        character = CHARACTER;
        if (!mapping.containsKey(character)) character = ' ';
        invalidate();
    }

    public boolean isDot() { return dot; }
    public void setDot(final boolean DOT) {
        dot = DOT;
        invalidate();
    }

    public int getSegmentColor() { return segmentColor; }
    public void setSegmentColor(final int SEGMENT_COLOR) {
        int[] rgb = Util.intToRGB(SEGMENT_COLOR);
        segmentColor = SEGMENT_COLOR;
        invalidate();
    }

    public boolean isGlow() { return glow; }
    public void setGlow(final boolean GLOW) {
        glow = GLOW;
        invalidate();
    }

    public int derive(final String COLOR, final float PERCENTAGE) {
        return derive(Color.parseColor(COLOR), PERCENTAGE);
    }
    public int derive(final int COLOR, final float PERCENTAGE) {
        float   p   = clamp(-100f, 100f, PERCENTAGE) / 100.0f;
        float[] hsv = new float[3];
        Color.colorToHSV(COLOR, hsv);
        hsv[2] = clamp(0f, 1f, hsv[2] + (p * hsv[2]));
        return Color.HSVToColor(hsv);
    }

    private float clamp(final float MIN, final float MAX, final float VALUE) {
        if (VALUE < MIN) return MIN;
        if (VALUE > MAX) return MAX;
        return VALUE;
    }
}

