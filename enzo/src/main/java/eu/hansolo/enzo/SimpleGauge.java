package eu.hansolo.enzo;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.*;
import android.graphics.Bitmap.Config;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import eu.hansolo.enzo.SectionEvent.SectionEventType;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Created by hansolo on 17.02.15.
 */
public class SimpleGauge extends View {
    private static final String TAG = "SimpleGauge";

    public static final  int   DEFAULT_WIDTH  = 300;
    public static final  int   DEFAULT_HEIGHT = 300;
    public static final  float TOP            = 0.0f;
    public static final  float LEFT           = 0.0f;
    public static final  float RIGHT          = 1.0f;
    public static final  float BOTTOM         = 1.0f;
    public static final  float CENTER         = 0.5f;
    public static final  float MIN_VALUE      = 0.0f;
    public static final  float MAX_VALUE      = 100.0f;
    private static final float START_ANGLE    = -225f;
    private static final float ANGLE_RANGE    = 270;
    private static       int   size           = 0;

    private float           value;
    private float           oldValue;
    private float           currentValue;
    private float           minValue;
    private float           maxValue;
    private float           range;
    private float           angleStep;
    private String          title;
    private boolean         titleVisible;
    private String          unit;
    private boolean         unitVisible;
    private int             decimals;
    private int             needleColor;
    private int             frameColor;
    private int             textColor;
    private int             sectionTextColor;
    private int             section0Color;
    private float           section0Start;
    private float           section0Stop;
    private String          section0Text;
    private Drawable        section0Image;
    private int             section1Color;
    private float           section1Start;
    private float           section1Stop;
    private String          section1Text;
    private Drawable        section1Image;
    private int             section2Color;
    private float           section2Start;
    private float           section2Stop;
    private String          section2Text;
    private Drawable        section2Image;
    private int             section3Color;
    private float           section3Start;
    private float           section3Stop;
    private String          section3Text;
    private Drawable        section3Image;
    private int             section4Color;
    private float           section4Start;
    private float           section4Stop;
    private String          section4Text;
    private Drawable        section4Image;
    private int             section5Color;
    private float           section5Start;
    private float           section5Stop;
    private String          section5Text;
    private Drawable        section5Image;
    private int             section6Color;
    private float           section6Start;
    private float           section6Stop;
    private String          section6Text;
    private Drawable        section6Image;
    private int             section7Color;
    private float           section7Start;
    private float           section7Stop;
    private String          section7Text;
    private Drawable        section7Image;
    private int             section8Color;
    private float           section8Start;
    private float           section8Stop;
    private String          section8Text;
    private Drawable        section8Image;
    private int             section9Color;
    private float           section9Start;
    private float           section9Stop;
    private String          section9Text;
    private Drawable        section9Image;
    private List<Section>   sections;
    private boolean         sectionIconVisible;
    private boolean         sectionTextVisible;
    private RectF           backgroundRect;
    private RectF           frameRect;
    private RectF           needleRect;
    private Paint           backgroundPaint;
    private Paint           needlePaint;
    private Paint           valuePaint;
    private Paint           titlePaint;
    private Path            needlePath;
    private ValueAnimator   animator;
    private boolean         animated;
    private long            animationDuration;


    // ******************** Constructors **************************************
    public SimpleGauge(final Context CONTEXT, final AttributeSet ATTRS) {
        this(CONTEXT, ATTRS, 0);
    }
    public SimpleGauge(final Context CONTEXT, final AttributeSet ATTRS, final int DEF_STYLE) {
        super(CONTEXT, ATTRS, DEF_STYLE);
        sections             = new ArrayList<Section>(10);
        needlePath           = new Path();
        animator             = ValueAnimator.ofFloat(minValue, minValue);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        initAttributes(CONTEXT, ATTRS, DEF_STYLE);
        init();
    }


    // ******************** Initialization ************************************
    private void initAttributes(final Context CONTEXT, final AttributeSet ATTRS, final int DEF_STYLE) {
        final TypedArray a = CONTEXT.obtainStyledAttributes(ATTRS, R.styleable.SimpleGauge, DEF_STYLE, 0);

        minValue           = a.getFloat(R.styleable.SimpleGauge_minValueSimpleGauge, MIN_VALUE);
        maxValue           = a.getFloat(R.styleable.SimpleGauge_maxValueSimpleGauge, MAX_VALUE);
        value              = a.getFloat(R.styleable.SimpleGauge_valueSimpleGauge, minValue);
        title              = a.getString(R.styleable.SimpleGauge_titleSimpleGauge) == null ? "" : a.getString(R.styleable.SimpleGauge_titleSimpleGauge);
        titleVisible       = a.getBoolean(R.styleable.SimpleGauge_titleVisibleSimpleGauge, false);
        unit               = a.getString(R.styleable.SimpleGauge_unitSimpleGauge) == null ? "" : a.getString(R.styleable.SimpleGauge_unitSimpleGauge);
        unitVisible        = a.getBoolean(R.styleable.SimpleGauge_unitVisibleSimpleGauge, false);
        animated           = a.getBoolean(R.styleable.SimpleGauge_animatedSimpleGauge, true);
        animationDuration  = a.getInt(R.styleable.SimpleGauge_animationDurationSimpleGauge, 800);
        decimals           = a.getInt(R.styleable.SimpleGauge_decimalsSimpleGauge, 0);
        needleColor        = a.getColor(R.styleable.SimpleGauge_needleColorSimpleGauge, Color.rgb(90, 97, 95));
        frameColor         = a.getColor(R.styleable.SimpleGauge_frameColorSimpleGauge, Color.WHITE);
        textColor          = a.getColor(R.styleable.SimpleGauge_textColorSimpleGauge, Color.WHITE);
        sectionTextColor   = a.getColor(R.styleable.SimpleGauge_sectionTextColorSimpleGauge, Color.WHITE);
        sectionIconVisible = a.getBoolean(R.styleable.SimpleGauge_sectionIconVisibleSimpleGauge, false);
        sectionTextVisible = a.getBoolean(R.styleable.SimpleGauge_sectionTextVisibleSimpleGauge, false);
        section0Color      = a.getColor(R.styleable.SimpleGauge_section0ColorSimpleGauge, Color.parseColor("#97b329"));
        section0Start      = a.getFloat(R.styleable.SimpleGauge_section0StartSimpleGauge, 0f);
        section0Stop       = a.getFloat(R.styleable.SimpleGauge_section0StopSimpleGauge, 0f);
        section0Text       = a.getString(R.styleable.SimpleGauge_section0TextSimpleGauge);
        section0Image      = a.getDrawable(R.styleable.SimpleGauge_section0ImageSimpleGauge);
        section1Color      = a.getColor(R.styleable.SimpleGauge_section1ColorSimpleGauge, Color.parseColor("#aacc2a"));
        section1Start      = a.getFloat(R.styleable.SimpleGauge_section1StartSimpleGauge, 0f);
        section1Stop       = a.getFloat(R.styleable.SimpleGauge_section1StopSimpleGauge, 0f);
        section1Text       = a.getString(R.styleable.SimpleGauge_section1TextSimpleGauge);
        section1Image      = a.getDrawable(R.styleable.SimpleGauge_section1ImageSimpleGauge);
        section2Color      = a.getColor(R.styleable.SimpleGauge_section2ColorSimpleGauge, Color.parseColor("#d4ea35"));
        section2Start      = a.getFloat(R.styleable.SimpleGauge_section2StartSimpleGauge, 0f);
        section2Stop       = a.getFloat(R.styleable.SimpleGauge_section2StopSimpleGauge, 0f);
        section2Text       = a.getString(R.styleable.SimpleGauge_section2TextSimpleGauge);
        section2Image      = a.getDrawable(R.styleable.SimpleGauge_section2ImageSimpleGauge);
        section3Color      = a.getColor(R.styleable.SimpleGauge_section3ColorSimpleGauge, Color.parseColor("#f2de31"));
        section3Start      = a.getFloat(R.styleable.SimpleGauge_section3StartSimpleGauge, 0f);
        section3Stop       = a.getFloat(R.styleable.SimpleGauge_section3StopSimpleGauge, 0f);
        section3Text       = a.getString(R.styleable.SimpleGauge_section3TextSimpleGauge);
        section3Image      = a.getDrawable(R.styleable.SimpleGauge_section3ImageSimpleGauge);
        section4Color      = a.getColor(R.styleable.SimpleGauge_section4ColorSimpleGauge, Color.parseColor("#fccb2e"));
        section4Start      = a.getFloat(R.styleable.SimpleGauge_section4StartSimpleGauge, 0f);
        section4Stop       = a.getFloat(R.styleable.SimpleGauge_section4StopSimpleGauge, 0f);
        section4Text       = a.getString(R.styleable.SimpleGauge_section4TextSimpleGauge);
        section4Image      = a.getDrawable(R.styleable.SimpleGauge_section4ImageSimpleGauge);
        section5Color      = a.getColor(R.styleable.SimpleGauge_section5ColorSimpleGauge, Color.parseColor("#f3a429"));
        section5Start      = a.getFloat(R.styleable.SimpleGauge_section5StartSimpleGauge, 0f);
        section5Stop       = a.getFloat(R.styleable.SimpleGauge_section5StopSimpleGauge, 0f);
        section5Text       = a.getString(R.styleable.SimpleGauge_section5TextSimpleGauge);
        section5Image      = a.getDrawable(R.styleable.SimpleGauge_section5ImageSimpleGauge);
        section6Color      = a.getColor(R.styleable.SimpleGauge_section6ColorSimpleGauge, Color.parseColor("#f18c23"));
        section6Start      = a.getFloat(R.styleable.SimpleGauge_section6StartSimpleGauge, 0f);
        section6Stop       = a.getFloat(R.styleable.SimpleGauge_section6StopSimpleGauge, 0f);
        section6Text       = a.getString(R.styleable.SimpleGauge_section6TextSimpleGauge);
        section6Image      = a.getDrawable(R.styleable.SimpleGauge_section6ImageSimpleGauge);
        section7Color      = a.getColor(R.styleable.SimpleGauge_section7ColorSimpleGauge, Color.parseColor("#f65821"));
        section7Start      = a.getFloat(R.styleable.SimpleGauge_section7StartSimpleGauge, 0f);
        section7Stop       = a.getFloat(R.styleable.SimpleGauge_section7StopSimpleGauge, 0f);
        section7Text       = a.getString(R.styleable.SimpleGauge_section7TextSimpleGauge);
        section7Image      = a.getDrawable(R.styleable.SimpleGauge_section7ImageSimpleGauge);
        section8Color      = a.getColor(R.styleable.SimpleGauge_section8ColorSimpleGauge, Color.parseColor("#f3351f"));
        section8Start      = a.getFloat(R.styleable.SimpleGauge_section8StartSimpleGauge, 0f);
        section8Stop       = a.getFloat(R.styleable.SimpleGauge_section8StopSimpleGauge, 0f);
        section8Text       = a.getString(R.styleable.SimpleGauge_section8TextSimpleGauge);
        section8Image      = a.getDrawable(R.styleable.SimpleGauge_section8ImageSimpleGauge);
        section9Color      = a.getColor(R.styleable.SimpleGauge_section9ColorSimpleGauge, Color.parseColor("#f61319"));
        section9Start      = a.getFloat(R.styleable.SimpleGauge_section9StartSimpleGauge, 0f);
        section9Stop       = a.getFloat(R.styleable.SimpleGauge_section9StopSimpleGauge, 0f);
        section9Text       = a.getString(R.styleable.SimpleGauge_section9TextSimpleGauge);
        section9Image      = a.getDrawable(R.styleable.SimpleGauge_section9ImageSimpleGauge);

        a.recycle();

        if (section0Stop > minValue) sections.add(new Section(Util.clamp(minValue, maxValue, section0Start), Util.clamp(minValue, maxValue, section0Stop), section0Color, section0Text, section0Image));
        if (section1Stop > minValue) sections.add(new Section(Util.clamp(minValue, maxValue, section1Start), Util.clamp(minValue, maxValue, section1Stop), section1Color, section1Text, section1Image));
        if (section2Stop > minValue) sections.add(new Section(Util.clamp(minValue, maxValue, section2Start), Util.clamp(minValue, maxValue, section2Stop), section2Color, section2Text, section2Image));
        if (section3Stop > minValue) sections.add(new Section(Util.clamp(minValue, maxValue, section3Start), Util.clamp(minValue, maxValue, section3Stop), section3Color, section3Text, section3Image));
        if (section4Stop > minValue) sections.add(new Section(Util.clamp(minValue, maxValue, section4Start), Util.clamp(minValue, maxValue, section4Stop), section4Color, section4Text, section4Image));
        if (section5Stop > minValue) sections.add(new Section(Util.clamp(minValue, maxValue, section5Start), Util.clamp(minValue, maxValue, section5Stop), section5Color, section5Text, section5Image));
        if (section6Stop > minValue) sections.add(new Section(Util.clamp(minValue, maxValue, section6Start), Util.clamp(minValue, maxValue, section6Stop), section6Color, section6Text, section6Image));
        if (section7Stop > minValue) sections.add(new Section(Util.clamp(minValue, maxValue, section7Start), Util.clamp(minValue, maxValue, section7Stop), section7Color, section7Text, section7Image));
        if (section8Stop > minValue) sections.add(new Section(Util.clamp(minValue, maxValue, section8Start), Util.clamp(minValue, maxValue, section8Stop), section8Color, section8Text, section8Image));
        if (section9Stop > minValue) sections.add(new Section(Util.clamp(minValue, maxValue, section9Start), Util.clamp(minValue, maxValue, section9Stop), section9Color, section9Text, section9Image));

        animator.setDuration(animationDuration);
        range     = maxValue - minValue;
        angleStep = ANGLE_RANGE / range;
    }
    private void init() {
        initBounds();
        initGraphics();
        initAnimator();
    }
    private void initBounds() {
        backgroundRect = new RectF(LEFT, TOP, RIGHT, BOTTOM);
        needleRect     = new RectF(LEFT, TOP, RIGHT, BOTTOM);
    }
    private void initGraphics() {
        backgroundPaint     = getBackgroundPaint();
        needlePaint         = getNeedlePaint();
        valuePaint          = getValuePaint();
        titlePaint          = getTitlePaint();
    }
    private void initAnimator() {
        animator.addUpdateListener(new AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator animation) {
                value = (Float) animation.getAnimatedValue();
                for (Section section : sections) {
                    if (section.contains(value)) {
                        section.fireSectionEvent(new SectionEvent(SimpleGauge.this, SectionEventType.ENTERING_SECTION));
                        break;
                    }
                }
                invalidate();
            }
        });
    }


    // ******************** Graphics related **********************************
    private Paint getBackgroundPaint() {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        return paint;
    }
    private Paint getNeedlePaint() {
        final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        return paint;
    }
    private Paint getValuePaint() {
        final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setColor(textColor);
        paint.setStyle(Style.FILL);
        paint.setTextSize(size * 0.145f);
        paint.setTextAlign(Align.LEFT);
        paint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "Roboto-Medium.ttf"));
        return paint;
    }
    private Paint getTitlePaint() {
        final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setColor(textColor);
        paint.setStyle(Style.FILL);
        paint.setTextSize(size * 0.045f);
        paint.setTextAlign(Align.LEFT);
        paint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "Roboto-Medium.ttf"));
        return paint;
    }


    // ******************** State related *************************************
    @Override protected void onRestoreInstanceState(final Parcelable state) {
        final Bundle bundle     = (Bundle) state;
        final Parcelable superState = bundle.getParcelable("superState");
        super.onRestoreInstanceState(superState);
        value = bundle.getFloat("value");
        unit  = bundle.getString("unit");
        title = bundle.getString("title");
    }
    @Override protected Parcelable onSaveInstanceState() {
        final Parcelable superState = super.onSaveInstanceState();
        final Bundle     state      = new Bundle();
        state.putParcelable("superState", superState);
        state.putFloat("value", value);
        state.putString("unit", unit);
        state.putString("title", title);
        return state;
    }


    // ******************** Size related **************************************
    @Override protected void onMeasure(final int WIDTH_MEASURE_SPEC, final int HEIGHT_MEASURE_SPEC) {
        // Loggable.log.debug(String.format("WIDTH_MEASURE_SPEC=%s, HEIGHT_MEASURE_SPEC=%s",
        // View.MeasureSpec.toString(WIDTH_MEASURE_SPEC),
        // View.MeasureSpec.toString(HEIGHT_MEASURE_SPEC)));

        final int WIDTH_MODE  = MeasureSpec.getMode(WIDTH_MEASURE_SPEC);
        final int HEIGHT_MODE = MeasureSpec.getMode(HEIGHT_MEASURE_SPEC);
        final int WIDTH_SIZE  = MeasureSpec.getSize(WIDTH_MEASURE_SPEC);
        final int HEIGHT_SIZE = MeasureSpec.getSize(HEIGHT_MEASURE_SPEC);

        final int CHOSEN_WIDTH  = chooseWidth(WIDTH_MODE, WIDTH_SIZE);
        final int CHOSEN_HEIGHT = chooseHeight(HEIGHT_MODE, HEIGHT_SIZE);
        setMeasuredDimension(CHOSEN_WIDTH, CHOSEN_HEIGHT);
    }

    private int chooseWidth(final int MODE, final int WIDTH) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return WIDTH;
            case View.MeasureSpec.UNSPECIFIED:
            default                          :return DEFAULT_WIDTH;
        }
    }
    private int chooseHeight(final int MODE, final int HEIGHT) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return HEIGHT;
            case View.MeasureSpec.UNSPECIFIED:
            default                          : return DEFAULT_HEIGHT;
        }
    }

    @Override protected void onSizeChanged(final int WIDTH, final int HEIGHT, final int OLD_WIDTH, final int OLD_HEIGHT) { resize(); }

    private void resize() {
        final Canvas CANVAS = new Canvas();
        final int    width  = getWidth();
        final int    height = getHeight();
        final float  scale  = Math.min(width, height);
        size = width < height ? width : height;
        CANVAS.scale(scale, scale);
        CANVAS.translate((scale == height) ? ((width - scale) / 2) / scale : 0, (scale == width) ? ((height - scale) / 2) / scale : 0);

        backgroundRect.set(0, 0, size, size);
        needleRect.set(0, 0, size, size);

        resizeBackgroundPath();
        resizeNeedlePath();

        valuePaint.setTextSize(size * 0.145f);
        titlePaint.setTextSize(size * 0.045f);
    }
    private void resizeBackgroundPath() {
        frameRect = new RectF(size * 0.015f, size * 0.015f, size - (size * 0.03f), size - (size * 0.03f));
    }
    private void resizeNeedlePath() {
        needlePath.reset();
        needlePath.moveTo(0.275f * size, 0.5f * size);
        needlePath.cubicTo(0.275f * size, 0.62426575f * size,
                           0.37573425f * size, 0.725f * size,
                           0.5f * size, 0.725f * size);
        needlePath.cubicTo(0.62426575f * size, 0.725f * size,
                           0.725f * size, 0.62426575f * size,
                           0.725f * size, 0.5f * size);
        needlePath.cubicTo(0.725f * size, 0.3891265f * size,
                           0.6448105f * size, 0.296985f * size,
                           0.5392625f * size, 0.2784125f * size);
        needlePath.lineTo(0.5f * size, 0.0225f * size);
        needlePath.lineTo(0.4607375f * size, 0.2784125f * size);
        needlePath.cubicTo(0.3551895f * size, 0.296985f * size,
                           0.275f * size, 0.3891265f * size,
                           0.275f * size, 0.5f * size);
        needlePath.close();
    }


    // ******************** Drawing related ***********************************
    @Override protected void onDraw(final Canvas CANVAS) {
        drawBackground(CANVAS);
        drawSections(CANVAS);
        drawNeedle(CANVAS);
        drawForeground(CANVAS);
    }

    private void drawBackground(final Canvas CANVAS) {
        backgroundPaint.setColor(frameColor);
        backgroundPaint.setStyle(Style.STROKE);
        backgroundPaint.setStrokeWidth(size * 0.03f);
        CANVAS.drawArc(frameRect, START_ANGLE, ANGLE_RANGE, true, backgroundPaint);
    }
    private void drawSections(final Canvas CANVAS) {
        backgroundPaint.setStyle(Style.FILL);
        float sectionStartAngle;
        float sectionStopAngle;
        float sinValue;
        float cosValue;
        for (Section section : sections) {
            backgroundPaint.setColor(section.getColor());

            if (section.getStart() < minValue && section.getStop() < maxValue) {
                sectionStartAngle = START_ANGLE + minValue * angleStep;
            } else {
                sectionStartAngle = START_ANGLE + (section.getStart() - minValue) * angleStep;
            }

            sectionStopAngle  = (section.getStop() - section.getStart()) * angleStep;
            CANVAS.drawArc(frameRect, sectionStartAngle, sectionStopAngle, true, backgroundPaint);

            // Draw Section Text
            if (isSectionTextVisible()) {
                sinValue = (float) -Math.sin(Math.toRadians(270 - sectionStartAngle - sectionStopAngle * 0.5));
                cosValue = (float) -Math.cos(Math.toRadians(270 -sectionStartAngle - sectionStopAngle * 0.5));
                Point textPoint = new Point((int) (size * 0.5f + size * 0.365f * sinValue), (int) (size * 0.5f + size * 0.365f * cosValue));
                backgroundPaint.setTextSize(0.08f * size);
                backgroundPaint.setTextAlign(Align.CENTER);
                backgroundPaint.setColor(getSectionTextColor());
                CANVAS.drawText(section.getText(), textPoint.x, textPoint.y, backgroundPaint);
            } else if (isSectionIconVisible()) {
            // Draw Section Icon
                if (null != section.getImage()) {
                    Bitmap icon = convertToBitmap(section.getImage(), (int) (size * 0.12f), (int) (size * 0.12f));
                    sinValue = (float) -Math.sin(Math.toRadians(270 - sectionStartAngle - sectionStopAngle * 0.5));
                    cosValue = (float) -Math.cos(Math.toRadians(270 -sectionStartAngle - sectionStopAngle * 0.5));
                    Point iconPoint = new Point((int) (size * 0.5f + size * 0.365f * sinValue), (int) (size * 0.5f + size * 0.365f * cosValue));
                    CANVAS.drawBitmap(icon, iconPoint.x - size * 0.06f, iconPoint.y - size * 0.06f, backgroundPaint);
                }
            }
        }
    }
    private void drawNeedle(final Canvas CANVAS) {
        CANVAS.save();
        CANVAS.rotate(angleStep * (value - minValue) - START_ANGLE, size * 0.5f, size * 0.5f);

        needlePaint.setStyle(Style.FILL);
        needlePaint.setColor(needleColor);
        CANVAS.drawPath(needlePath, needlePaint);
        needlePaint.setStyle(Style.STROKE);
        needlePaint.setColor(frameColor);
        needlePaint.setStrokeWidth(size * 0.03f);
        CANVAS.drawPath(needlePath, needlePaint);

        CANVAS.restore();
    }
    private void drawForeground(final Canvas CANVAS) {
        // Value + Unit
        valuePaint.setTextAlign(Align.CENTER);
        String valueAndUnit = valueString(value) + ((unitVisible && !unit.isEmpty()) ? unit : "");
        float  textWidth    = valuePaint.measureText(valueAndUnit);
        if (textWidth > 0.395f * size) {
            float decrement = 0f;
            while (textWidth > 0.395f * size && valuePaint.getTextSize() > 0f) {
                valuePaint.setTextSize(size * (0.145f - decrement));
                textWidth = valuePaint.measureText(valueAndUnit);
                decrement += 0.01f;
            }
        }
        CANVAS.drawText(valueAndUnit, size *  0.5f, size * ((!titleVisible || title.isEmpty()) ? 0.54f : 0.52f), valuePaint);

        // Title
        titlePaint.setTextAlign(Align.CENTER);
        if (titleVisible) CANVAS.drawText(title, 0.5f * size, size * 0.59f, titlePaint);
    }


    // ******************** Methods *******************************************
    public float getValue() { return value; }
    public void setValue(final float VALUE) {
        oldValue     = value;
        if (animated) {
            currentValue = Util.clamp(minValue, maxValue, VALUE);
            if (animator.isRunning()) animator.cancel();
            animator.setFloatValues(oldValue, currentValue);
            animator.start();
        } else {
            value = Util.clamp(minValue, maxValue, VALUE);
            for (Section section : sections) {
                if (section.contains(value)) {
                    section.fireSectionEvent(new SectionEvent(SimpleGauge.this, SectionEventType.ENTERING_SECTION));
                    break;
                }
            }
            invalidate();
        }
    }

    public String getTitle() {return title; }
    public void setTitle(final String TITLE) {
        title = TITLE;
        invalidate();
    }

    public String getUnit() { return unit; }
    public void setUnit(final String UNIT) {
        unit = UNIT;
        invalidate();
    }

    public boolean isSectionTextVisible() { return sectionTextVisible; }
    public void setSectionTextVisible(final boolean SECTION_TEXT_VISIBLE) { sectionTextVisible = SECTION_TEXT_VISIBLE; }

    public boolean isSectionIconVisible() { return sectionIconVisible; }
    public void setSectionIconVisible(final boolean SECTION_ICON_VISIBLE) { sectionIconVisible = SECTION_ICON_VISIBLE; }

    public int getSectionTextColor() { return sectionTextColor; }
    public void setSectionTextColor(final int SECTION_TEXT_COLOR) { sectionTextColor = SECTION_TEXT_COLOR; }

    public List<Section> getSections() { return sections; }


    // ******************** Utility methods ***********************************
    private String valueString(final float VALUE) { return String.format(Locale.US, "%." + Integer.toString(decimals) + "f", VALUE); }

    public Bitmap convertToBitmap(Drawable drawable, int widthPixels, int heightPixels) {
        Bitmap mutableBitmap = Bitmap.createBitmap(widthPixels, heightPixels, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mutableBitmap);
        drawable.setBounds(0, 0, widthPixels, heightPixels);
        drawable.draw(canvas);
        return mutableBitmap;
    }
}

