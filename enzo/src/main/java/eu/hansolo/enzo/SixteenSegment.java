package eu.hansolo.enzo;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by hansolo on 17.10.15.
 */
public class SixteenSegment extends View {
    private static final String                 TAG = "SixteenSegment";

    public static final int                     DEFAULT_WIDTH  = 268;
    public static final int                     DEFAULT_HEIGHT = 357;
    public static final float                   TOP            = 0.0f;
    public static final float                   LEFT           = 0.0f;
    public static final float                   RIGHT          = 1.0f;
    public static final float                   BOTTOM         = 1.0f;
    public static final float                   CENTER         = 0.5f;
    private             int                     a1,a2,b,c,d2,d1,e,f,g,h,i,k,l,m,n,p;
    private             Map<Character, Integer> mapping;
    private             char                    character;
    private             boolean                 dot;
    private             int                     segmentColor;
    private             boolean                 glow;
    private             RectF                   inactiveRect;
    private             RectF                   activeRect;
    private             Paint                   inactivePaint;
    private             Paint                   activePaint;
    private             float                   aspectRatio;
    private             Path                    segA1;
    private             Path                    segA2;
    private             Path                    segB;
    private             Path                    segC;
    private             Path                    segD2;
    private             Path                    segD1;
    private             Path                    segE;
    private             Path                    segF;
    private             Path                    segG;
    private             Path                    segH;
    private             Path                    segI;
    private             Path                    segK;
    private             Path                    segL;
    private             Path                    segM;
    private             Path                    segN;
    private             Path                    segP;
    private             Path                    segDot;


    // ******************** Constructors **************************************
    public SixteenSegment(final Context CONTEXT, final AttributeSet ATTRS) {
        this(CONTEXT, ATTRS, 0);
    }
    public SixteenSegment(final Context CONTEXT, final AttributeSet ATTRS, final int DEF_STYLE) {
        super(CONTEXT, ATTRS, DEF_STYLE);

        a1 = 1 << 0;
        a2 = 1 << 1;
        b  = 1 << 2;
        c  = 1 << 3;
        d2 = 1 << 4;
        d1 = 1 << 5;
        e  = 1 << 6;
        f  = 1 << 7;
        g  = 1 << 8;
        h  = 1 << 9;
        i  = 1 << 10;
        k  = 1 << 11;
        l  = 1 << 12;
        m  = 1 << 13;
        n  = 1 << 14;
        p  = 1 << 15;

        mapping = new HashMap<Character, Integer>(48);
        mapping.put(' ', 0);
            // 0-9
        mapping.put('0', a1 | a2 | b | c | d2 | d1 | e | f);
        mapping.put('1', b | c);
        mapping.put('2', a1 | a2 | b | d2 | d1 | e | p | k);
        mapping.put('3', a1 | a2 | b | c | d2 | d1 | p | k);
        mapping.put('4', b | c | f | p | k);
        mapping.put('5', a1 | a2 | c | d2 | d1 | f | p | k);
        mapping.put('6', a1 | a2 | c | d2 | d1 | e | f | p | k);
        mapping.put('7', a1 | a2 | b | c);
        mapping.put('8', a1 | a2 | b | c | d1 | d2 | e | f | p | k);
        mapping.put('9', a1 | a2 | b | c | f | p | k);
            // * + , - . /
        mapping.put('*', g | h | i | k| l | m | n | p);
        mapping.put('+', h | k | m | p);
        mapping.put(',', n);
        mapping.put('-', p | k);
        mapping.put('/', i | n);
            // A-Z
        mapping.put('A', a1 | a2 | b | c | e | f | p | k);
        mapping.put('B', a1 | a2 | b | c | d2 | d1 | h | m | k);
        mapping.put('C', a1 | a2 | d2 | d1 | e | f);
        mapping.put('D', a1 | a2 | b | c | d2 | d1 | h | m);
        mapping.put('E', a1 | a2 | d2 | d1 | e | f | p | k);
        mapping.put('F', a1 | a2 | e | f | p | k);
        mapping.put('G', a1 | a2 | c | d2 | d1 | e | f | k);
        mapping.put('H', b | c | e | f | p | k);
        mapping.put('I', a1 | a2 | d2 | d1 | m | h);
        mapping.put('J', b | c | d2 | d1 | e);
        mapping.put('K', e | f | i | l | p);
        mapping.put('L', d2 | d1 | e | f);
        mapping.put('M', b | c | e | f | g | i);
        mapping.put('N', b | c | e | f | g | l);
        mapping.put('O', a1 | a2 | b | c | d2 | d1 | e | f);
        mapping.put('P', a1 | a2 | b | e | f | p | k);
        mapping.put('Q', a1 | a2 | b | c | d2 | d1 | e | f | l);
        mapping.put('R', a1 | a2 | b | e | f | p | k | l);
        mapping.put('S', a1 | a2 | c | d2 | d1 | g | k);
        mapping.put('T', a1 | a2 | h | m);
        mapping.put('U', b | c | d2 | d1 | e | f);
        mapping.put('V', e | f | i | n);
        mapping.put('W', b | c | e | f | l | n);
        mapping.put('X', g | i | l | n);
        mapping.put('Y', g | i | m);
        mapping.put('Z', a1 | a2 | d2 | d1 | i | n);
        
        initAttributes(CONTEXT, ATTRS, DEF_STYLE);
        init();
    }


    // ******************** Initialization ************************************
    private void initAttributes(final Context context, final AttributeSet attrs, final int defStyle) {
        final TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.Led, defStyle, 0);
        character    = ta.getString(R.styleable.SixteenSegment_characterSixteenSegment) == null ? '0' : ta.getString(R.styleable.SixteenSegment_characterSixteenSegment).charAt(0);
        dot          = ta.getBoolean(R.styleable.SixteenSegment_dotSixteenSegment, true);
        segmentColor = ta.getInt(R.styleable.SixteenSegment_segmentColorSixteenSegment, Color.rgb(255, 0, 0));
        glow         = ta.getBoolean(R.styleable.SixteenSegment_glowSixteenSegment, false);
        ta.recycle();
        aspectRatio  = DEFAULT_HEIGHT / DEFAULT_WIDTH;
    }
    private void init() {
        initBounds();
        initGraphics();
    }
    private void initBounds() {
        inactiveRect = new RectF(LEFT, TOP, RIGHT, BOTTOM);
        activeRect   = new RectF(LEFT, TOP, RIGHT, BOTTOM);
    }
    private void initGraphics() {
        segA1         = new Path();
        segA2         = new Path();
        segB          = new Path();
        segC          = new Path();
        segD2         = new Path();
        segD1         = new Path();
        segE          = new Path();
        segF          = new Path();
        segG          = new Path();
        segH          = new Path();
        segI          = new Path();
        segK          = new Path();
        segL          = new Path();
        segM          = new Path();
        segN          = new Path();
        segP          = new Path();
        segDot        = new Path();
        inactivePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        activePaint   = new Paint(Paint.ANTI_ALIAS_FLAG);
    }


    // ******************** State related *************************************
    @Override protected void onRestoreInstanceState(final Parcelable state) {
        final Bundle bundle     = (Bundle) state;
        final Parcelable superState = bundle.getParcelable("superState");
        super.onRestoreInstanceState(superState);
        character    = bundle.getChar("character");
        dot          = bundle.getBoolean("dot");
        segmentColor = bundle.getInt("color");
        glow         = bundle.getBoolean("glow");
    }
    @Override protected Parcelable onSaveInstanceState() {
        final Parcelable superState = super.onSaveInstanceState();
        final Bundle     state      = new Bundle();
        state.putParcelable("superState", superState);
        state.putChar("character", character);
        state.putBoolean("dot", dot);
        state.putInt("color", segmentColor);
        state.putBoolean("glow", glow);
        return state;
    }


    // ******************** Size related **************************************
    @Override protected void onMeasure(final int WIDTH_MEASURE_SPEC, final int HEIGHT_MEASURE_SPEC) {
        // Loggable.log.debug(String.format("WIDTH_MEASURE_SPEC=%s, HEIGHT_MEASURE_SPEC=%s",
        // View.MeasureSpec.toString(WIDTH_MEASURE_SPEC),
        // View.MeasureSpec.toString(HEIGHT_MEASURE_SPEC)));

        final int WIDTH_MODE  = MeasureSpec.getMode(WIDTH_MEASURE_SPEC);
        final int HEIGHT_MODE = MeasureSpec.getMode(HEIGHT_MEASURE_SPEC);
        final int WIDTH_SIZE  = MeasureSpec.getSize(WIDTH_MEASURE_SPEC);
        final int HEIGHT_SIZE = MeasureSpec.getSize(HEIGHT_MEASURE_SPEC);

        final int CHOSEN_WIDTH  = chooseWidth(WIDTH_MODE, WIDTH_SIZE);
        final int CHOSEN_HEIGHT = chooseHeight(HEIGHT_MODE, HEIGHT_SIZE);
        setMeasuredDimension(CHOSEN_WIDTH, CHOSEN_HEIGHT);
    }

    private int chooseWidth(final int MODE, final int WIDTH) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return WIDTH;
            case View.MeasureSpec.UNSPECIFIED:
            default                          :return DEFAULT_WIDTH;
        }
    }
    private int chooseHeight(final int MODE, final int HEIGHT) {
        switch (MODE) {
            case View.MeasureSpec.AT_MOST    :
            case View.MeasureSpec.EXACTLY    : return HEIGHT;
            case View.MeasureSpec.UNSPECIFIED:
            default                          : return DEFAULT_HEIGHT;
        }
    }

    @Override protected void onSizeChanged(final int WIDTH, final int HEIGHT, final int OLD_WIDTH, final int OLD_HEIGHT) { resize(); }

    private void resize() {
        final Canvas CANVAS = new Canvas();
        final int    width  = getWidth();
        final int    height = getHeight();
        final float  scale  = Math.min(width, height);
        CANVAS.scale(scale, scale);
        CANVAS.translate((scale == height) ? ((width - scale) / 2) / scale : 0, (scale == width) ? ((height - scale) / 2) / scale : 0);

        inactiveRect.set(0, 0, width, height);
        activeRect.set(0, 0, width, height);

        resizeSegments();
    }

    private void resizeSegments() {
        final float width  = getWidth();
        final float height = getHeight();

        segA1.reset();
        segA1.moveTo(0.11790393013100436f * width, 0.014925373134328358f * height);
        segA1.lineTo(0.13973799126637554f * width, 0.0f * height);
        segA1.lineTo(0.4497816593886463f * width, 0.0f * height);
        segA1.lineTo(0.4978165938864629f * width, 0.029850746268656716f * height);
        segA1.lineTo(0.4978165938864629f * width, 0.03582089552238806f * height);
        segA1.lineTo(0.4410480349344978f * width, 0.07164179104477612f * height);
        segA1.lineTo(0.1965065502183406f * width, 0.07164179104477612f * height);
        segA1.lineTo(0.11790393013100436f * width, 0.01791044776119403f * height);
        segA1.lineTo(0.11790393013100436f * width, 0.014925373134328358f * height);
        segA1.close();

        segA2.reset();
        segA2.moveTo(0.8864628820960698f * width, 0.01791044776119403f * height);
        segA2.lineTo(0.8602620087336245f * width, 0.0f * height);
        segA2.lineTo(0.5545851528384279f * width, 0.0f * height);
        segA2.lineTo(0.5021834061135371f * width, 0.029850746268656716f * height);
        segA2.lineTo(0.5021834061135371f * width, 0.03283582089552239f * height);
        segA2.lineTo(0.5589519650655022f * width, 0.07164179104477612f * height);
        segA2.lineTo(0.8122270742358079f * width, 0.07164179104477612f * height);
        segA2.lineTo(0.8864628820960698f * width, 0.020895522388059702f * height);
        segA2.lineTo(0.8864628820960698f * width, 0.01791044776119403f * height);
        segA2.close();

        segB.reset();
        segB.moveTo(0.8951965065502183f * width, 0.023880597014925373f * height);
        segB.lineTo(0.9213973799126638f * width, 0.04477611940298507f * height);
        segB.lineTo(0.9213973799126638f * width, 0.08358208955223881f * height);
        segB.lineTo(0.8820960698689956f * width, 0.4626865671641791f * height);
        segB.lineTo(0.8296943231441049f * width, 0.49850746268656715f * height);
        segB.lineTo(0.777292576419214f * width, 0.4626865671641791f * height);
        segB.lineTo(0.8209606986899564f * width, 0.07462686567164178f * height);
        segB.lineTo(0.8951965065502183f * width, 0.023880597014925373f * height);
        segB.close();

        segC.reset();
        segC.moveTo(0.8296943231441049f * width, 0.5014925373134328f * height);
        segC.lineTo(0.8777292576419214f * width, 0.5343283582089552f * height);
        segC.lineTo(0.8296943231441049f * width, 0.9671641791044776f * height);
        segC.lineTo(0.8078602620087336f * width, 0.982089552238806f * height);
        segC.lineTo(0.7292576419213974f * width, 0.9253731343283582f * height);
        segC.lineTo(0.7685589519650655f * width, 0.5432835820895522f * height);
        segC.lineTo(0.8296943231441049f * width, 0.5014925373134328f * height);
        segC.close();

        segD2.reset();
        segD2.moveTo(0.7205240174672489f * width, 0.9283582089552239f * height);
        segD2.lineTo(0.7991266375545851f * width, 0.982089552238806f * height);
        segD2.lineTo(0.7991266375545851f * width, 0.9880597014925373f * height);
        segD2.lineTo(0.7816593886462883f * width, 1.0f * height);
        segD2.lineTo(0.47161572052401746f * width, 1.0f * height);
        segD2.lineTo(0.4279475982532751f * width, 0.9701492537313433f * height);
        segD2.lineTo(0.4279475982532751f * width, 0.9641791044776119f * height);
        segD2.lineTo(0.48034934497816595f * width, 0.9283582089552239f * height);
        segD2.lineTo(0.7205240174672489f * width, 0.9283582089552239f * height);
        segD2.close();

        segD1.reset();
        segD1.moveTo(0.3624454148471616f * width, 0.9283582089552239f * height);
        segD1.lineTo(0.4148471615720524f * width, 0.9641791044776119f * height);
        segD1.lineTo(0.4148471615720524f * width, 0.9701492537313433f * height);
        segD1.lineTo(0.36681222707423583f * width, 1.0f * height);
        segD1.lineTo(0.06550218340611354f * width, 1.0f * height);
        segD1.lineTo(0.039301310043668124f * width, 0.982089552238806f * height);
        segD1.lineTo(0.039301310043668124f * width, 0.9761194029850746f * height);
        segD1.lineTo(0.1091703056768559f * width, 0.9283582089552239f * height);
        segD1.lineTo(0.3624454148471616f * width, 0.9283582089552239f * height);
        segD1.close();

        segE.reset();
        segE.moveTo(0.03056768558951965f * width, 0.9761194029850746f * height);
        segE.lineTo(0.0f * width, 0.9552238805970149f * height);
        segE.lineTo(0.0f * width, 0.9164179104477612f * height);
        segE.lineTo(0.043668122270742356f * width, 0.5373134328358209f * height);
        segE.lineTo(0.09606986899563319f * width, 0.5014925373134328f * height);
        segE.lineTo(0.14410480349344978f * width, 0.5373134328358209f * height);
        segE.lineTo(0.10043668122270742f * width, 0.9253731343283582f * height);
        segE.lineTo(0.03056768558951965f * width, 0.9761194029850746f * height);
        segE.close();

        segF.reset();
        segF.moveTo(0.1091703056768559f * width, 0.01791044776119403f * height);
        segF.lineTo(0.18777292576419213f * width, 0.07462686567164178f * height);
        segF.lineTo(0.15283842794759825f * width, 0.45671641791044776f * height);
        segF.lineTo(0.09170305676855896f * width, 0.49850746268656715f * height);
        segF.lineTo(0.043668122270742356f * width, 0.4626865671641791f * height);
        segF.lineTo(0.08733624454148471f * width, 0.03283582089552239f * height);
        segF.lineTo(0.1091703056768559f * width, 0.01791044776119403f * height);
        segF.close();

        segG.reset();
        segG.moveTo(0.1965065502183406f * width, 0.07462686567164178f * height);
        segG.lineTo(0.24017467248908297f * width, 0.11343283582089553f * height);
        segG.lineTo(0.4104803493449782f * width, 0.3492537313432836f * height);
        segG.lineTo(0.4497816593886463f * width, 0.48656716417910445f * height);
        segG.lineTo(0.3406113537117904f * width, 0.41492537313432837f * height);
        segG.lineTo(0.18340611353711792f * width, 0.19701492537313434f * height);
        segG.lineTo(0.1965065502183406f * width, 0.07462686567164178f * height);
        segG.close();

        segH.reset();
        segH.moveTo(0.5021834061135371f * width, 0.041791044776119404f * height);
        segH.lineTo(0.5502183406113537f * width, 0.07462686567164178f * height);
        segH.lineTo(0.5414847161572053f * width, 0.14626865671641792f * height);
        segH.lineTo(0.519650655021834f * width, 0.34328358208955223f * height);
        segH.lineTo(0.4585152838427948f * width, 0.4955223880597015f * height);
        segH.lineTo(0.4148471615720524f * width, 0.3492537313432836f * height);
        segH.lineTo(0.44541484716157204f * width, 0.07462686567164178f * height);
        segH.lineTo(0.5021834061135371f * width, 0.041791044776119404f * height);
        segH.close();

        segI.reset();
        segI.moveTo(0.8122270742358079f * width, 0.07462686567164178f * height);
        segI.lineTo(0.8034934497816594f * width, 0.1880597014925373f * height);
        segI.lineTo(0.5982532751091703f * width, 0.4f * height);
        segI.lineTo(0.47161572052401746f * width, 0.48656716417910445f * height);
        segI.lineTo(0.5327510917030568f * width, 0.33432835820895523f * height);
        segI.lineTo(0.7379912663755459f * width, 0.12238805970149254f * height);
        segI.lineTo(0.8122270742358079f * width, 0.07462686567164178f * height);
        segI.close();

        segK.reset();
        segK.moveTo(0.462882096069869f * width, 0.49850746268656715f * height);
        segK.lineTo(0.5152838427947598f * width, 0.4626865671641791f * height);
        segK.lineTo(0.7685589519650655f * width, 0.4626865671641791f * height);
        segK.lineTo(0.8253275109170306f * width, 0.49850746268656715f * height);
        segK.lineTo(0.7729257641921398f * width, 0.5373134328358209f * height);
        segK.lineTo(0.519650655021834f * width, 0.5373134328358209f * height);
        segK.lineTo(0.462882096069869f * width, 0.49850746268656715f * height);
        segK.close();

        segL.reset();
        segL.moveTo(0.7248908296943232f * width, 0.9253731343283582f * height);
        segL.lineTo(0.7248908296943232f * width, 0.9104477611940298f * height);
        segL.lineTo(0.7336244541484717f * width, 0.8238805970149253f * height);
        segL.lineTo(0.7336244541484717f * width, 0.8f * height);
        segL.lineTo(0.5764192139737991f * width, 0.5850746268656717f * height);
        segL.lineTo(0.47161572052401746f * width, 0.5134328358208955f * height);
        segL.lineTo(0.5065502183406113f * width, 0.6447761194029851f * height);
        segL.lineTo(0.6855895196506551f * width, 0.8865671641791045f * height);
        segL.lineTo(0.7248908296943232f * width, 0.9253731343283582f * height);
        segL.close();

        segM.reset();
        segM.moveTo(0.4192139737991266f * width, 0.9611940298507463f * height);
        segM.lineTo(0.37117903930131f * width, 0.9283582089552239f * height);
        segM.lineTo(0.37117903930131f * width, 0.8925373134328358f * height);
        segM.lineTo(0.39737991266375544f * width, 0.6567164179104478f * height);
        segM.lineTo(0.462882096069869f * width, 0.5044776119402985f * height);
        segM.lineTo(0.5065502183406113f * width, 0.6477611940298508f * height);
        segM.lineTo(0.4759825327510917f * width, 0.9253731343283582f * height);
        segM.lineTo(0.4192139737991266f * width, 0.9611940298507463f * height);
        segM.close();

        segN.reset();
        segN.moveTo(0.1091703056768559f * width, 0.9253731343283582f * height);
        segN.lineTo(0.11790393013100436f * width, 0.808955223880597f * height);
        segN.lineTo(0.2314410480349345f * width, 0.6955223880597015f * height);
        segN.lineTo(0.31877729257641924f * width, 0.6f * height);
        segN.lineTo(0.4497816593886463f * width, 0.5134328358208955f * height);
        segN.lineTo(0.3930131004366812f * width, 0.6507462686567164f * height);
        segN.lineTo(0.3799126637554585f * width, 0.6716417910447762f * height);
        segN.lineTo(0.18340611353711792f * width, 0.8776119402985074f * height);
        segN.lineTo(0.1091703056768559f * width, 0.9253731343283582f * height);
        segN.close();

        segP.reset();
        segP.moveTo(0.10043668122270742f * width, 0.49850746268656715f * height);
        segP.lineTo(0.1572052401746725f * width, 0.4626865671641791f * height);
        segP.lineTo(0.40611353711790393f * width, 0.4626865671641791f * height);
        segP.lineTo(0.4585152838427948f * width, 0.49850746268656715f * height);
        segP.lineTo(0.4017467248908297f * width, 0.5373134328358209f * height);
        segP.lineTo(0.1572052401746725f * width, 0.5373134328358209f * height);
        segP.lineTo(0.10043668122270742f * width, 0.49850746268656715f * height);
        segP.close();
        
        segDot.reset();
        segDot.addCircle(0.9301310043668122f * width, 0.9522388059701492f * height, 0.07423580786026202f * width, Direction.CW);
        segDot.close();
    }


    // ******************** Drawing related ***********************************
    @Override protected void onDraw(final Canvas CANVAS) {
        drawSegments(CANVAS, mapping.get(character) == null ? 0 : mapping.get(character));
    }

    private void drawSegments(final Canvas CANVAS, final int SEGMENTS) {
        inactivePaint.setStyle(Style.FILL);
        inactivePaint.setColor(derive(segmentColor, -80));

        activePaint.setStyle(Style.FILL);
        activePaint.setColor(segmentColor);

        for (int segment = 0 ; segment < 16 ; segment++) {
            // Check for each bit in bitmask
            Paint paint = ((1 << segment & SEGMENTS) != 0) ? activePaint : inactivePaint;
            switch(segment) {
                case  0: CANVAS.drawPath(segA1, paint); break;
                case  1: CANVAS.drawPath(segA2, paint); break;
                case  2: CANVAS.drawPath(segB, paint); break;
                case  3: CANVAS.drawPath(segC, paint); break;
                case  4: CANVAS.drawPath(segD2, paint); break;
                case  5: CANVAS.drawPath(segD1, paint); break;
                case  6: CANVAS.drawPath(segE, paint); break;
                case  7: CANVAS.drawPath(segF, paint); break;
                case  8: CANVAS.drawPath(segG, paint); break;
                case  9: CANVAS.drawPath(segH, paint); break;
                case 10: CANVAS.drawPath(segI, paint); break;
                case 11: CANVAS.drawPath(segK, paint); break;
                case 12: CANVAS.drawPath(segL, paint); break;
                case 13: CANVAS.drawPath(segM, paint); break;
                case 14: CANVAS.drawPath(segN, paint); break;
                case 15: CANVAS.drawPath(segP, paint); break;
            }
        }
        CANVAS.drawPath(segDot, dot ? activePaint : inactivePaint);
    };


    // ******************** Methods *******************************************
    public char getCharacter() { return character; }
    public void setCharacter(final char CHARACTER) {
        character = CHARACTER;
        if (!mapping.containsKey(character)) character = ' ';
        invalidate();
    }

    public boolean isDot() { return dot; }
    public void setDot(final boolean DOT) {
        dot = DOT;
        invalidate();
    }

    public int getSegmentColor() { return segmentColor; }
    public void setSegmentColor(final int SEGMENT_COLOR) {
        int[] rgb = Util.intToRGB(SEGMENT_COLOR);
        segmentColor = SEGMENT_COLOR;
        invalidate();
    }

    public boolean isGlow() { return glow; }
    public void setGlow(final boolean GLOW) {
        glow = GLOW;
        invalidate();
    }

    public int derive(final String COLOR, final float PERCENTAGE) {
        return derive(Color.parseColor(COLOR), PERCENTAGE);
    }
    public int derive(final int COLOR, final float PERCENTAGE) {
        float   p   = clamp(-100f, 100f, PERCENTAGE) / 100.0f;
        float[] hsv = new float[3];
        Color.colorToHSV(COLOR, hsv);
        hsv[2] = clamp(0f, 1f, hsv[2] + (p * hsv[2]));
        return Color.HSVToColor(hsv);
    }

    private float clamp(final float MIN, final float MAX, final float VALUE) {
        if (VALUE < MIN) return MIN;
        if (VALUE > MAX) return MAX;
        return VALUE;
    }
}
