package eu.hansolo.enzo;

/**
 * Created by hansolo on 17.04.15.
 */
public class Stop {
    private int   color;
    private float offset;

    public Stop(final float OFFSET, final int COLOR) {
        offset = Util.clamp(0f, 1f, OFFSET);
        color  = COLOR;
    }

    public int getColor() { return color; }
    public void setColor(final int COLOR) { color = COLOR; }

    public float getOffset() { return offset; }
    public void setOffset(final float OFFSET) { offset = Util.clamp(0f, 1f, OFFSET); }
}
