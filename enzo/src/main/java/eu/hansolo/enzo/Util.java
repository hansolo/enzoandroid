package eu.hansolo.enzo;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.util.DisplayMetrics;


/**
 * Created by hansolo on 17.04.15.
 */
public class Util {
    private static final double DARKER_BRIGHTER_FACTOR = 0.7;


    public static float clamp(final float MIN, final float MAX, final float VALUE) {
        if (Float.compare(VALUE, MIN) < 0) return MIN;
        if (Float.compare(VALUE, MAX) > 0) return MAX;
        return VALUE;
    }
    public static int clamp(final int MIN, final int MAX, final int VALUE) {
        if (VALUE < MIN) return MIN;
        if (VALUE > MAX) return MAX;
        return VALUE;
    }
    public static long clamp(final long MIN, final long MAX, final long VALUE) {
        if (VALUE < MIN) return MIN;
        if (VALUE > MAX) return MAX;
        return VALUE;
    }

    public static int[] intToRGB(final int COLOR) {
        return new int[] { Color.red(COLOR), Color.green(COLOR), Color.blue(COLOR), Color.alpha(COLOR)};
    }

    public static float interpolate(final float A, final float B, final float FRACTION) { return (A + ((B - A) * FRACTION)); }

    public static int interpolateColor(final int COLOR_1, final int COLOR_2, final float FRACTION) {
        float[] hsva = new float[3];
        float[] hsvb = new float[3];
        Color.colorToHSV(COLOR_1, hsva);
        Color.colorToHSV(COLOR_2, hsvb);
        for (int i = 0; i < 3; i++) {
            hsvb[i] = interpolate(hsva[i], hsvb[i], FRACTION);
        }
        return Color.HSVToColor(hsvb);
    }

    public static int brighter(final int COLOR, final float FRACTION) {
        final double FACTOR = 1d / 255d;
        int[]        rgba   = intToRGB(COLOR);
        return deriveColor(rgba[0] * FACTOR, rgba[1] * FACTOR, rgba[2] * FACTOR, rgba[3] * FACTOR,
                           0, 1.0, 1.0 / DARKER_BRIGHTER_FACTOR, 1.0);
    }

    public static int darker(final int COLOR, final float FRACTION) {
        final double FACTOR = 1d / 255d;
        int[]        rgba   = intToRGB(COLOR);
        return deriveColor(rgba[0] * FACTOR, rgba[1] * FACTOR, rgba[2] * FACTOR, rgba[3] * FACTOR,
                           0, 1.0, DARKER_BRIGHTER_FACTOR, 1.0);
    }

    public static int deriveColor(double red, double green, double blue, double opacity,
                           double hueShift, double saturationFactor,
                           double brightnessFactor, double opacityFactor) {

        double[] hsb = RGBtoHSB(red, green, blue);

        /* Allow brightness increase of black color */
        double b = hsb[2];
        if (b == 0 && brightnessFactor > 1.0) {
            b = 0.05;
        }

        /* the tail "+ 360) % 360" solves shifts into negative numbers */
        double h = (((hsb[0] + hueShift) % 360) + 360) % 360;
        double s = Math.max(Math.min(hsb[1] * saturationFactor, 1.0), 0.0);
        b = Math.max(Math.min(b * brightnessFactor, 1.0), 0.0);
        double a = Math.max(Math.min(opacity * opacityFactor, 1.0), 0.0);

        double[] rgbDouble = HSBtoRGB(h, s, b);
        double[] rgbInt = { (rgbDouble[0] * 255), (rgbDouble[1] * 255), (rgbDouble[2] * 255), (a * 255)};

        return Color.argb((int) rgbInt[3], (int) rgbInt[0], (int) rgbInt[1], (int) rgbInt[2]);
    }

    public static double[] HSBtoRGB(double hue, double saturation, double brightness) {
        // normalize the hue
        double normalizedHue = ((hue % 360) + 360) % 360;
        hue = normalizedHue/360;

        double r = 0, g = 0, b = 0;
        if (saturation == 0) {
            r = g = b = brightness;
        } else {
            double h = (hue - Math.floor(hue)) * 6.0;
            double f = h - java.lang.Math.floor(h);
            double p = brightness * (1.0 - saturation);
            double q = brightness * (1.0 - saturation * f);
            double t = brightness * (1.0 - (saturation * (1.0 - f)));
            switch ((int) h) {
                case 0:
                    r = brightness;
                    g = t;
                    b = p;
                    break;
                case 1:
                    r = q;
                    g = brightness;
                    b = p;
                    break;
                case 2:
                    r = p;
                    g = brightness;
                    b = t;
                    break;
                case 3:
                    r = p;
                    g = q;
                    b = brightness;
                    break;
                case 4:
                    r = t;
                    g = p;
                    b = brightness;
                    break;
                case 5:
                    r = brightness;
                    g = p;
                    b = q;
                    break;
            }
        }
        double[] f = new double[3];
        f[0] = r;
        f[1] = g;
        f[2] = b;
        return f;
    }

    public static double[] RGBtoHSB(final int RED, final int GREEN, final int BLUE) {
        final double FACTOR = 1d / 255d;
        return RGBtoHSB(RED * FACTOR, GREEN * FACTOR, BLUE * FACTOR);
    }
    public static double[] RGBtoHSB(final double RED, final double GREEN, final double BLUE) {
        double hue, saturation, brightness;
        double[] hsbvals = new double[3];
        double cmax = (RED > GREEN) ? RED : GREEN;
        if (BLUE > cmax) cmax = BLUE;
        double cmin = (RED < GREEN) ? RED : GREEN;
        if (BLUE < cmin) cmin = BLUE;

        brightness = cmax;
        if (cmax != 0)
            saturation = (cmax - cmin) / cmax;
        else
            saturation = 0;

        if (saturation == 0) {
            hue = 0;
        } else {
            double redc = (cmax - RED) / (cmax - cmin);
            double greenc = (cmax - GREEN) / (cmax - cmin);
            double bluec = (cmax - BLUE) / (cmax - cmin);
            if (RED == cmax)
                hue = bluec - greenc;
            else if (GREEN == cmax)
                hue = 2.0 + redc - bluec;
            else
                hue = 4.0 + greenc - redc;
            hue = hue / 6.0;
            if (hue < 0)
                hue = hue + 1.0;
        }
        hsbvals[0] = hue * 360;
        hsbvals[1] = saturation;
        hsbvals[2] = brightness;
        return hsbvals;
    }

    public static float dpToPixel(final float DP, final Context CONTEXT){
        final Resources RESOURCES = CONTEXT.getResources();
        final DisplayMetrics METRICS   = RESOURCES.getDisplayMetrics();
        final float PX = DP * (METRICS.densityDpi / 160f);
        return PX;
    }
    public static float pxToDp(final float PX, final Context CONTEXT){
        final Resources      RESOURCES = CONTEXT.getResources();
        final DisplayMetrics METRICS   = RESOURCES.getDisplayMetrics();
        final float DP = PX / (METRICS.densityDpi / 160f);
        return DP;
    }
}
